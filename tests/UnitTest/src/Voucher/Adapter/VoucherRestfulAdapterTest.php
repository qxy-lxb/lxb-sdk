<?php
namespace Sdk\Voucher\Adapter;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Voucher\Model\Voucher;
use Sdk\Voucher\Model\NullVoucher;
use Sdk\Voucher\Utils\MockFactory;
use Sdk\Voucher\Translator\VoucherRestfulTranslator;

class VoucherRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(VoucherRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends VoucherRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    // public function testImplementsIVoucherAdapter()
    // {
    //     $this->assertInstanceOf(
    //         'Sdk\Voucher\Adapter\IVoucherAdapter',
    //         $this->stub
    //     );
    // }

    public function testGetResource()
    {
        $this->assertEquals('vouchers', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Voucher\Translator\VoucherRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'VOUCHER_LIST',
                VoucherRestfulAdapter::SCENARIOS['VOUCHER_LIST']
            ],
            [
                'VOUCHER_FETCH_ONE',
                VoucherRestfulAdapter::SCENARIOS['VOUCHER_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $voucher = MockFactory::generateVoucherObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullVoucher())
            ->willReturn($voucher);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($voucher, $result);
    }
    /**
     * 为VoucherRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$Voucher，$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareVoucherTranslator(
        Voucher $Voucher,
        array $keys,
        array $VoucherArray
    ) {
        $translator = $this->prophesize(VoucherRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($Voucher),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($VoucherArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(Voucher $Voucher)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($Voucher);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareVoucherTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $voucher = MockFactory::generateVoucherObject(1);
        $voucherArray = array('bindCompanies');

        $this->prepareVoucherTranslator(
            $voucher,
            array(
                'category',
                'voucher',
                'status',
                'crew'
            ),
            $voucherArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('vouchers', $voucherArray);

        $this->success($voucher);

        $result = $this->stub->add($voucher);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareVoucherTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $voucher = MockFactory::generateVoucherObject(1);
        $voucherArray = array('vouchers');

        $this->prepareVoucherTranslator(
            $voucher,
            array(
                'category',
                'voucher',
                'status',
                'crew'
            ),
            $voucherArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('vouchers', $voucherArray);

        $this->failure($voucher);
        $result = $this->stub->add($voucher);
        $this->assertFalse($result);
    }
}
