<?php
namespace Sdk\Voucher\Model;

use Sdk\Voucher\Repository\VoucherRepository;
use Sdk\Voucher\Repository\VoucherSessionRepository;

use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IEnableAbleAdapter;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Sdk\Crew\Model\Crew;

use Marmot\Core;

class VoucherTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(Voucher::class)
            ->setMethods([
                'getRepository',
                'IOperatAbleAdapter',
                'IEnableAbleAdapter'
            ])->getMock();

        $this->childStub = new Class extends Voucher{

            public function getRepository() : VoucherRepository
            {
                return parent::getRepository();
            }

            public function getIOperatAbleAdapter() : IOperatAbleAdapter
            {
                return parent::getIOperatAbleAdapter();
            }

            public function getIEnableAbleAdapter() : IEnableAbleAdapter
            {
                return parent::getIEnableAbleAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Voucher\Repository\VoucherRepository',
            $this->childStub->getRepository()
        );
    }

    public function testCorrectImplementsIObject()
    {
        $this->assertInstanceof('Marmot\Common\Model\IObject', $this->stub);
    }

    public function testCorrectImplementsIOperatAble()
    {
        $this->assertInstanceof('Sdk\Common\Model\IOperatAble', $this->stub);
    }

    public function testCorrectImplementsIEnableAble()
    {
        $this->assertInstanceof('Sdk\Common\Model\IEnableAble', $this->stub);
    }

    public function testGetIOperatAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IOperatAbleAdapter',
            $this->childStub->getIOperatAbleAdapter()
        );
    }

    public function testGetIEnabledAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IEnableAbleAdapter',
            $this->childStub->getIEnableAbleAdapter()
        );
    }

    public function testSetNameCorrectType()
    {
        $this->stub->setName('string');
        $this->assertEquals('string', $this->stub->getName());
    }
    /**
     * 设置 Voucher Name() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->stub->setName(array());
    }

    public function testSetVoucherCorrectType()
    {
        $this->stub->setVoucher(array(1,2,3));
        $this->assertEquals(array(1,2,3), $this->stub->getVoucher());
    }
    /**
     * 设置 Voucher Voucher() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVoucherWrongType()
    {
        $this->stub->setVoucher('string');
    }

    public function testSetCategoryCorrectType()
    {
        $this->stub->setCategory(1);
        $this->assertEquals(1, $this->stub->getCategory());
    }
    /**
     * 设置 Voucher Category() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCategoryWrongType()
    {
        $this->stub->setCategory('string');
    }

    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(1);
        $this->assertEquals(1, $this->stub->getStatus());
    }
    /**
     * 设置 Voucher setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('string');
    }

    public function testSetCrewCorrectType()
    {
        $object = new Crew();
        $this->stub->setCrew($object);
        $this->assertSame($object, $this->stub->getCrew());
    }

    /**
     * 设置 Policy setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewType()
    {
        $this->stub->setCrew(array(1,2,3));
    }
}
