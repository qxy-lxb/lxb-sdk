<?php
namespace Sdk\NaturalPerson\Model;

use PHPUnit\Framework\TestCase;

class IdentityInfoTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new IdentityInfo(
            '张三',
            '610528172727382837',
            '男',
            '0000-00-00',
            '西安旺座国际',
            array('positivePhoto'),
            array('reversePhoto')
        );
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGtRealName()
    {
        $this->assertEquals('张三', $this->stub->getRealName());
    }

    public function testGetCardId()
    {
        $this->assertEquals('610528172727382837', $this->stub->getCardId());
    }

    public function testGetGender()
    {
        $this->assertEquals('男', $this->stub->getGender());
    }

    public function testGetBirthday()
    {
        $this->assertEquals('0000-00-00', $this->stub->getBirthday());
    }

    public function testGetAddress()
    {
        $this->assertEquals('西安旺座国际', $this->stub->getAddress());
    }

    public function testGetPositivePhoto()
    {
        $this->assertEquals(array('positivePhoto'), $this->stub->getPositivePhoto());
    }

    public function testGetReversePhoto()
    {
        $this->assertEquals(array('reversePhoto'), $this->stub->getReversePhoto());
    }
}
