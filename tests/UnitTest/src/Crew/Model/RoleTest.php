<?php
namespace Sdk\Crew\Model;

use PHPUnit\Framework\TestCase;

/**
 * Role\Model\Role.class.php 测试文件
 * @author chloroplast
 * @version 1.0.0:2017.12.02
 */

class RoleTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new Role();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    /**
     * Role 角色,测试构造函数
     */
    public function testRoleConstructor()
    {
        //测试初始化主键id
        $this->assertEquals(0, $this->stub->getId());

        //测试初始化
        $this->assertEmpty($this->stub->getName());
    }


    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Role setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 Role setId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdWrongType()
    {
        $this->stub->setId('string');
    }

    /**
     * 设置 Role setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //name 测试 -------------------------------------------------------- start
    /**
     * 设置 Role setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->stub->setName('string');
        $this->assertEquals('string', $this->stub->getName());
    }

    /**
     * 设置 Role setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->stub->setName(array(1,2,3));
    }
    //name 测试 --------------------------------------------------------   end

    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 Role setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->stub->setStatus($actual);
        $this->assertEquals($expected, $this->stub->getStatus());
    }
    /**
     * 循环测试 Role setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(Role::STATUS_NORMAL,Role::STATUS_NORMAL),
            array(Role::STATUS_DELETE,Role::STATUS_DELETE),
            array(999,Role::STATUS_NORMAL),
        );
    }
    /**
     * 设置 Role setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('string');
    }
    //status 测试 ------------------------------------------------------   end

    public function testIsPlatform()
    {
        $role = new Role(Role::PLATFORM);
        $this->assertTrue($role->isPlatform());
    }

    public function testIsAdministration()
    {
        $role = new Role(Role::ADMINISTRATION);
        $this->assertTrue($role->isAdministration());
    }

    public function testIsRepair()
    {
        $role = new Role(Role::REPAIR);
        $this->assertTrue($role->isRepair());
    }

    public function testIsPresentation()
    {
        $role = new Role(Role::PRESENTATION);
        $this->assertTrue($role->isPresentation());
    }
}
