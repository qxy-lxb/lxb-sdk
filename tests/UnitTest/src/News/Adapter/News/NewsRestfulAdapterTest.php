<?php
namespace Sdk\News\Adapter\News;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\News\Model\News;
use Sdk\News\Model\NullNews;
use Sdk\News\Utils\MockFactory;
use Sdk\News\Translator\NewsRestfulTranslator;

class NewsRestfulAdapterTest extends TestCase
{
    private $stub;

    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(NewsRestfulAdapter::class)
            ->setMethods([
                'fetchOneAction',
                'isSuccess',
                'post',
                'patch',
                'translateToObject',
                'getTranslator'
            ])->getMock();

        $this->childStub = new class extends NewsRestfulAdapter {
            public function getResource() : string
            {
                return parent::getResource();
            }

            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }

            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    public function testImplementsINewsAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\News\Adapter\News\INewsAdapter',
            $this->stub
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('news', $this->childStub->getResource());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\News\Translator\NewsRestfulTranslator',
            $this->childStub->getTranslator()
        );
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childStub->scenario($expect);
        $this->assertEquals($actual, $this->childStub->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'NEWS_LIST',
                NewsRestfulAdapter::SCENARIOS['NEWS_LIST']
            ],
            [
                'NEWS_FETCH_ONE',
                NewsRestfulAdapter::SCENARIOS['NEWS_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }
    /**
     * 设置ID
     * 根据ID生成模拟数据
     * 揭示fetchOneAction，期望返回模拟的数据
     * 执行fetchOne（）方法
     * 判断result是否和模拟数据相等，不相等则抛出异常
     */
    public function testFetchOne()
    {
        $id = 1;

        $news = MockFactory::generateNewsObject($id);

        $this->stub->expects($this->exactly(1))
            ->method('fetchOneAction')
            ->with($id, new NullNews())
            ->willReturn($news);

        $result = $this->stub->fetchOne($id);
        $this->assertEquals($news, $result);
    }
    /**
     * 为NewsRestfulTranslator建立预言
     * 建立预期状况：objectToArray() 方法将会被调用一次，并以$News，$keys为参数
     * 揭示预言中的getTranslator，并将仿件对象链接到主体上
     */
    private function prepareNewsTranslator(
        News $news,
        array $keys,
        array $newsArray
    ) {
        $translator = $this->prophesize(NewsRestfulTranslator::class);
        $translator->objectToArray(
            Argument::exact($news),
            Argument::exact($keys)
        )->shouldBeCalledTimes(1)
            ->willReturn($newsArray);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
    }

    private function success(News $news)
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(true);
        $this->stub->expects($this->exactly(1))
            ->method('translateToObject')
            ->with($news);
    }

    private function failure()
    {
        $this->stub->expects($this->exactly(1))
            ->method('isSuccess')
            ->willReturn(false);
        $this->stub->expects($this->exactly(0))
            ->method('translateToObject');
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行add（）
     * 判断 result 是否为true
     */
    public function testAddSuccess()
    {
        $news = MockFactory::generateNewsObject(1);
        $newsArray = array('bindCompanies');

        $this->prepareNewsTranslator(
            $news,
            array(
                'title',
                'content',
                'image',
                'attachments',
                'category',
                'crew'
            ),
            $newsArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('news', $newsArray);

        $this->success($news);

        $result = $this->stub->add($news);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行add（）
     * 判断 result 是否为false
     */
    public function testAddFailure()
    {
        $news = MockFactory::generateNewsObject(1);
        $newsArray = array('news');

        $this->prepareNewsTranslator(
            $news,
            array(
                'title',
                'content',
                'image',
                'attachments',
                'category',
                'crew'
            ),
            $newsArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('post')
            ->with('news', $newsArray);

        $this->failure($news);
        $result = $this->stub->add($news);
        $this->assertFalse($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行success（）
     * 执行edit（）
     * 判断 result 是否为true
     */
    public function testEditSuccess()
    {
        $news = MockFactory::generateNewsObject(1);
        $NewsArray = array('news');

        $this->prepareNewsTranslator(
            $news,
            array(
                'title',
                'content',
                'image',
                'attachments'
            ),
            $NewsArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'news/'.$news->getId(),
                $NewsArray
            );

        $this->success($news);

        $result = $this->stub->edit($news);
        $this->assertTrue($result);
    }
    /**
     * 生成模拟数据，传参为1
     * 设置空数组
     * 执行prepareNewsTranslator方法
     * 揭示预言中的post，并将仿件对象链接到主体上
     * 执行failure（）
     * 执行edit（）
     * 判断 result 是否为false
     */
    public function testEditFailure()
    {
        $news = MockFactory::generateNewsObject(1);
        $newsArray = array('news');

        $this->prepareNewsTranslator(
            $news,
            array(
                'title',
                'content',
                'image',
                'attachments'
            ),
            $newsArray
        );

        $this->stub->expects($this->exactly(1))
            ->method('patch')
            ->with(
                'news/'.$news->getId(),
                $newsArray
            );

        $this->failure($news);
        $result = $this->stub->edit($news);
        $this->assertFalse($result);
    }
}
