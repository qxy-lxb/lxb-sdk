<?php
namespace Sdk\News\Model;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Crew\Model\Crew;
use Sdk\News\Repository\NewsRepository;
use Sdk\Common\Adapter\IEnableAbleAdapter;
use Sdk\Common\Adapter\ITopAbleAdapter;
use Sdk\Common\Model\IEnableAble;
use Sdk\Common\Model\ITopAble;

class NewsTest extends TestCase
{
    private $stub;
    private $childStub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(News::class)
            ->setMethods(['getRepository','getIEnableAbleAdapter','getITopAbleAdapter','isOperateAble'])
            ->getMock();

        $this->childStub = new class extends News
        {
            public function getRepository(): NewsRepository
            {
                return parent::getRepository();
            }

            public function getIEnableAbleAdapter(): IEnableAbleAdapter
            {
                return parent::getIEnableAbleAdapter();
            }

            public function getITopAbleAdapter() : ITopAbleAdapter
            {
                return parent::getITopAbleAdapter();
            }
        };
    }

    public function tearDown()
    {
        unset($this->stub);
        unset($this->childStub);
    }

    /**
     * News 新闻类,测试构造函数
     */
    public function testNewsConstructor()
    {
        $this->assertEquals(p, $this->stub->getId());
        $this->assertEmpty($this->stub->getTitle());
        $this->assertEmpty($this->stub->getContent());
        $this->assertEmpty($this->stub->getImage());
        $this->assertEquals(0, $this->stub->getCreateTime());
        $this->assertEquals(0, $this->stub->getUpdateTime());
        $this->assertEquals(0, $this->stub->getStatusTime());
    }

    public function testCorrectImplementsIEnableAble()
    {
        $this->assertInstanceof('Sdk\Common\Model\IEnableAble', $this->stub);
    }

    public function testCorrectImplementsITopAble()
    {
        $this->assertInstanceof('Sdk\Common\Model\ITopAble', $this->stub);
    }

    public function testCorrectImplementsIObject()
    {
        $this->assertInstanceof('Marmot\Common\Model\IObject', $this->stub);
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 News setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 News setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //title 测试 ------------------------------------------------------- start
    /**
     * 设置 News setTitle() 正确的传参类型,期望传值正确
     */
    public function testSetTitleCorrectType()
    {
        $this->stub->setTitle('string');
        $this->assertEquals('string', $this->stub->getTitle());
    }

    /**
     * 设置 News setTitle() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTitleWrongType()
    {
        $this->stub->setTitle(array(1, 2, 3));
    }
    //title 测试 -------------------------------------------------------   end

    //content 测试 ----------------------------------------------------- start
    /**
     * 设置 News setContent() 正确的传参类型,期望传值正确
     */
    public function testSetContentCorrectType()
    {
        $this->stub->setContent(array());
        $this->assertEquals(array(), $this->stub->getContent());
    }

    /**
     * 设置 News setContent() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContentWrongType()
    {
        $this->stub->setContent('string');
    }
    //content 测试 -----------------------------------------------------   end

    //attachments 测试 ------------------------------------------------- start
    public function testSetAttachmentsCorrectType()
    {
        $this->stub->setAttachments(array(1, 2, 3));
        $this->assertEquals(array(1, 2, 3), $this->stub->getAttachments());
    }

    /**
     * @expectedException TypeError
     */
    public function testSetAttachmentsWrongType()
    {
        $this->stub->setAttachments(1);
    }
    //attachments 测试 -------------------------------------------------   end

    //image 测试 ------------------------------------------------------ start
    public function testSetImageCorrectType()
    {
        $this->stub->setImage(array());
        $this->assertEquals(array(), $this->stub->getImage());
    }

    /**
     * @expectedException TypeError
     */
    public function testSetImageWrongType()
    {
        $this->stub->setImage('string');
    }
    //image 测试 ------------------------------------------------------   end

    //category 测试 --------------------------------------------- start
    /**
     * 设置 News setCategory() 正确的传参类型,期望传值正确
     */
    public function testSetCategoryCorrectType()
    {
        $object = News::CATEGORY['POLICY_NEWS'];
        $this->stub->setCategory($object);
        $this->assertSame($object, $this->stub->getCategory());
    }

    /**
     * 设置 News setCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCategoryWrongType()
    {
        $this->stub->setCategory('string');
    }
    //category 测试 ---------------------------------------------   end

    //crew 测试 --------------------------------------------- start
    /**
     * 设置 News setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $object = new Crew();
        $this->stub->setCrew($object);
        $this->assertSame($object, $this->stub->getCrew());
    }

    /**
     * 设置 News setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->stub->setCrew('string');
    }

    //crew 测试 ---------------------------------------------   end

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\News\Repository\NewsRepository',
            $this->childStub->getRepository()
        );
    }

    public function testGetIEnableAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\IEnableAbleAdapter',
            $this->childStub->getIEnableAbleAdapter()
        );
    }

    public function testGetITopAbleAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\ITopAbleAdapter',
            $this->childStub->getITopAbleAdapter()
        );
    }

    public function testAdd()
    {
        $repository = $this->prophesize(NewsRepository::class);
        $repository->add(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
                    ->method('getRepository')
                    ->willReturn($repository->reveal());

        $result = $this->stub->add();
        $this->assertTrue($result);
    }

    public function testAddFailure()
    {
        $result = $this->stub->add();
        $this->assertFalse($result);
    }

    public function testEditSuccess()
    {
        $repository = $this->prophesize(NewsRepository::class);
        $repository->edit(Argument::exact($this->stub))->shouldBeCalledTimes(1)->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->edit();
        $this->assertTrue($result);
    }

    public function testEditFailure()
    {
        $result = $this->stub->edit();
        $this->assertFalse($result);
    }
}
