<?php
namespace Sdk\News\Utils;

use Common\Model\IApplyAble;
use Sdk\News\Model\News;

class ArrayGenerate
{
    public static function generateNews(int $seed = 0, $value = array()) : array
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $news = array();

        $news = array(
            'data'=>array(
                'type'=>'news',
                'id'=>$faker->randomNumber(2)
            )
        );

        $attributes = array();
        //title
        $title = isset($value['title']) ? $value['title'] : $faker->word;
        $attributes['title'] = $title;

        //content
        $content = isset($value['content']) ? $value['content'] : array(1);

        $attributes['content'] = $content;

        //createTime
        $createTime = isset($value['createTime']) ? $value['createTime'] : 1513737146;
        $attributes['createTime'] = $createTime;

         //updateTime
        $updateTime = isset($value['updateTime']) ? $value['updateTime'] : 1513737146;
        $attributes['updateTime'] = $updateTime;

         //statusTime
        $statusTime = isset($value['statusTime']) ? $value['statusTime'] : 1513737146;
        $attributes['statusTime'] = $statusTime;

        //image
        $image = isset($value['image']) ? $value['image'] :  array('12','2','2');
        $attributes['image'] = $image;

        //attachments
        $attachments = isset($value['attachments']) ? $value['attachments'] : array('12','2','2');
        $attributes['attachments'] = $attachments;

        //status
        $status = isset($value['status']) ? $value['status'] : $faker->randomElement(
            $array = array(
                News::STATUS['ENABLED'],
                News::STATUS['DISABLED']
            )
        );
        $attributes['status'] = $status;


        //category
        $category = isset($value['category']) ?
            $value['category'] : $faker->randomElement(
                $array = array(
                News::CATEGORY['POLICY_NEWS'],
                News::CATEGORY['CREDIT_INFO']
                )
            );
        $attributes['category'] = $category;

        $news['data']['attributes'] = $attributes;

        // $news['data']['relationships']['crew']['data'] = array(
        //         'type' => 'crews',
        //         'id' => $faker->randomNumber(1)
        // );

        return $news;
    }

    public static function generateUnAuditedNews(int $seed = 0) : array
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $unAuditedNews = array();

        $unAuditedNews = array(
            'data'=>array(
                'type'=>'unAuditNews',
                'id'=>$faker->randomNumber(1)
            )
        );

        $attributes = array();
        //title
        $title = isset($value['title']) ? $value['title'] : $faker->word;
        $attributes['title'] = $title;

        //content
        $content = isset($value['content']) ? $value['content'] : $faker->sentence;

        $attributes['content'] = $content;

        //createTime
        $createTime = isset($value['createTime']) ? $value['createTime'] : 1513737146;
        $attributes['createTime'] = $createTime;

        //updateTime
        $updateTime = isset($value['updateTime']) ? $value['updateTime'] : 1513737146;
        $attributes['updateTime'] = $updateTime;

        //statusTime
        $statusTime = isset($value['statusTime']) ? $value['statusTime'] : 1513737146;
        $attributes['statusTime'] = $statusTime;

        //source
        $source = isset($value['source']) ? $value['source'] : $faker->word;
        $attributes['source'] = $source;

        //image
        $image = isset($value['image']) ? $value['image'] : $faker->md5;
        $attributes['image'] = $image;

        //attachments
        $attachments = isset($value['attachments']) ? $value['attachments'] : array('12','2','2');
        $attributes['attachments'] = $attachments;

        //status
        $status = isset($value['status']) ? $value['status'] : $faker->randomElement(
            $array = array(
                News::STATUS['ENABLED'],
                News::STATUS['DISABLED']
            )
        );
        $attributes['status'] = $status;
        //applyStatus
        $applyStatus = isset($value['applyStatus']) ? $value['applyStatus'] : $faker->randomElement(
            $array = array(
                IApplyAble::APPLY_STATUS['PENDING'],
                IApplyAble::APPLY_STATUS['APPROVE'],
                IApplyAble::APPLY_STATUS['REJECT'],
            )
        );
        $attributes['applyStatus'] = $applyStatus;
        //applyType
        $applyType = isset($value['applyType']) ? $value['applyType'] : $faker->randomElement(
            $array = array(
                IApplyAble::APPLY_TYPE['NULL'],
                IApplyAble::APPLY_TYPE['ADD'],
                IApplyAble::APPLY_TYPE['EDIT'],
                IApplyAble::APPLY_TYPE['ACCEPT'],
                IApplyAble::APPLY_TYPE['ENABLE'],
                IApplyAble::APPLY_TYPE['DISABLE']
            )
        );
        $attributes['applyType'] = $applyType;

        //parentCategory
        $parentCategory = isset($value['parentCategory']) ? $value['parentCategory'] : $faker->randomNumber(1);
        $attributes['parentCategory'] = $parentCategory;

        //category
        $category = isset($value['category']) ? $value['category'] : $faker->randomNumber(1);
        $attributes['category'] = $category;

        //newsType
        $newsType = isset($value['newsType']) ? $value['newsType'] : $faker->randomNumber(1);
        $attributes['newsType'] = $newsType;

        $unAuditedNews['data']['attributes'] = $attributes;

        //publishUserGroup
        $unAuditedNews['data']['relationships']['publishUserGroup']['data'] = array(
                'type' => 'userGroups',
                'id' => $faker->randomNumber(1)
        );
        //crew
        $unAuditedNews['data']['relationships']['crew']['data'] = array(
                'type' => 'crews',
                'id' => $faker->randomNumber(1)
        );
        //applyUserGroup
        $unAuditedNews['data']['relationships']['applyUserGroup']['data'] = array(
                'type' => 'userGroups',
                'id' => $faker->randomNumber(1)
        );
        //applyCrew
        $unAuditedNews['data']['relationships']['applyCrew']['data'] = array(
                'type' => 'crews',
                'id' => $faker->randomNumber(1)
        );

        return $unAuditedNews;
    }
}
