<?php
namespace Sdk\News\Utils;

use Sdk\News\Model\News;

use Sdk\Common\Model\IEnableAble;
use Sdk\News\Model\IdentityInfo;

class MockFactory
{
    /**
     * [generateNewsArray 生成实名认证数组]
     * @return [array] [认证信息]
     */
    public static function generateNewsArray() : array
    {
        $faker = \Faker\Factory::create('zh_CN');

        $identifyPersonal = array();

        $identifyPersonal = array(
            'data'=>array(
                'type'=>'news',
                'id'=>$faker->randomNumber(2)
            )
        );
        $value = array();
        $attributes = array();
        //member
        $member = self::generateMember($faker, $value);
        $attributes['member'] = $member;
        //realName
        $realName = self::generateRealName($faker, $value);
        $attributes['realName'] = $realName;
        //cardId
        $cardId = self::generateCardId($faker, $value);
        $attributes['cardId'] = $cardId;
        //gender
        $gender = self::generateGender($faker, $value);
        $attributes['gender'] = $gender;
        //birthday
        $birthday = self::generateBirthday($faker, $value);
        $attributes['birthday'] = $birthday;
        //address
        $address = self::generateAddress($faker, $value);
        $attributes['address'] = $address;
        //reversePhoto
        $reversePhoto = self::generateReversePhoto($faker, $value);
        $attributes['reversePhoto'] = $reversePhoto;
        //positivePhoto
        $positivePhoto = self::generatePositivePhoto($faker, $value);
        $attributes['positivePhoto'] = $positivePhoto;

        //createTime
        $createTime = \Sdk\Common\Utils\MockFactory::generateCreateTime($faker, $value);
        $attributes['createTime'] = $createTime;
        //updateTime
        $updateTime = \Sdk\Common\Utils\MockFactory::generateUpdateTime($faker, $value);
        $attributes['updateTime'] = $updateTime;
        //statusTime
        $statusTime = \Sdk\Common\Utils\MockFactory::generateStatusTime($faker, $value);
        $attributes['statusTime'] = $statusTime;
        //status
        $status = \Sdk\Common\Utils\MockFactory::generateStatus($faker, $value);
        $attributes['status'] = $status;

        $identifyPersonal['data']['attributes'] = $attributes;

        $identifyPersonal['data']['relationships']['member']['data'] = array(
            'type' => 'members',
            'id' => $faker->randomNumber(1)
        );

        return $identifyPersonal;
    }
    /**
     * [generateNewsObject 生成实名认证信息对象]
     * @param  int|integer $id
     * @param  int|integer $seed
     * @param  array       $value
     * @return [object]             [认证信息]
     */
    public static function generateNewsObject(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : News {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $news = new News($id);

        //title
        $title = isset($value['title']) ? $value['title'] : $faker->word;
        $news->setTitle($title);

        //content
        $content = isset($value['content']) ? $value['content'] :  array('12','2','2');
        
        $news->setContent($content);

        //createTime
        $createTime = isset($value['createTime']) ? $value['createTime'] : 1513737146;
        $news->setCreateTime($createTime);

         //updateTime
        $updateTime = isset($value['updateTime']) ? $value['updateTime'] : 1513737146;
        $news->setUpdateTime($updateTime);

         //statusTime
        $statusTime = isset($value['statusTime']) ? $value['statusTime'] : 1513737146;
        $news->setstatusTime($statusTime);

        //image
        $image = isset($value['image']) ? $value['image'] :  array('12','2','2');
        $news->setImage($image);

        //attachments
        $attachments = isset($value['attachments']) ? $value['attachments'] : array('12','2','2');
        $news->setAttachments($attachments);

        //status
        $status = isset($value['status']) ? $value['status'] : $faker->randomElement(
            $array = array(
                News::STATUS['ENABLED'],
                News::STATUS['DISABLED']
            )
        );

        $news->setStatus($status);

        //category
        $category = isset($value['category']) ?
            $value['category'] : $faker->randomElement(
                $array = array(
                News::CATEGORY['POLICY_NEWS'],
                News::CATEGORY['CREDIT_INFO']
                )
            );

        $news->setCategory($category);

        return $news;
    }

    private static function generateRealName($faker, array $value = array())
    {
        return $realName = isset($value['realName']) ? $value['realName'] : $faker->name;
    }

    private static function generateMember($faker, array $value = array())
    {
        return $member = isset($value['member']) ?
            $value['member'] : \Sdk\Member\Utils\MockFactory::generateMemberObject(
                $faker->numerify(),
                $faker->numerify()
            );
    }

    private static function generateCardId($faker, array $value = array())
    {
        return $cardId = isset($value['cardId']) ?
            $value['cardId'] : $faker->creditCardNumber;
    }

    private static function generateGender($faker, array $value = array())
    {
        return $gender = isset($value['gender']) ? $value['gender'] : '男';
    }

    private static function generateBirthday($faker, array $value = array())
    {
        return $birthday = isset($value['birthday']) ? $value['birthday'] : '0000-00-00';
    }

    private static function generateAddress($faker, array $value = array())
    {
        return $address = isset($value['address']) ? $value['address'] : $faker->name;
    }

    private static function generatePositivePhoto($faker, array $value = array())
    {
        return $positivePhoto = isset($attributes['positivePhoto']) ?
        $attributes['positivePhoto'] : array('name'=>'positivePhoto','identify'=>'positivePhoto.jpg');
    }

    private static function generateReversePhoto($faker, array $value = array())
    {
        return $reversePhoto = isset($attributes['reversePhoto']) ?
        $attributes['reversePhoto'] : array('name'=>'reversePhoto','identify'=>'reversePhoto.jpg');
    }
}
