<?php
namespace Sdk\TradeRecord\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;

use Sdk\TradeRecord\Adapter\TradeRecord\ITradeRecordAdapter;
use Sdk\TradeRecord\Adapter\TradeRecord\TradeRecordRestfulAdapter;
use Sdk\TradeRecord\Adapter\TradeRecord\TradeRecordMockAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class TradeRecordRepository extends Repository implements ITradeRecordAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'TRADE_RECORD_LIST';
    const FETCH_ONE_MODEL_UN = 'TRADE_RECORD_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new TradeRecordRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : ITradeRecordAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : ITradeRecordAdapter
    {
        return new TradeRecordMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
