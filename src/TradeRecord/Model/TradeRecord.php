<?php
namespace Sdk\TradeRecord\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\MemberAccount\Model\MemberAccount;

use Sdk\Deposit\Model\NullDeposit;
use Sdk\TradeRecord\Model\ITradeAble;

class TradeRecord implements IObject, ITradeAble
{
    use Object;
    
    const TRADE_RECORD_TYPES = array(
        'NULL' => 0,
        'DEPOSIT' => 1, //金币充值（收入）
        'COURSE' => 2, //课程购买
        'ORDER' => 3  //服务购买
    );

    // 交易平台
    const TRANSACTION_PLATFORM = array(
        'NULL' => 0,
        "IOS" => 1, //苹果
        "ANDROID" => 2, //安卓
    );

    private $id;
    
    private $referenceId;

    private $reference;

    private $memberAccount;

    private $tradeTime;

    private $type;

    private $tradeMoney;

    private $debtor;

    private $creditor;

    private $transactionPlatform;

    private $cellphone;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->referenceId = 0;
        $this->reference = new NullDeposit();
        $this->memberAccount = new MemberAccount();
        $this->tradeTime = 0;
        $this->type = self::TRADE_RECORD_TYPES['NULL'];
        $this->tradeMoney = 0;
        $this->debtor = '';
        $this->creditor = '';
        $this->transactionPlatform = 0;
        $this->cellphone = 0;
        $this->status = 0;
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->referenceId);
        unset($this->reference);
        unset($this->memberAccount);
        unset($this->tradeTime);
        unset($this->type);
        unset($this->tradeMoney);
        unset($this->debtor);
        unset($this->creditor);
        unset($this->transactionPlatform);
        unset($this->cellphone);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setReference(ITradeAble $reference)
    {
        $this->reference = $reference;
    }

    public function getReference() : ITradeAble
    {
        return $this->reference;
    }
    
    public function setReferenceId(int $referenceId) : void
    {
        $this->referenceId = $referenceId;
    }

    public function getReferenceId() : int
    {
        return $this->referenceId;
    }

    public function setMemberAccount(MemberAccount $memberAccount)
    {
        $this->memberAccount = $memberAccount;
    }

    public function getMemberAccount() : MemberAccount
    {
        return $this->memberAccount;
    }
    
    public function setTradeTime(int $tradeTime) : void
    {
        $this->tradeTime = $tradeTime;
    }

    public function getTradeTime() : int
    {
        return $this->tradeTime;
    }
    
    public function setType(int $type) : void
    {
        $this->type = in_array($type, self::TRADE_RECORD_TYPES) ?
                    $type :
                    self::TRADE_RECORD_TYPES['NULL'];
    }

    public function getType() : int
    {
        return $this->type;
    }
    
    public function setTradeMoney(float $tradeMoney) : void
    {
        $this->tradeMoney = $tradeMoney;
    }

    public function getTradeMoney() : float
    {
        return $this->tradeMoney;
    }
    
    public function setDebtor(string $debtor) : void
    {
        $this->debtor = $debtor;
    }

    public function getDebtor() : string
    {
        return $this->debtor;
    }

    public function setCreditor(string $creditor) : void
    {
        $this->creditor = $creditor;
    }

    public function getCreditor() : string
    {
        return $this->creditor;
    }

    public function setTransactionPlatform(int $transactionPlatform) : void
    {
        $this->transactionPlatform = in_array($transactionPlatform, self::TRANSACTION_PLATFORM) ?
                    $transactionPlatform :
                    self::TRANSACTION_PLATFORM['NULL'];
    }

    public function getTransactionPlatform() : int
    {
        return $this->transactionPlatform;
    }

    public function setCellphone(int $cellphone) : void
    {
        $this->cellphone = $cellphone;
    }

    public function getCellphone() : int
    {
        return $this->cellphone;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
}
