<?php
namespace Sdk\TradeRecord\Model;

use Marmot\Interfaces\INull;

class NullTradeRecord extends TradeRecord implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
