<?php
namespace Sdk\TradeRecord\Translator;

use Sdk\TradeRecord\Model\TradeRecord;

use Marmot\Interfaces\IRestfulTranslator;

class TranslatorFactory
{
    const MAPS = array(
        TradeRecord::TRADE_RECORD_TYPES['DEPOSIT'] =>
        'Sdk\Deposit\Translator\DepositRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['COURSE'] =>
        'Sdk\Order\Translator\OrderRestfulTranslator',
        TradeRecord::TRADE_RECORD_TYPES['ORDER'] =>
        'Sdk\ServiceOrder\Translator\ServiceOrderRestfulTranslator',
    );

    public function getTranslator(string $type) : IRestfulTranslator
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';
        return class_exists($translator) ? new $translator : NullRestfulTranslator::getInstance();
    }
}
