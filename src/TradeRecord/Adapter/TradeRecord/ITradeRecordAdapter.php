<?php
namespace Sdk\TradeRecord\Adapter\TradeRecord;

use Sdk\Common\Adapter\IFetchAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface ITradeRecordAdapter extends IFetchAbleAdapter, IAsyncAdapter
{
}
