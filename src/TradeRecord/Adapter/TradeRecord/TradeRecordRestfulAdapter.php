<?php
namespace Sdk\TradeRecord\Adapter\TradeRecord;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\CommonMapErrorsTrait;

use Sdk\TradeRecord\Translator\TradeRecordRestfulTranslator;
use Sdk\TradeRecord\Model\NullTradeRecord;
use Sdk\TradeRecord\Model\TradeRecord;

use Marmot\Core;

class TradeRecordRestfulAdapter extends GuzzleAdapter implements ITradeRecordAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    private $resource;

    const SCENARIOS = [
        'TRADE_RECORD_LIST'=>[
            'fields'=>[],
            'include'=>'memberAccount,memberAccount.member,reference'
        ],
        'TRADE_RECORD_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'memberAccount,memberAccount.member,reference'
        ]
    ];
    
    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new TradeRecordRestfulTranslator(); 
        $this->resource = 'tradeRecords';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        return $this->commonMapErrors();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullTradeRecord());
    }
}
