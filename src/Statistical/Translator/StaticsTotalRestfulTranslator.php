<?php
namespace Statistical\Translator;

use Sdk\Statistical\Model\NullStatistical;
use Sdk\Statistical\Model\Statistical;
use System\Classes\Translator;

class StaticsByCategoryTotalTranslator extends Translator implements IStatisticalTranslator
{
    public function arrayToObject(array $expression, $statistical = null)
    {
        unset($statistical);
        unset($expression);
        return new NullStatistical();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }
    
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($statistical, array $keys = array())
    {
        if (!$statistical instanceof Statistical) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'result',
            );
        }

        $expression = array();
        if (in_array('result', $keys)) {
            $result = $statistical->getResult();

            $expression = array(
                'columns' => array('category', 'total'),
                'rows' => $rows
            );
        }

        return $expression;
    }
}
