<?php
namespace Statistical\Translator;

use System\Classes\Translator;
use Marmot\Core;

class NullStatisticalTranslator extends Translator implements IStatisticalTranslator
{
    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        Core::setLastError(RESOURCE_NOT_EXIST);
        return array();
    }

    public function arrayToObject(array $expression, $object = null)
    {
        unset($expression);
        unset($object);
        Core::setLastError(RESOURCE_NOT_EXIST);
    }

    public function objectToArray($object)
    {
        unset($object);
        Core::setLastError(RESOURCE_NOT_EXIST);
    }
}
