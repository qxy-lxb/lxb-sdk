<?php
namespace Statistical\Translator;

interface IStatisticalTranslator
{
    public function arrayToObject(array $expression, $object = null);
    public function objectToArray($object);
    public function arrayToObjects(array $expression) : array;
}
