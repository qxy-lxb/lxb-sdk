<?php
namespace Sdk\Statistical\Model;

class Statistical
{
    private $id;

    private $result;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->result = array();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->result);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setResult(array $result)
    {
        $this->result = $result;
    }

    public function getResult() : array
    {
        return $this->result;
    }
}
