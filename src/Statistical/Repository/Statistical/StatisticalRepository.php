<?php
namespace Sdk\Statistical\Repository\Statistical;

use Sdk\Statistical\Adapter\Statistical\IStatisticalAdapter;
use Sdk\Statistical\Model\Statistical;

class StatisticalRepository
{
    private $adapter;

    public function __construct(IStatisticalAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function setAdapter(IStatisticalAdapter $adapter) : void
    {
        $this->adapter = $adapter;
    }

    public function getAdapter() : IStatisticalAdapter
    {
        return $this->adapter;
    }

    public function analyse(array $filter = array()) : Statistical
    {
        return $this->getAdapter()->analyse($filter);
    }

    public function analyseAsync(array $filter = array())
    {
        return $this->getAdapter()->analyseAsync($filter);
    }
}
