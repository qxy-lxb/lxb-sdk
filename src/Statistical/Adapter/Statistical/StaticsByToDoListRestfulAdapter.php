<?php
namespace Sdk\Statistical\Adapter\Statistical;

use Sdk\Statistical\Model\NullStatistical;
use Sdk\Statistical\Model\Statistical;

class StaticsByToDoListRestfulAdapter extends StatisticalRestfulAdapter
{
    public function analyse($filter) : Statistical
    {
        $this->get(
            'statisticals/staticsByToDoList',
            array('filter'=>$filter)
        );

        return $this->isSuccess() ? $this->translateToObject() : new NullStatistical();
    }

    public function analyseAsync()
    {
        return $this->getAsync('statisticals/staticsByToDoList');
    }
}
