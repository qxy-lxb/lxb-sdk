<?php
namespace Sdk\Statistical\Adapter\Statistical;

use Sdk\Statistical\Model\NullStatistical;
use Sdk\Statistical\Model\Statistical;
use Marmot\Interfaces\INull;
use Marmot\Core;

class NullStatisticalAdapter implements INull
{
    public function analyse(array $filter = array()) : Statistical
    {
        unset($filter);
        return new NullStatistical();
    }
}
