<?php
namespace Sdk\Statistical\Adapter\Statistical;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;

interface IStatisticalAdapter extends IFetchAbleAdapter, IAsyncAdapter
{
}
