<?php
namespace Sdk\Statistical\Adapter\Statistical;

use Sdk\Statistical\Model\NullStatistical;
use Sdk\Statistical\Model\Statistical;

class StaticsByMemberRestfulAdapter extends StatisticalRestfulAdapter
{
    const SOURCE_MAPPING = array(
        3 => 'statisticals/staticsMemberByMonth',
        4 => 'statisticals/staticsMemberByYear',
    );

    public function getSource($filter): string
    {
        $scene = $filter['scene'];
        $resource = self::SOURCE_MAPPING[$scene] ?
        self::SOURCE_MAPPING[$scene] :
        self::SOURCE_MAPPING[3];

        return $resource;
    }

    public function analyse($filter) : Statistical
    {
        $source = $this->getSource($filter);

        $this->get(
            $source,
            array('filter'=>$filter)
        );

        return $this->isSuccess() ? $this->translateToObject() : new NullStatistical();
    }

    public function analyseAsync($filter)
    {
        $source = $this->getSource($filter);
        
        return $this->getAsync(
            $source,
            array('filter'=>$filter)
        );
    }
}
