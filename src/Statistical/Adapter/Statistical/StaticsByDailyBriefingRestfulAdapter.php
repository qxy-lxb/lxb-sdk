<?php
namespace Sdk\Statistical\Adapter\Statistical;

use Sdk\Statistical\Model\NullStatistical;
use Sdk\Statistical\Model\Statistical;

class StaticsByDailyBriefingRestfulAdapter extends StatisticalRestfulAdapter
{
    public function analyse($filter) : Statistical
    {
        $this->get(
            'statisticals/staticsByDailyBriefing',
            array('filter'=>$filter)
        );

        return $this->isSuccess() ? $this->translateToObject() : new NullStatistical();
    }

    public function analyseAsync()
    {
        return $this->getAsync('statisticals/staticsByDailyBriefing');
    }
}
