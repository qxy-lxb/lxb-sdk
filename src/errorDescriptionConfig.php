<?php

return array(
    CSRF_VERIFY_FAILURE=>
        array(
            'id'=>CSRF_VERIFY_FAILURE,
            'link'=>'',
            'status'=>403,
            'code'=>'CSRF_VERIFY_FAILURE',
            'title'=>'csrf验证失效',
            'detail'=>'csrf验证失效,请刷新页面',
            'source'=>array(
            ),
            'meta'=>array()
        ),
    AFS_VERIFY_FAILURE=>
        array(
            'id'=>AFS_VERIFY_FAILURE,
            'link'=>'',
            'status'=>403,
            'code'=>'AFS_VERIFY_FAILURE',
            'title'=>'滑动验证失效',
            'detail'=>'滑动验证失效,请刷新页面',
            'source'=>array(
            ),
            'meta'=>array()
        ),
    NEED_SIGNIN=>
        array(
            'id'=>NEED_SIGNIN,
            'link'=>'',
            'status'=>403,
            'code'=>'NEED_SIGNIN',
            'title'=>'用户未登录',
            'detail'=>'用户未登录',
            'source'=>array(
            ),
            'meta'=>array()
        ),
    SMS_SEND_TOO_QUICK=>
        array(
            'id'=>SMS_SEND_TOO_QUICK,
            'link'=>'',
            'status'=>403,
            'code'=>'SMS_SEND_TOO_QUICK',
            'title'=>'短信发送太频繁',
            'detail'=>'短信发送太频繁',
            'source'=>array(
            ),
            'meta'=>array()
        ),
    HASH_INVALID=>
        array(
            'id'=>HASH_INVALID,
            'link'=>'',
            'status'=>403,
            'code'=>'HASH_INVALID',
            'title'=>'哈希无效',
            'detail'=>'哈希无效',
            'source'=>array(
            ),
            'meta'=>array()
        ),
    PARAMETER_IS_UNIQUE=>
        array(
            'id'=>PARAMETER_IS_UNIQUE,
            'link'=>'',
            'status'=>409,
            'code'=>'PARAMETER_IS_UNIQUE',
            'title'=>'数据重复',
            'detail'=>'表述传输了一个已经存在的数据,但是该数据不能重复',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    PARAMETER_FORMAT_INCORRECT=>
        array(
            'id'=>PARAMETER_FORMAT_INCORRECT,
            'link'=>'',
            'status'=>403,
            'code'=>'PARAMETER_FORMAT_INCORRECT',
            'title'=>'数据格式不正确',
            'detail'=>'表述传输了一个格式不正确的字段',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    PARAMETER_IS_EMPTY=>
        array(
            'id'=>PARAMETER_IS_EMPTY,
            'link'=>'',
            'status'=>403,
            'code'=>'PARAMETER_IS_EMPTY',
            'title'=>'数据不能为空',
            'detail'=>'表述传输了一个空数据,但是该数据不能为空',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    IMAGE_FORMAT_ERROR=>
        array(
            'id'=>IMAGE_FORMAT_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'IMAGE_FORMAT_ERROR',
            'title'=>'照片格式不正确，请重新上传，正确格式为jpg,png',
            'detail'=>'照片格式不正确，请重新上传，正确格式为jpg,png',
            'source'=>array(
                'pointer'=>'image'
            ),
            'meta'=>array()
        ),
    ATTACHMENT_FORMAT_ERROR=>
        array(
            'id'=>ATTACHMENT_FORMAT_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'ATTACHMENT_FORMAT_ERROR',
            'title'=>'附件格式错误',
            'detail'=>'附件格式错误，附件格式为zip, doc, docx，xls, xlsx，ppt',
            'source'=>array(
                'pointer'=>'attachments'
            ),
            'meta'=>array()
        ),
    REAL_NAME_FORMAT_ERROR=>
        array(
            'id'=>REAL_NAME_FORMAT_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'REAL_NAME_FORMAT_ERROR',
            'title'=>'姓名格式错误',
            'detail'=>'请输入正确的姓名',
            'source'=>array(
                'pointer'=>'attachments'
            ),
            'meta'=>array()
        ),
    CELLPHONE_FORMAT_ERROR=>
        array(
            'id'=>CELLPHONE_FORMAT_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'CELLPHONE_FORMAT_ERROR',
            'title'=>'手机号格式错误',
            'detail'=>'请填写正确的手机号码',
            'source'=>array(
                'pointer'=>'attachments'
            ),
            'meta'=>array()
       ),
    PASSWORD_FORMAT_ERROR=>
        array(
            'id'=>PASSWORD_FORMAT_ERROR,
            'link'=>'',
            'status'=>403,PASSWORD_FORMAT_ERROR,
            'code'=>'PASSWORD_FORMAT_ERROR',
            'title'=>'密码格式不正确',
            'detail'=>'密码长度为8-30位，必须包含数字和字母特殊字符',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    CARDID_FORMAT_ERROR=>
        array(
            'id'=>CARDID_FORMAT_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'CARDID_FORMAT_ERROR',
            'title'=>'身份号格式不正确',
            'detail'=>'请填写正确的身份证号',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    OLD_PASSWORD_INCORRECT=>
        array(
            'id'=>OLD_PASSWORD_INCORRECT,
            'link'=>'',
            'status'=>403,
            'code'=>'OLD_PASSWORD_INCORRECT',
            'title'=>'旧密码不正确',
            'detail'=>'旧密码不正确',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    PASSWORD_INCORRECT=>
        array(
            'id'=>PASSWORD_INCORRECT,
            'link'=>'',
            'status'=>403,
            'code'=>'PASSWORD_INCORRECT',
            'title'=>'账号密码不匹配',
            'detail'=>'账号和密码不匹配，请检查您的输入',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    NAME_FORMAT_ERROR=>
        array(
            'id'=>NAME_FORMAT_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'NAME_FORMAT_ERROR',
            'title'=>'名称格式不正确',
            'detail'=>'名称格式错误',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    NICK_NAME_FORMAT_ERROR=>
        array(
            'id'=>NICK_NAME_FORMAT_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'NICK_NAME_FORMAT_ERROR',
            'title'=>'昵称格式不正确',
            'detail'=>'请输入2-10位的昵称',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    CELLPHONE_EXIST=>
        array(
            'id'=>CELLPHONE_EXIST,
            'link'=>'',
            'status'=>403,
            'code'=>'CELLPHONE_EXIST',
            'title'=>'手机号已存在',
            'detail'=>'该手机号已存在，请重新输入',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
    ),
    CELLPHONE_NOT_EXIST=>
        array(
            'id'=>CELLPHONE_NOT_EXIST,
            'link'=>'',
            'status'=>403,
            'code'=>'CELLPHONE_NOT_EXIST',
            'title'=>'账号不存在',
            'detail'=>'账号不存在，请检查您的输入',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    CAPTCHA_ERROR=>
        array(
            'id'=>CAPTCHA_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'CAPTCHA_ERROR',
            'title'=>'验证码不正确',
            'detail'=>'验证码有误，请重新填写',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
    ),
    INCONSISTENT_PASSWORD=>
        array(
            'id'=>INCONSISTENT_PASSWORD,
            'link'=>'',
            'status'=>403,
            'code'=>'INCONSISTENT_PASSWORD',
            'title'=>'确认密码必须和密码一致',
            'detail'=>'确认密码必须和密码一致',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
    ),
    ADDRESS_FORMAT_ERROR=>
        array(
            'id'=>ADDRESS_FORMAT_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'ADDRESS_FORMAT_ERROR',
            'title'=>'详细地址格式不正确',
            'detail'=>'详细地址格式错误',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    GENDER_TYPE_NOT_EXIST=>
        array(
            'id'=>GENDER_TYPE_NOT_EXIST,
            'link'=>'',
            'status'=>403,
            'code'=>'GENDER_TYPE_NOT_EXIST',
            'title'=>'性别类型不存在',
            'detail'=>'性别类型不存在',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    ROLE_FORMAT_ERROR=>
        array(
            'id'=>ROLE_FORMAT_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'ROLE_FORMAT_ERROR',
            'title'=>'人员角色格式不正确',
            'detail'=>'人员角色格式不正确',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    ORGANIZATION_TYPE_NOT_EXIST=>
        array(
            'id'=>ORGANIZATION_TYPE_NOT_EXIST,
            'link'=>'',
            'status'=>403,
            'code'=>'ORGANIZATION_TYPE_NOT_EXIST',
            'title'=>'所属机构名称类型不存在',
            'detail'=>'所属机构名称类型不存在',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    PASSWORD_INCONSISTENCY=>
        array(
            'id'=>PASSWORD_INCONSISTENCY,
            'link'=>'',
            'status'=>403,
            'code'=>'PASSWORD_INCONSISTENCY',
            'title'=>'密码与确认密码不一致',
            'detail'=>'密码与确认密码不一致',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    STATUS_ALREADY_DISABLED=>
        array(
            'id'=>STATUS_ALREADY_DISABLED,
            'link'=>'',
            'status'=>403,
            'code'=>'STATUS_ALREADY_DISABLED',
            'title'=>'状态已禁用',
            'detail'=>'状态已禁用',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    STATUS_ALREADY_ENABLED=>
        array(
            'id'=>STATUS_ALREADY_ENABLED,
            'link'=>'',
            'status'=>403,
            'code'=>'STATUS_ALREADY_ENABLED',
            'title'=>'状态已启用',
            'detail'=>'状态已启用',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    VOUCHER_FORMAT_ERROR=>
        array(
            'id'=>VOUCHER_FORMAT_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'VOUCHER_FORMAT_ERROR',
            'title'=>'凭证格式不正确',
            'detail'=>'凭证格式不正确',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    ENTERPRISE_NAME_FORMAT_ERROR => array(
        'id' => ENTERPRISE_NAME_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'ENTERPRISE_NAME_FORMAT_ERROR',
        'title' => '企业名称格式不正确',
        'detail' => '企业名称格式不正确',
        'source' => array(
            'pointer' => 'name'
        ),
        'meta' => array()
    ),
    CODE_FORMAT_ERROR => array(
        'id' => CODE_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'CODE_FORMAT_ERROR',
        'title' => '统一社会信用代码格式不正确',
        'detail' => '统一社会信用代码格式不正确',
        'source' => array(
            'pointer' => 'unifiedSocialCreditCode'
        ),
        'meta' => array()
    ),
    PRINCIPAL_FORMAT_ERROR => array(
        'id' => PRINCIPAL_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'PRINCIPAL_FORMAT_ERROR',
        'title' => '法定代表人格式不正确',
        'detail' => '法定代表人格式不正确',
        'source' => array(
            'pointer' => 'principal'
        ),
        'meta' => array()
    ),
    CONTACTS_FORMAT_ERROR => array(
        'id' => CONTACTS_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'CONTACTS_FORMAT_ERROR',
        'title' => '联系人格式不正确',
        'detail' => '联系人格式不正确',
        'source' => array(
            'pointer' => 'contacts'
        ),
        'meta' => array()
    ),
    PHONE_FORMAT_ERROR => array(
        'id' => PHONE_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'PHONE_FORMAT_ERROR',
        'title' => '联系电话格式不正确',
        'detail' => '联系电话格式不正确',
        'source' => array(
            'pointer' => 'phone'
        ),
        'meta' => array()
    ),
    AUTH_LETTER_FORMAT_ERROR => array(
        'id' => AUTH_LETTER_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'AUTH_LETTER_FORMAT_ERROR',
        'title' => '企业授权函格式不正确',
        'detail' => '企业授权函格式不正确',
        'source' => array(
            'pointer' => 'authLetter'
        ),
        'meta' => array()
    ),
    REGISTERED_DATE_FORMAT_ERROR => array(
        'id' => REGISTERED_DATE_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'REGISTERED_DATE_FORMAT_ERROR',
        'title' => '注册时间格式不正确',
        'detail' => '注册时间格式不正确',
        'source' => array(
            'pointer' => 'registeredDate'
        ),
        'meta' => array()
    ),
    CAPITAL_FORMAT_ERROR => array(
        'id' => CAPITAL_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'CAPITAL_FORMAT_ERROR',
        'title' => '注册资本格式不正确',
        'detail' => '注册资本格式不正确',
        'source' => array(
            'pointer' => 'capital'
        ),
        'meta' => array()
    ),
    ENTERPRISE_MEMBER_FORMAT_ERROR => array(
        'id' => ENTERPRISE_MEMBER_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'ENTERPRISE_MEMBER_FORMAT_ERROR',
        'title' => '认证用户格式不正确',
        'detail' => '认证用户格式不正确',
        'source' => array(
            'pointer' => 'member'
        ),
        'meta' => array()
    ),
    GENDER_FORMAT_ERROR => array(
        'id' => GENDER_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'GENDER_FORMAT_ERROR',
        'title' => '性别格式不正确',
        'detail' => '性别格式不正确',
        'source' => array(
            'pointer' => 'gender'
        ),
        'meta' => array()
    ),
    REJECTREASON_FORMAT_ERROR=>
        array(
            'id'=>REJECTREASON_FORMAT_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'REJECTREASON_FORMAT_ERROR',
            'title'=>'企业驳回原因格式不正确',
            'detail'=>'请输入2-50位的字符',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    NEWS_TYPE_NOT_EXIST=>
        array(
            'id'=>NEWS_TYPE_NOT_EXIST,
            'link'=>'',
            'status'=>403,
            'code'=>'NEWS_TYPE_NOT_EXIST',
            'title'=>'新闻类型不存在',
            'detail'=>'新闻类型不存在',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    TITLE_FORMAT_ERROR=>
        array(
            'id'=>TITLE_FORMAT_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'TITLE_FORMAT_ERROR',
            'title'=>'标题格式不正确',
            'detail'=>'标题格式错误，长度范围应为1-40位',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
        ),
    DATE_FORMAT_ERROR=>
        array(
            'id'=>DATE_FORMAT_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'DATE_FORMAT_ERROR',
            'title'=>'日期格式不正确',
            'detail'=>'日期格式错误',
            'source'=>array(
                'pointer'=>''
            ),
            'meta'=>array()
    ),
    ORGANIZATION_NAME_FORMAT_ERROR => array(
        'id' => ORGANIZATION_NAME_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'ORGANIZATION_NAME_FORMAT_ERROR',
        'title' => '机构名称格式不正确',
        'detail' => '机构名称格式不正确',
        'source' => array(
            'pointer' => 'name'
        ),
        'meta' => array()
    ),
    ORGANIZATION_CONTACTS_FORMAT_ERROR => array(
        'id' => ORGANIZATION_CONTACTS_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'ORGANIZATION_CONTACTS_FORMAT_ERROR',
        'title' => '联系人格式不正确',
        'detail' => '联系人格式不正确',
        'source' => array(
            'pointer' => 'contacts'
        ),
        'meta' => array()
    ),
    ORGANIZATION_PHONE_FORMAT_ERROR => array(
        'id' => ORGANIZATION_PHONE_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'ORGANIZATION_PHONE_FORMAT_ERROR',
        'title' => '联系电话格式不正确',
        'detail' => '联系电话格式不正确',
        'source' => array(
            'pointer' => 'phone'
        ),
        'meta' => array()
    ),
    ORGANIZATION_URL_FORMAT_ERROR => array(
        'id' => ORGANIZATION_URL_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'ORGANIZATION_URL_FORMAT_ERROR',
        'title' => '官网地址格式不正确',
        'detail' => '官网地址格式不正确',
        'source' => array(
            'pointer' => 'url'
        ),
        'meta' => array()
    ),
    SETTLED_DATE_FORMAT_ERROR => array(
        'id' => SETTLED_DATE_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'SETTLED_DATE_FORMAT_ERROR',
        'title' => '入驻日期格式不正确',
        'detail' => '入驻日期格式不正确',
        'source' => array(
            'pointer' => 'settledDate'
        ),
        'meta' => array()
    ),
    END_DATE_FORMAT_ERROR => array(
        'id' => END_DATE_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'END_DATE_FORMAT_ERROR',
        'title' => '截止日期格式不正确',
        'detail' => '截止日期格式不正确',
        'source' => array(
            'pointer' => 'endDate'
        ),
        'meta' => array()
    ),
    CONTRACT_FORMAT_ERROR => array(
        'id' => CONTRACT_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'CONTRACT_FORMAT_ERROR',
        'title' => '合同文件格式不正确',
        'detail' => '合同文件格式不正确',
        'source' => array(
            'pointer' => 'contract'
        ),
        'meta' => array()
    ),
    ORGANIZATION_CATEGORY_FORMAT_ERROR => array(
        'id' => ORGANIZATION_CATEGORY_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'ORGANIZATION_CATEGORY_FORMAT_ERROR',
        'title' => '机构分类格式不正确',
        'detail' => '机构分类格式不正确',
        'source' => array(
            'pointer' => 'category'
        ),
        'meta' => array()
    ),
    ORGANIZATION_NAME_EXIST => array(
        'id' => ORGANIZATION_NAME_EXIST,
        'link' => '',
        'status' => 403,
        'code' => 'ORGANIZATION_NAME_EXIST',
        'title' => '机构名称存在',
        'detail' => '机构名称存在',
        'source' => array(
            'pointer' => 'name'
        ),
        'meta' => array()
    ),
    REPAIRRECORD_TYPE_NOT_EXIST => array(
        'id' => REPAIRRECORD_TYPE_NOT_EXIST,
        'link' => '',
        'status' => 403,
        'code' => 'REPAIRRECORD_TYPE_NOT_EXIST',
        'title' => '行政处罚类型不存在',
        'detail' => '行政处罚类型不存在',
        'source' => array(
            'pointer' => 'severity'
        ),
        'meta' => array()
    ),
    REPAIR_RECORD_REJECTREASON_FORMAT_ERROR => array(
        'id' => REPAIR_RECORD_REJECTREASON_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'REPAIR_RECORD_REJECTREASON_FORMAT_ERROR',
        'title' => '信用修复驳回原因格式不正确',
        'detail' => '信用修复驳回原因格式不正确',
        'source' => array(
            'pointer' => 'rejectReason'
        ),
        'meta' => array()
    ),
    DISHONESTY_INFO_FORMAT_ERROR => array(
        'id' => DISHONESTY_INFO_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'DISHONESTY_INFO_FORMAT_ERROR',
        'title' => '失信信息格式不正确',
        'detail' => '失信信息格式不正确',
        'source' => array(
            'pointer' => 'dishonestyInfo'
        ),
        'meta' => array()
    ),
    REPAIR_RECORD_DELIVER_STATUS_ALREADY_SUBMITTED => array(
        'id' => REPAIR_RECORD_DELIVER_STATUS_ALREADY_SUBMITTED,
        'link' => '',
        'status' => 403,
        'code' => 'REPAIR_RECORD_DELIVER_STATUS_ALREADY_SUBMITTED',
        'title' => '递交状态已递交',
        'detail' => '递交状态已递交',
        'source' => array(
            'pointer' => 'deliverStatus'
        ),
        'meta' => array()
    ),
    REPAIR_RECORD_DETERMINE_STATUS_ALREADY_DETERMINED => array(
        'id' => REPAIR_RECORD_DETERMINE_STATUS_ALREADY_DETERMINED,
        'link' => '',
        'status' => 403,
        'code' => 'REPAIR_RECORD_DETERMINE_STATUS_ALREADY_DETERMINED',
        'title' => '判定状态已判定',
        'detail' => '判定状态已判定',
        'source' => array(
            'pointer' => 'determineStatus'
        ),
        'meta' => array()
    ),
    REPAIR_RECORD_APPLY_STATUS_NOT_UNSUBMITTED => array(
        'id' => REPAIR_RECORD_APPLY_STATUS_NOT_UNSUBMITTED,
        'link' => '',
        'status' => 403,
        'code' => 'REPAIR_RECORD_APPLY_STATUS_NOT_UNSUBMITTED',
        'title' => '审核状态非未提交审核',
        'detail' => '审核状态非未提交审核',
        'source' => array(
            'pointer' => 'applyStatus'
        ),
        'meta' => array()
    ),
    APPLY_STATUS_NOT_APPROVED => array(
        'id' => APPLY_STATUS_NOT_APPROVED,
        'link' => '',
        'status' => 403,
        'code' => 'APPLY_STATUS_NOT_APPROVED',
        'title' => '审核状态非审核通过',
        'detail' => '审核状态非审核通过',
        'source' => array(
            'pointer' => 'applyStatus'
        ),
        'meta' => array()
    ),
    VIDEO_NAME_FORMAT_ERROR => array(
        'id' => VIDEO_NAME_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'VIDEO_NAME_FORMAT_ERROR',
        'title' => '视频名称格式不正确',
        'detail' => '视频名称格式不正确',
        'source' => array(
            'pointer' => 'name'
        ),
        'meta' => array()
    ),
    VIDEO_STUDY_TIME_FORMAT_ERROR => array(
        'id' => VIDEO_STUDY_TIME_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'VIDEO_STUDY_TIME_FORMAT_ERROR',
        'title' => '视频学时格式不正确',
        'detail' => '视频学时格式不正确',
        'source' => array(
            'pointer' => 'studyTime'
        ),
        'meta' => array()
    ),
    VIDEO_TIME_FORMAT_ERROR => array(
        'id' => VIDEO_TIME_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'VIDEO_TIME_FORMAT_ERROR',
        'title' => '视频时长格式不正确',
        'detail' => '视频时长格式不正确',
        'source' => array(
            'pointer' => 'videoTime'
        ),
        'meta' => array()
    ),
    VIDEO_FORMAT_ERROR => array(
        'id' => VIDEO_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'VIDEO_FORMAT_ERROR',
        'title' => '视频格式不正确',
        'detail' => '视频格式不正确',
        'source' => array(
            'pointer' => 'video'
        ),
        'meta' => array()
    ),
    VIDEO_CATEGORY_FORMAT_ERROR => array(
        'id' => VIDEO_CATEGORY_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'VIDEO_CATEGORY_FORMAT_ERROR',
        'title' => '视频对应类型不正确',
        'detail' => '视频对应类型不正确',
        'source' => array(
            'pointer' => 'category'
        ),
        'meta' => array()
    ),
    EXAMINATIONS_SUBJECT_FORMAT_ERROR => array(
        'id' => EXAMINATIONS_SUBJECT_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'EXAMINATIONS_SUBJECT_FORMAT_ERROR',
        'title' => '题目格式不正确',
        'detail' => '题目格式不正确',
        'source' => array(
            'pointer' => 'subject'
        ),
        'meta' => array()
    ),
    EXAMINATIONS_OPTIONS_FORMAT_ERROR => array(
        'id' => EXAMINATIONS_OPTIONS_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'EXAMINATIONS_OPTIONS_FORMAT_ERROR',
        'title' => '选项格式不正确',
        'detail' => '选项格式不正确',
        'source' => array(
            'pointer' => 'options'
        ),
        'meta' => array()
    ),
    EXAMINATIONS_ANSWERS_FORMAT_ERROR => array(
        'id' => EXAMINATIONS_ANSWERS_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'EXAMINATIONS_ANSWERS_FORMAT_ERROR',
        'title' => '答案格式不正确',
        'detail' => '答案格式不正确',
        'source' => array(
            'pointer' => 'answers'
        ),
        'meta' => array()
    ),
    EXAMINATIONS_OPTIONS_TYPE_FORMAT_ERROR => array(
        'id' => EXAMINATIONS_OPTIONS_TYPE_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'EXAMINATIONS_OPTIONS_TYPE_FORMAT_ERROR',
        'title' => '选项类型不正确',
        'detail' => '选项类型不正确',
        'source' => array(
            'pointer' => 'optionsType'
        ),
        'meta' => array()
    ),
    COURSE_NUMBER_FORMAT_ERROR => array(
        'id' => COURSE_NUMBER_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'COURSE_NUMBER_FORMAT_ERROR',
        'title' => '课程编号格式不正确',
        'detail' => '课程编号格式不正确',
        'source' => array(
            'pointer' => 'number'
        ),
        'meta' => array()
    ),
    COURSE_NAME_FORMAT_ERROR => array(
        'id' => COURSE_NAME_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'COURSE_NAME_FORMAT_ERROR',
        'title' => '课程名称格式不正确,应为1-50位',
        'detail' => '课程名称格式不正确,应为1-50位',
        'source' => array(
            'pointer' => 'name'
        ),
        'meta' => array()
    ),
    COURSE_CATEGORY_FORMAT_ERROR => array(
        'id' => COURSE_CATEGORY_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'COURSE_CATEGORY_FORMAT_ERROR',
        'title' => '课程对应类型不正确',
        'detail' => '课程对应类型不正确',
        'source' => array(
            'pointer' => 'category'
        ),
        'meta' => array()
    ),
    COURSE_PRICE_FORMAT_ERROR => array(
        'id' => COURSE_PRICE_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'COURSE_PRICE_FORMAT_ERROR',
        'title' => '课程价格格式不正确',
        'detail' => '课程价格格式不正确',
        'source' => array(
            'pointer' => 'price'
        ),
        'meta' => array()
    ),
    COURSE_STUDYTIME_FORMAT_ERROR => array(
        'id' => COURSE_STUDYTIME_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'COURSE_STUDYTIME_FORMAT_ERROR',
        'title' => '课程学时格式不正确,至少为3个学时',
        'detail' => '课程学时格式不正确,至少为3个学时',
        'source' => array(
            'pointer' => 'studyTime'
        ),
        'meta' => array()
    ),
    COURSE_PASSLINE_FORMAT_ERROR => array(
        'id' => COURSE_PASSLINE_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'COURSE_PASSLINE_FORMAT_ERROR',
        'title' => '课程计分规则格式不正确',
        'detail' => '课程计分规则格式不正确',
        'source' => array(
            'pointer' => 'passLine'
        ),
        'meta' => array()
    ),
    COURSE_EXPIRATION_DATE_FORMAT_ERROR => array(
        'id' => COURSE_EXPIRATION_DATE_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'COURSE_EXPIRATION_DATE_FORMAT_ERROR',
        'title' => '课程有效期格式不正确',
        'detail' => '课程有效期格式不正确',
        'source' => array(
            'pointer' => 'expirationDate'
        ),
        'meta' => array()
    ),
    COURSE_VIDEOS_FORMAT_ERROR => array(
        'id' => COURSE_VIDEOS_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'COURSE_VIDEOS_FORMAT_ERROR',
        'title' => '课程视频格式不正确',
        'detail' => '课程视频格式不正确',
        'source' => array(
            'pointer' => 'videos'
        ),
        'meta' => array()
    ),
    COURSE_SYNOPSIS_FORMAT_ERROR => array(
        'id' => COURSE_SYNOPSIS_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'COURSE_SYNOPSIS_FORMAT_ERROR',
        'title' => '课程简介格式不正确',
        'detail' => '课程简介格式不正确',
        'source' => array(
            'pointer' => 'synopsis'
        ),
        'meta' => array()
    ),
    IS_REQUIRED_EXAM_FORMAT_ERROR => array(
        'id' => IS_REQUIRED_EXAM_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'IS_REQUIRED_EXAM_FORMAT_ERROR',
        'title' => '是否需要考试格式不正确',
        'detail' => '是否需要考试格式不正确',
        'source' => array(
            'pointer' => 'isRequiredExam'
        ),
        'meta' => array()
    ),
    COURSE_STATUS_ALREADY_ON => array(
        'id' => COURSE_STATUS_ALREADY_ON,
        'link' => '',
        'status' => 403,
        'code' => 'COURSE_STATUS_ALREADY_ON',
        'title' => '课程状态已上架',
        'detail' => '课程状态已上架',
        'source' => array(
            'pointer' => 'status'
        ),
        'meta' => array()
    ),
    COURSE_STATUS_ALREADY_OFF => array(
        'id' => COURSE_STATUS_ALREADY_OFF,
        'link' => '',
        'status' => 403,
        'code' => 'COURSE_STATUS_ALREADY_OFF',
        'title' => '课程状态已下架',
        'detail' => '课程状态已下架',
        'source' => array(
            'pointer' => 'status'
        ),
        'meta' => array()
    ),
    COURSE_OFF_REASON_FORMAT_ERROR => array(
        'id' => COURSE_OFF_REASON_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'COURSE_OFF_REASON_FORMAT_ERROR',
        'title' => '课程下架原因格式不正确',
        'detail' => '课程下架原因格式不正确',
        'source' => array(
            'pointer' => 'offReason'
        ),
        'meta' => array()
    ),
    INITIATOR_FORMAT_ERROR => array(
        'id' => INITIATOR_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'INITIATOR_FORMAT_ERROR',
        'title' => '培训发起方格式错误',
        'detail' => '培训发起方格式错误',
        'source' => array(
            'pointer' => ''
        ),
        'meta' => array()
    ),
    RELEVANT_INFORMATION_FORMAT_ERROR => array(
        'id' => RELEVANT_INFORMATION_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'RELEVANT_INFORMATION_FORMAT_ERROR',
        'title' => '受培训相关资料格式错误',
        'detail' => '受培训相关资料格式错误',
        'source' => array(
            'pointer' => ''
        ),
        'meta' => array()
    ),
    COURSE_REJECTREASON_FORMAT_ERROR => array(
        'id' => COURSE_REJECTREASON_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'COURSE_REJECTREASON_FORMAT_ERROR',
        'title' => '课程驳回原因格式不正确',
        'detail' => '课程驳回原因格式不正确',
        'source' => array(
            'pointer' => 'rejectReason'
        ),
        'meta' => array()
    ),
    COURSE_STUDYTIME_DISSATISFACTION_FORMAT_ERROR => array(
        'id' => COURSE_STUDYTIME_DISSATISFACTION_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'COURSE_STUDYTIME_DISSATISFACTION_FORMAT_ERROR',
        'title' => '课程学时不满足3学时',
        'detail' => '课程学时不满足3学时',
        'source' => array(
            'pointer' => 'studyTime'
        ),
        'meta' => array()
    ),
    ID_FORMAT_ERROR => array(
        'id' => ID_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'ID_FORMAT_ERROR',
        'title' => 'id格式不正确',
        'detail' => 'id格式不正确',
        'source' => array(
            'pointer' => ''
        ),
        'meta' => array()
    ),
    PRICE_FORMAT_ERROR => array(
        'id' => PRICE_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'PRICE_FORMAT_ERROR',
        'title' => '价格格式不正确',
        'detail' => '价格格式不正确',
        'source' => array(
            'pointer' => 'price'
        ),
        'meta' => array()
    ),
    COURSE_EXAMINATION_SIZE_FORMAT_ERROR => array(
        'id' => COURSE_EXAMINATION_SIZE_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'COURSE_EXAMINATION_SIZE_FORMAT_ERROR',
        'title' => '课程试题不足40道',
        'detail' => '课程试题不足40道',
        'source' => array(
            'pointer' => 'examination'
        ),
        'meta' => array()
    ),
    EXAM_PAPER_FORMAT_ERROR => array(
        'id' => EXAM_PAPER_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'EXAM_PAPER_FORMAT_ERROR',
        'title' => '考试试卷格式不正确',
        'detail' => '考试试卷格式不正确',
        'source' => array(
            'pointer' => 'examPaper'
        ),
        'meta' => array()
    ),
    LEARNED_PROGRESS_FORMAT_ERROR => array(
        'id' => LEARNED_PROGRESS_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'LEARNED_PROGRESS_FORMAT_ERROR',
        'title' => '已学习时长进度格式不正确',
        'detail' => '已学习时长进度格式不正确',
        'source' => array(
            'pointer' => 'learnedProgress'
        ),
        'meta' => array()
    ),
    TRAINING_RECORD_FORMAT_ERROR => array(
        'id' => TRAINING_RECORD_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'TRAINING_RECORD_FORMAT_ERROR',
        'title' => '关联培训记录格式不正确',
        'detail' => '关联培训记录格式不正确',
        'source' => array(
            'pointer' => 'trainingRecord'
        ),
        'meta' => array()
    ),
    TRAINING_RECORD_ALREADY_PASS => array(
        'id' => TRAINING_RECORD_ALREADY_PASS,
        'link' => '',
        'status' => 403,
        'code' => 'TRAINING_RECORD_ALREADY_PASS',
        'title' => '本次培训进度已通过',
        'detail' => '本次培训进度已通过',
        'source' => array(
            'pointer' => ''
        ),
        'meta' => array()
    ),
    TRAINING_RECORD_ALREADY_FINISHED => array(
        'id' => TRAINING_RECORD_ALREADY_FINISHED,
        'link' => '',
        'status' => 403,
        'code' => 'TRAINING_RECORD_ALREADY_FINISHED',
        'title' => '本次培训进度已完成',
        'detail' => '本次培训进度已完成',
        'source' => array(
            'pointer' => ''
        ),
        'meta' => array()
    ),
    TRAINING_RECORD_ALREADY_LEARNED => array(
        'id' => TRAINING_RECORD_ALREADY_LEARNED,
        'link' => '',
        'status' => 403,
        'code' => 'TRAINING_RECORD_ALREADY_LEARNED',
        'title' => '本次培训学习进度为已完成',
        'detail' => '本次培训学习进度为已完成',
        'source' => array(
            'pointer' => ''
        ),
        'meta' => array()
    ),
    COURSE_SNAPSHOT_FORMAT_ERROR => array(
        'id' => COURSE_SNAPSHOT_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'COURSE_SNAPSHOT_FORMAT_ERROR',
        'title' => '课程快照格式不正确',
        'detail' => '课程快照格式不正确',
        'source' => array(
            'pointer' => 'snapshot'
        ),
        'meta' => array()
    ),
    SERVICE_PRICE_FORMAT_ERROR => array(
        'id' => SERVICE_PRICE_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'SERVICE_PRICE_FORMAT_ERROR',
        'title' => '报告价格格式不正确',
        'detail' => '报告价格格式不正确',
        'source' => array(
            'pointer' => 'price'
        ),
        'meta' => array()
    ),
    SERVICE_ATTACHMENTS_FORMAT_ERROR => array(
        'id' => SERVICE_ATTACHMENTS_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'SERVICE_ATTACHMENTS_FORMAT_ERROR',
        'title' => '附件格式不正确',
        'detail' => '附件格式不正确',
        'source' => array(
            'pointer' => 'attachments'
        ),
        'meta' => array()
    ),
    SERVICE_ISAGREEMENT_FORMAT_ERROR => array(
        'id' => SERVICE_ISAGREEMENT_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'SERVICE_ISAGREEMENT_FORMAT_ERROR',
        'title' => '是否需要用户协议格式不正确',
        'detail' => '是否需要用户协议格式不正确',
        'source' => array(
            'pointer' => 'isAgreement'
        ),
        'meta' => array()
    ),
    SERVICE_REPORTNAME_FORMAT_ERROR => array(
        'id' => SERVICE_REPORTNAME_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'SERVICE_REPORTNAME_FORMAT_ERROR',
        'title' => '报告名称格式不正确',
        'detail' => '报告名称格式不正确',
        'source' => array(
            'pointer' => 'reportName'
        ),
        'meta' => array()
    ),
    SERVICE_SNAPSHOT_FORMAT_ERROR => array(
        'id' => SERVICE_SNAPSHOT_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'SERVICE_SNAPSHOT_FORMAT_ERROR',
        'title' => '服务快照格式不正确',
        'detail' => '服务快照格式不正确',
        'source' => array(
            'pointer' => 'snapshot'
        ),
        'meta' => array()
    ),
    SERVICE_STATUS_ALREADY_ON => array(
        'id' => SERVICE_STATUS_ALREADY_ON,
        'link' => '',
        'status' => 403,
        'code' => 'SERVICE_STATUS_ALREADY_ON',
        'title' => '服务状态已上架',
        'detail' => '服务状态已上架',
        'source' => array(
            'pointer' => 'applyStatus'
        ),
        'meta' => array()
    ),
    SERVICE_STATUS_ALREADY_OFF => array(
        'id' => SERVICE_STATUS_ALREADY_OFF,
        'link' => '',
        'status' => 403,
        'code' => 'SERVICE_STATUS_ALREADY_OFF',
        'title' => '服务状态已下架',
        'detail' => '服务状态已下架',
        'source' => array(
            'pointer' => 'applyStatus'
        ),
        'meta' => array()
    ),
    SERVICE_REJECTREASON_FORMAT_ERROR => array(
        'id' => SERVICE_REJECTREASON_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'SERVICE_REJECTREASON_FORMAT_ERROR',
        'title' => '服务驳回原因格式不正确',
        'detail' => '请输入2-50位的字符',
        'source' => array(
            'pointer' => 'rejectReason'
        ),
        'meta' => array()
    ),
    SERVICE_OFF_REASON_FORMAT_ERROR => array(
        'id' => SERVICE_OFF_REASON_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'SERVICE_OFF_REASON_FORMAT_ERROR',
        'title' => '服务强制下架原因格式不正确',
        'detail' => '请输入2-50位的字符',
        'source' => array(
            'pointer' => 'offReason'
        ),
        'meta' => array()
    ),
    REPORT_ORGANIZATION_FORMAT_ERROR => array(
        'id' => REPORT_ORGANIZATION_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'REPORT_ORGANIZATION_FORMAT_ERROR',
        'title' => '报告出具机构名称格式错误',
        'detail' => '请输入1-50位的字符',
        'source' => array(
            'pointer' => 'organizationName'
        ),
        'meta' => array()
    ),
    REPORT_NAME_FORMAT_ERROR => array(
        'id' => REPORT_NAME_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'REPORT_NAME_FORMAT_ERROR',
        'title' => '报告名称格式错误',
        'detail' => '请输入1-50位的字符',
        'source' => array(
            'pointer' => 'reportName'
        ),
        'meta' => array()
    ),
    ISSUE_TIME_FORMAT_ERROR => array(
        'id' => ISSUE_TIME_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'ISSUE_TIME_FORMAT_ERROR',
        'title' => '报告出具时间格式错误',
        'detail' => '',
        'source' => array(
            'pointer' => 'issueTime'
        ),
        'meta' => array()
    ),
    END_TIME_FORMAT_ERROR => array(
        'id' => END_TIME_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'END_TIME_FORMAT_ERROR',
        'title' => '报告截止时间格式错误',
        'detail' => '',
        'source' => array(
            'pointer' => 'endTime'
        ),
        'meta' => array()
    ),
    REPORT_NUMBER_FORMAT_ERROR => array(
        'id' => REPORT_NUMBER_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'REPORT_NUMBER_FORMAT_ERROR',
        'title' => '报告编号格式错误',
        'detail' => '请输入1-50位的字符',
        'source' => array(
            'pointer' => 'reportNumber'
        ),
        'meta' => array()
    ),
    REPORT_GRADE_FORMAT_ERROR => array(
        'id' => REPORT_GRADE_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'REPORT_GRADE_FORMAT_ERROR',
        'title' => '报告信用等级格式错误',
        'detail' => '请输入1-10位的字符',
        'source' => array(
            'pointer' => 'grade'
        ),
        'meta' => array()
    ),
    ELECTRONIC_REPORT_FORMAT_ERROR => array(
        'id' => ELECTRONIC_REPORT_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'ELECTRONIC_REPORT_FORMAT_ERROR',
        'title' => '信用报告电子版格式错误',
        'detail' => '请输入1-50位的字符',
        'source' => array(
            'pointer' => 'electronicReport'
        ),
        'meta' => array()
    ),
    CREW_NOT_HAVE_PUBLISHING_PERMISSION => array(
        'id' => CREW_NOT_HAVE_PUBLISHING_PERMISSION,
        'link' => '',
        'status' => 403,
        'code' => 'CREW_NOT_HAVE_PUBLISHING_PERMISSION',
        'title' => '该用户不具备发布权限',
        'detail' => '该用户不具备发布权限',
        'source' => array(
            'pointer' => 'crew'
        ),
        'meta' => array()
    ),
    APPLY_STATUS_NOT_UNSUBMITTED => array(
        'id' => APPLY_STATUS_NOT_UNSUBMITTED,
        'link' => '',
        'status' => 403,
        'code' => 'APPLY_STATUS_NOT_UNSUBMITTED',
        'title' => '审核状态非未提交审核',
        'detail' => '审核状态非未提交审核',
        'source' => array(
            'pointer' => 'applyStatus'
        ),
        'meta' => array()
    ),
    ORGANIZATION_TRAIN_TYPE_FORMAT_ERROR => array(
        'id' => ORGANIZATION_TRAIN_TYPE_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'ORGANIZATION_TRAIN_TYPE_FORMAT_ERROR',
        'title' => '培训机构类型不存在',
        'detail' => '培训机构类型不存在',
        'source' => array(
            'pointer' => 'type'
        ),
        'meta' => array()
    ),
    SOURCE_FORMAT_ERROR => array(
        'id' => SOURCE_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'SOURCE_FORMAT_ERROR',
        'title' => '来源格式不正确',
        'detail' => '来源格式不正确',
        'source' => array(
            'pointer' => 'source'
        ),
        'meta' => array()
    ),
    DEVICE_ADDRESS_FORMAT_ERROR => array(
        'id' => DEVICE_ADDRESS_FORMAT_ERROR,
        'link' => '',
        'status' => 403,
        'code' => 'DEVICE_ADDRESS_FORMAT_ERROR',
        'title' => '设备地址格式不正确',
        'detail' => '设备地址格式不正确',
        'source' => array(
            'pointer' => 'deviceAddress'
        ),
        'meta' => array()
    ),
    STICK_ALREADY_TOP => array(
        'id' => STICK_ALREADY_TOP,
        'link' => '',
        'status' => 403,
        'code' => 'STICK_ALREADY_TOP',
        'title' => '已置顶',
        'detail' => '已置顶',
        'source' => array(
            'pointer' => 'stick'
        ),
        'meta' => array()
    ),
    STICK_ALREADY_CANCELTOP => array(
        'id' => STICK_ALREADY_CANCELTOP,
        'link' => '',
        'status' => 403,
        'code' => 'STICK_ALREADY_CANCELTOP',
        'title' => '已取消置顶',
        'detail' => '已取消置顶',
        'source' => array(
            'pointer' => 'stick'
        ),
        'meta' => array()
    ),
    RESOURCE_STATUS_READ=> array(
        'id'=>RESOURCE_STATUS_READ,
        'link'=>'',
        'status'=>403,
        'code'=> RESOURCE_STATUS_READ,
        'title'=>'已读',
        'detail'=>'表述该资源已读.',
        'source'=>array(
            'pointer'=>'status'
        ),
        'meta'=>array()
    ),
    GOLD_COIN_NAME_FORMAT_ERROR=> array(
        'id'=>GOLD_COIN_NAME_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=> GOLD_COIN_NAME_FORMAT_ERROR,
        'title'=>'金币名称格式不正确',
        'detail'=>'金币名称格式不正确.',
        'source'=>array(
            'pointer'=>'status'
        ),
        'meta'=>array()
    ),
    GOLD_COIN_NUMBER_FORMAT_ERROR=> array(
        'id'=>GOLD_COIN_NUMBER_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=> GOLD_COIN_NUMBER_FORMAT_ERROR,
        'title'=>'金币数量格式不正确',
        'detail'=>'金币数量格式不正确.',
        'source'=>array(
            'pointer'=>'status'
        ),
        'meta'=>array()
    ),
    GOLD_COIN_PRODUCT_ID_FORMAT_ERROR=> array(
        'id'=>GOLD_COIN_PRODUCT_ID_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=> GOLD_COIN_PRODUCT_ID_FORMAT_ERROR,
        'title'=>'产品ID格式不正确',
        'detail'=>'产品ID格式不正确.',
        'source'=>array(
            'pointer'=>'status'
        ),
        'meta'=>array()
    ),
    GOLD_COIN_AMOUNT_FORMAT_ERROR=> array(
        'id'=>GOLD_COIN_AMOUNT_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=> GOLD_COIN_AMOUNT_FORMAT_ERROR,
        'title'=>'购买金额格式不正确',
        'detail'=>'购买金额格式不正确.',
        'source'=>array(
            'pointer'=>'status'
        ),
        'meta'=>array()
    ),
    GOLD_COIN_CREW_FORMAT_ERROR=> array(
        'id'=>GOLD_COIN_CREW_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=> GOLD_COIN_CREW_FORMAT_ERROR,
        'title'=>'发布用户格式不正确',
        'detail'=>'发布用户格式不正确.',
        'source'=>array(
            'pointer'=>'status'
        ),
        'meta'=>array()
    ),
    STATUS_ALREADY_SHELVE => array(
        'id' => STATUS_ALREADY_SHELVE,
        'link' => '',
        'status' => 403,
        'code' => 'STATUS_ALREADY_SHELVE',
        'title' => '状态为已上架',
        'detail' => '状态为已上架',
        'source' => array(
            'pointer' => 'status'
        ),
        'meta' => array()
    ),
    STATUS_ALREADY_OFF_SHELVE => array(
        'id' => STATUS_ALREADY_OFF_SHELVE,
        'link' => '',
        'status' => 403,
        'code' => 'STATUS_ALREADY_OFF_SHELVE',
        'title' => '状态为已下架',
        'detail' => '状态为已下架',
        'source' => array(
            'pointer' => 'status'
        ),
        'meta' => array()
    ),
    ACCOUNT_BALANCE_NOT_ENOUGH => array(
        'id'=>ACCOUNT_BALANCE_NOT_ENOUGH,
        'link'=>'',
        'status'=>403,
        'code'=>'ACCOUNT_BALANCE_NOT_ENOUGH',
        'title'=>'账户余额不足',
        'detail'=>'账户余额不足',
        'source'=>array(
            'pointer'=>'accountBalance'
        ),
        'meta'=>array()
    ),
    DEPOSIT_STATUS_NOT_PENDING=>
        array(
            'id'=>DEPOSIT_STATUS_NOT_PENDING,
            'link'=>'',
            'status'=>403,
            'code'=>'DEPOSIT_STATUS_NOT_PENDING',
            'title'=>'充值状态非待充值',
            'detail'=>'充值状态非待充值',
            'source'=>array(
                'pointer'=>'status'
            ),
            'meta'=>array()
        ),
    DEPOSIT_AMOUNT_UNREASONABLE=>
        array(
            'id'=>DEPOSIT_AMOUNT_UNREASONABLE,
            'link'=>'',
            'status'=>403,
            'code'=>'DEPOSIT_AMOUNT_UNREASONABLE',
            'title'=>'充值金额不合理',
            'detail'=>'充值金额不合理',
            'source'=>array(
                'pointer'=>'amount'
            ),
            'meta'=>array()
        ),
    PAYMENT_AMOUNT_MISMATCH=>
        array(
            'id'=>PAYMENT_AMOUNT_MISMATCH,
            'link'=>'',
            'status'=>403,
            'code'=>'PAYMENT_AMOUNT_MISMATCH',
            'title'=>'支付金额不匹配',
            'detail'=>'支付金额不匹配',
            'source'=>array(
                'pointer'=>'amount'
            ),
            'meta'=>array()
        ),
);
