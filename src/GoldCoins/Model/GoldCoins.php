<?php
namespace Sdk\GoldCoins\Model;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Model\ShelveAbleTrait;
use Sdk\Common\Adapter\IShelveAbleAdapter;
use Sdk\Common\Model\IShelveAble;


use Sdk\Crew\Model\Crew;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Marmot\Core;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Sdk\GoldCoins\Repository\GoldCoinsRepository;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class GoldCoins implements IOperatAble, IObject, IShelveAble
{
    use OperatAbleTrait, Object, ShelveAbleTrait;

  
    private $id;

    private $name;

    private $number;

    private $productId;

    private $amount;

    private $crew;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->number = 0;
        $this->productId = 0;
        $this->amount = 0.00;
        $this->crew = new Crew();
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = self::STATUS['OFFSHELVE'];
        $this->repository = new GoldCoinsRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->number);
        unset($this->productId);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->status);
        unset($this->amount);
        unset($this->crew);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setNumber(int $number) : void
    {
        $this->number = $number;
    }

    public function getNumber() : int
    {
        return $this->number;
    }

    public function setProductId(string $productId) : void
    {
        $this->productId = $productId;
    }

    public function getProductId() : string
    {
        return $this->productId;
    }

    public function setAmount(float $amount) : void
    {
        $this->amount = $amount;
    }

    public function getAmount() : float
    {
        return $this->amount;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }
    

    protected function getRepository() : GoldCoinsRepository
    {
        return $this->repository;
    }

    protected function getIShelveAbleAdapter() : IShelveAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIOperatAbleRepository() : GoldCoinsRepository
    {
        return $this->getRepository();
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }
}
