<?php
namespace Sdk\GoldCoins\Adapter\GoldCoins;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IShelveAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface IGoldCoinsAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter, IShelveAbleAdapter
{
}
