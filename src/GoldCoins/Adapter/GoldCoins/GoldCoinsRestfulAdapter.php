<?php
namespace Sdk\GoldCoins\Adapter\GoldCoins;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\ShelveAbleRestfulAdapterTrait;

use Sdk\GoldCoins\Translator\GoldCoinsRestfulTranslator;
use Sdk\GoldCoins\Model\NullGoldCoins;
use Sdk\GoldCoins\Model\GoldCoins;

use Marmot\Core;

class GoldCoinsRestfulAdapter extends GuzzleAdapter implements IGoldCoinsAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        ShelveAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    private $resource;

    const SCENARIOS = [
        'GOLDCOINS_LIST'=>[
            'fields'=>[
                'crews'=>'cellphone'
            ],
            'include'=>'crews'
        ],
        'GOLDCOINS_FETCH_ONE'=>[
            'fields'=>[
                'crews'=>'cellphone'
            ],
            'include'=>'crews'
        ]
    ];
    
    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new GoldCoinsRestfulTranslator();
        $this->resource = 'goldCoins';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            115 => STATUS_ALREADY_SHELVE,
            116 => STATUS_ALREADY_OFF_SHELVE
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullGoldCoins());
    }

    public function addAction(GoldCoins $goldCoins) : bool
    {
        
        $data = $this->getTranslator()->objectToArray(
            $goldCoins,
            array('name', 'productId', 'number', 'amount', 'crew')
        );

        $this->post(
            $this->getResource(),
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($goldCoins);
            return true;
        }

        return false;
    }

    public function editAction(GoldCoins $goldCoins) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $goldCoins,
            array('name', 'productId', 'number', 'amount')
        );

        $this->patch(
            $this->getResource().'/'.$goldCoins->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($goldCoins);
            return true;
        }

        return false;
    }
}
