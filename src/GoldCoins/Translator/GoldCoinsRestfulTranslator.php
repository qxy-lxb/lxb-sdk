<?php
namespace Sdk\GoldCoins\Translator;

use Sdk\Common\Translator\RestfulTranslatorTrait;
use Marmot\Core;
use Sdk\Crew\Translator\CrewRestfulTranslator;

use Sdk\GoldCoins\Model\GoldCoins;
use Sdk\GoldCoins\Model\NullGoldCoins;

use Marmot\Interfaces\IRestfulTranslator;

class GoldCoinsRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $goldCoins = null)
    {
        return $this->translateToObject($expression, $goldCoins);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $goldCoins = null)
    {
        if (empty($expression)) {
            return new NullGoldCoins();
        }

        if ($goldCoins == null) {
            $goldCoins = new GoldCoins();
        }

        $data =  $expression['data'];

        $id = $data['id'];
        $goldCoins->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $goldCoins->setName($attributes['name']);
        }
        if (isset($attributes['number'])) {
            $goldCoins->setNumber($attributes['number']);
        }
        if (isset($attributes['productId'])) {
            $goldCoins->setProductId($attributes['productId']);
        }
        if (isset($attributes['amount'])) {
            $goldCoins->setAmount($attributes['amount']);
        }
        if (isset($attributes['status'])) {
            $goldCoins->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $goldCoins->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $goldCoins->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $goldCoins->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);

            $goldCoins->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        return $goldCoins;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($goldCoins, array $keys = array())
    {
        if (!$goldCoins instanceof GoldCoins) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'number',
                'productId',
                'amount',
                'status',
                'crew'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'goldCoins'
            )
        );
        
        if (in_array('id', $keys)) {
            $expression['data']['id'] = $goldCoins->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $goldCoins->getName();
        }
        if (in_array('number', $keys)) {
            $attributes['number'] = $goldCoins->getNumber();
        }
        if (in_array('productId', $keys)) {
            $attributes['productId'] = $goldCoins->getProductId();
        }
        if (in_array('amount', $keys)) {
            $attributes['amount'] = $goldCoins->getAmount();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $goldCoins->getStatus();
        }
        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => Core::$container->get('crew')->getId()
                )
             );
        }

        return $expression;
    }
}
