<?php
namespace Sdk\GoldCoins\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\ShelveAbleRepositoryTrait;

use Sdk\GoldCoins\Adapter\GoldCoins\IGoldCoinsAdapter;
use Sdk\GoldCoins\Adapter\GoldCoins\GoldCoinsRestfulAdapter;
use Sdk\GoldCoins\Adapter\GoldCoins\GoldCoinsMockAdapter;
use Sdk\GoldCoins\Model\GoldCoins;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class GoldCoinsRepository extends Repository implements IGoldCoinsAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait,  OperatAbleRepositoryTrait,ShelveAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'GOLDCOINS_LIST';
    const FETCH_ONE_MODEL_UN = 'GOLDCOINS_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new GoldCoinsRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : IGoldCoinsAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IGoldCoinsAdapter
    {
        return new GoldCoinsMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
