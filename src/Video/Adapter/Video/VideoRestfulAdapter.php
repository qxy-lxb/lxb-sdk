<?php
namespace Sdk\Video\Adapter\Video;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\CommonMapErrorsTrait;

use Sdk\Video\Translator\VideoRestfulTranslator;
use Sdk\Video\Model\NullVideo;
use Sdk\Video\Model\Video;

use Marmot\Core;

class VideoRestfulAdapter extends GuzzleAdapter implements IVideoAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    private $resource;

    const SCENARIOS = [
        'VIDEO_LIST'=>[
            'fields'=>[]
        ],
        'VIDEO_FETCH_ONE'=>[
            'fields'=>[]
        ]
    ];
    
    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new VideoRestfulTranslator();
        $this->resource = 'videos';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            10001 => VIDEO_NAME_FORMAT_ERROR,
            10002 => VIDEO_CATEGORY_FORMAT_ERROR,
            10003 => VIDEO_FORMAT_ERROR,
            10004 => VIDEO_STUDY_TIME_FORMAT_ERROR,
            10005 => VIDEO_TIME_FORMAT_ERROR,
            10006 => NEED_SIGNIN,
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullVideo());
    }

    public function addAction(Video $video) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $video,
            array('name', 'studyTime','videoTime', 'video','category','crew')
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($video);
            return true;
        }

        return false;
    }

    public function editAction(Video $video) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $video,
            array('name', 'studyTime','videoTime', 'video','category')
        );

        $this->patch(
            $this->getResource().'/'.$video->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($video);
            return true;
        }

        return false;
    }
}
