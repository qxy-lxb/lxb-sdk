<?php
namespace Sdk\Video\Adapter\Video;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface IVideoAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter
{
}
