<?php
namespace Sdk\Video\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;

use Sdk\Video\Adapter\Video\IVideoAdapter;
use Sdk\Video\Adapter\Video\VideoRestfulAdapter;
use Sdk\Video\Adapter\Video\NewMockAdapter;
use Sdk\Video\Model\Video;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class VideoRepository extends Repository implements IVideoAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, OperatAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'VIDEO_LIST';
    const FETCH_ONE_MODEL_UN = 'VIDEO_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new VideoRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : IVideoAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IVideoAdapter
    {
        return new NewMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
