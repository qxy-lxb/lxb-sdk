<?php
namespace Sdk\Video\Translator;

use Sdk\Common\Translator\RestfulTranslatorTrait;
use Marmot\Core;

use Sdk\Video\Model\Video;
use Sdk\Video\Model\NullVideo;

use Marmot\Interfaces\IRestfulTranslator;

class VideoRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    public function arrayToObject(array $expression, $video = null)
    {
        return $this->translateToObject($expression, $video);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $video = null)
    {
        if (empty($expression)) {
            return new NullVideo();
        }

        if ($video == null) {
            $video = new Video();
        }

        $data =  $expression['data'];

        $id = $data['id'];
        $video->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $video->setName($attributes['name']);
        }
        if (isset($attributes['studyTime'])) {
            $video->setStudyTime($attributes['studyTime']);
        }
        if (isset($attributes['videoTime'])) {
            $video->setVideoTime($attributes['videoTime']);
        }
        if (isset($attributes['attachment'])) {
            $video->setVideo($attributes['attachment']);
        }
        if (isset($attributes['category'])) {
            $video->setCategory($attributes['category']);
        }
        if (isset($attributes['statusTime'])) {
            $video->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['status'])) {
            $video->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $video->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $video->setUpdateTime($attributes['updateTime']);
        }

        return $video;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($video, array $keys = array())
    {
        if (!$video instanceof Video) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'studyTime',
                'videoTime',
                'video',
                'category',
                'crew'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'videos'
            )
        );
        
        if (in_array('id', $keys)) {
            $expression['data']['id'] = $video->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $video->getName();
        }
        if (in_array('studyTime', $keys)) {
            $attributes['studyTime'] = $video->getStudyTime();
        }
        if (in_array('videoTime', $keys)) {
            $attributes['videoTime'] = $video->getVideoTime();
        }
        if (in_array('video', $keys)) {
            $attributes['attachment'] = $video->getVideo();
        }
        if (in_array('category', $keys)) {
            $attributes['category'] = $video->getCategory();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $video->getCrew()->getId()
                )
             );
        }

        return $expression;
    }
}
