<?php
namespace Sdk\Video\Model;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Adapter\IEnableAbleAdapter;
use Sdk\Common\Adapter\ITopAbleAdapter;
use Sdk\Common\Model\IEnableAble;
use Sdk\Common\Model\EnableAbleTrait;

use Sdk\Crew\Model\Crew;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Marmot\Core;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Sdk\Video\Repository\VideoRepository;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Video implements IEnableAble, IOperatAble, IObject
{
    use EnableAbleTrait, OperatAbleTrait, Object;

    /**
     * @var CATEGORY['POLICY_INTERPRETATION']  政策解读 1
     * @var CATEGORY['LAWS_REGULATIONS']  法律法规 2
     * @var CATEGORY['WARNING_CASES']  警示案例 3
     */
    const CATEGORY = array(
        'POLICY_INTERPRETATION' => 1,
        'LAWS_REGULATIONS' => 2,
        'WARNING_CASES' => 3
    );

    private $id;
    
    private $name;
    
    private $studyTime;

    private $videoTime;
    
    private $video;
    
    private $category;
    
    private $repository;

    private $crew;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->studyTime = '';
        $this->videoTime = '';
        $this->video = array();
        $this->category = self::CATEGORY['POLICY_INTERPRETATION'];
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->repository = new VideoRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->studyTime);
        unset($this->videoTime);
        unset($this->video);
        unset($this->category);
        unset($this->crew);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setStudyTime(string $studyTime) : void
    {
        $this->studyTime = $studyTime;
    }

    public function getStudyTime() : string
    {
        return $this->studyTime;
    }

    public function setVideoTime(string $videoTime) : void
    {
        $this->videoTime = $videoTime;
    }

    public function getVideoTime() : string
    {
        return $this->videoTime;
    }

    public function setVideo(array $video) : void
    {
        $this->video = $video;
    }

    public function getVideo() : array
    {
        return $this->video;
    }

    public function setCategory(int $category) : void
    {
        $this->category = in_array(
            $category,
            self::CATEGORY
        ) ? $category : self::CATEGORY['POLICY_INTERPRETATION'];
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    protected function getRepository() : VideoRepository
    {
        return $this->repository;
    }

    protected function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }
}
