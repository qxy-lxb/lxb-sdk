<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Model\IDeleteAble;
use Sdk\Common\Model\DeleteTrait;

use Sdk\Order\Repository\OrderRepository;
use Sdk\Enterprise\Model\Enterprise;
use Sdk\Course\Model\Course;
use Sdk\Organization\Model\Organization;
use Sdk\Member\Model\Member;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IDeleteAbleAdapter;

use Sdk\MemberAccount\Model\MemberAccount;
use Sdk\TradeRecord\Model\ITradeAble;

class Order implements IOperatAble, IObject, IDeleteAble, ITradeAble
{
    use OperatAbleTrait, Object, DeleteTrait;

    const IS_BEAR = array(
        'YES' => 1,
        'NO' => 2
    );

    const ORDER_STATUS = array(
        'DEFAULT' => 0, // 正常
        'SOFT_DELETE' => -2 //删除
    );

    private $id;

    private $number;

    private $courseName;

    private $userName;

    private $organizationName;

    private $price;

    private $transactionPlatform;

    private $memberAccount;

    private $course;

    private $enterprise;

    private $organization;

    private $member;

    private $snapshot;

    private $repository;

    private $repairRecord;

    private $orderStatus;

    private $transactionNumber;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->number = '';
        $this->courseName = '';
        $this->userName = '';
        $this->organizationName = '';
        $this->price = 0;
        $this->transactionPlatform = 0;
        $this->memberAccount = new MemberAccount();
        $this->course = new Course();
        $this->enterprise = new Enterprise();
        $this->organization = new Organization();
        $this->snapshot = '';
        $this->orderStatus = self::ORDER_STATUS['DEFAULT'];
        $this->status = 0;
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->repository = new OrderRepository();
        $this->repairRecord = 0;
        $this->transactionNumber = '';
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->number);
        unset($this->courseName);
        unset($this->userName);
        unset($this->organizationName);
        unset($this->price);
        unset($this->transactionPlatform);
        unset($this->memberAccount);
        unset($this->course);
        unset($this->enterprise);
        unset($this->organization);
        unset($this->snapshot);
        unset($this->orderStatus);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
        unset($this->repairRecord);
        unset($this->transactionNumber);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setNumber(string $number) : void
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setCourseName(string $courseName) : void
    {
        $this->courseName = $courseName;
    }

    public function getCourseName() : string
    {
        return $this->courseName;
    }

    public function setUserName(string $userName) : void
    {
        $this->userName = $userName;
    }

    public function getUserName() : string
    {
        return $this->userName;
    }

    public function setOrganizationName(string $organizationName) : void
    {
        $this->organizationName = $organizationName;
    }

    public function getOrganizationName() : string
    {
        return $this->organizationName;
    }
    
    public function setPrice(int $price) : void
    {
        $this->price = $price;
    }

    public function getPrice() : int
    {
        return $this->price;
    }

    public function setTransactionPlatform(int $transactionPlatform) : void
    {
        $this->transactionPlatform = $transactionPlatform;
    }

    public function getTransactionPlatform() : int
    {
        return $this->transactionPlatform;
    }

    public function setMemberAccount(MemberAccount $memberAccount) : void
    {
        $this->memberAccount = $memberAccount;
    }

    public function getMemberAccount() : MemberAccount
    {
        return $this->memberAccount;
    }

    public function setCourse(Course $course) : void
    {
        $this->course = $course;
    }

    public function getCourse() : Course
    {
        return $this->course;
    }

    public function setEnterprise(Enterprise $enterprise) : void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise() : Enterprise
    {
        return $this->enterprise;
    }

    public function setOrganization(Organization $organization) : void
    {
        $this->organization = $organization;
    }

    public function getOrganization() : Organization
    {
        return $this->organization;
    }

    public function setSnapshot(string $snapshot) : void
    {
        $this->snapshot = $snapshot;
    }

    public function getSnapshot() : string
    {
        return $this->snapshot;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository() : OrderRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIDeleteAbleAdapter() : IDeleteAbleAdapter
    {
        return $this->getRepository();
    }

    public function pay() : bool
    {
        return $this->getRepository()->pay($this);
    }

    public function setRepairRecord(int $repairRecord) : void
    {
        $this->repairRecord = $repairRecord;
    }

    public function getRepairRecord() : int
    {
        return $this->repairRecord;
    }

    public function setOrderStatus(int $orderStatus) : void
    {
        $this->orderStatus = in_array($orderStatus, self::ORDER_STATUS) ?
        $orderStatus : self::ORDER_STATUS['DEFAULT'];
    }

    public function getOrderStatus() : int
    {
        return $this->orderStatus;
    }

    public function setTransactionNumber(string $transactionNumber) : void
    {
        $this->transactionNumber = $transactionNumber;
    }

    public function getTransactionNumber() : string
    {
        return $this->transactionNumber;
    }
}
