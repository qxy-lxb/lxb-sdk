<?php
namespace Sdk\Order\Translator;

use Marmot\Core;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Organization\Translator\OrganizationRestfulTranslator;
use Sdk\Course\Translator\CourseRestfulTranslator;
use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;

use Sdk\Order\Model\Order;
use Sdk\Order\Model\NullOrder;
use Sdk\Organization\Repository\OrganizationRepository;
use Sdk\Course\Repository\CourseRepository;

use Sdk\MemberAccount\Translator\MemberAccountRestfulTranslator;
use Sdk\TradeRecord\Model\ITradeAble;

class OrderRestfulTranslator implements IRestfulTranslator, ITradeAble
{
    use RestfulTranslatorTrait;

    protected function getOrganizationRestfulTranslator() : OrganizationRestfulTranslator
    {
        return new OrganizationRestfulTranslator();
    }

    protected function getCourseRestfulTranslator() : CourseRestfulTranslator
    {
        return new CourseRestfulTranslator();
    }

    protected function getMemberAccountRestfulTranslator()
    {
        return new MemberAccountRestfulTranslator();
    }

    protected function getEnterpriseRestfulTranslator() : EnterpriseRestfulTranslator
    {
        return new EnterpriseRestfulTranslator();
    }

    public function arrayToObject(array $expression, $order = null)
    {
        return $this->translateToObject($expression, $order);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    private function translateToObject(array $expression, $order = null)
    {
        if (empty($expression)) {
            return NullOrder::getInstance();
        }

        if ($order == null) {
            $order = new Order();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $order->setId($id);

        if (isset($data['attributes'])) {
            $attributes = $data['attributes'];

            if (isset($attributes['number'])) {
                $order->setNumber($attributes['number']);
            }
            if (isset($attributes['courseName'])) {
                $order->setCourseName($attributes['courseName']);
            }
            if (isset($attributes['userName'])) {
                $order->setUserName($attributes['userName']);
            }
            if (isset($attributes['organizationName'])) {
                $order->setOrganizationName($attributes['organizationName']);
            }
            if (isset($attributes['price'])) {
                $order->setPrice($attributes['price']);
            }
            if (isset($attributes['transactionPlatform'])) {
                $order->setTransactionPlatform($attributes['transactionPlatform']);
            }
            if (isset($attributes['transactionNumber'])) {
                $order->setTransactionNumber($attributes['transactionNumber']);
            }
            if (isset($attributes['status'])) {
                $order->setStatus($attributes['status']);
            }
            if (isset($attributes['orderStatus'])) {
                $order->setOrderStatus($attributes['orderStatus']);
            }
            if (isset($attributes['snapshot'])) {
                $order->setSnapshot($attributes['snapshot']);
            }
            if (isset($attributes['createTime'])) {
                $order->setCreateTime($attributes['createTime']);
            }
            if (isset($attributes['updateTime'])) {
                $order->setUpdateTime($attributes['updateTime']);
            }
            if (isset($attributes['statusTime'])) {
                $order->setStatusTime($attributes['statusTime']);
            }
        }
        
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
       
        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
       
        if (isset($relationships['course']['data'])) {
            $course = $this->changeArrayFormat($relationships['course']['data']);
            $order->setCourse($this->getCourseRestfulTranslator()->arrayToObject($course));
        }
        if (isset($relationships['enterprise']['data'])) {
            $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);
            $order->setEnterprise($this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise));
        }
        if (isset($relationships['organization']['data'])) {
            $order->getOrganization()->setId($relationships['organization']['data']['id']);
            $order->getOrganization()->setName($attributes['organizationName']);
        }

        if (isset($relationships['memberAccount']['data'])) {
            if (isset($expression['included'])) {
                $memberAccount = $this->changeArrayFormat($relationships['memberAccount']['data'],$expression['included']
                );
            }

            if (!isset($expression['included'])) {
                $memberAccount = $this->changeArrayFormat($relationships['memberAccount']['data']);
            }

            $order->setMemberAccount($this->getMemberAccountRestfulTranslator()->arrayToObject($memberAccount));
        }
        
        return $order;
    }

    public function objectToArray($order, array $keys = array())
    {
        if (!$order instanceof Order) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'price',
                'orderStatus',
                'transactionPlatform',
                'transactionNumber',
                'enterprise',
                'course',
                'repairRecord',
                'memberAccount' => []
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'courseOrders'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $order->getId();
        }
        if (in_array('price', $keys)) {
            $attributes['price'] = $order->getPrice();
        }
        if (in_array('orderStatus', $keys)) {
            $attributes['orderStatus'] = $order->getOrderStatus();
        }
        if (in_array('transactionPlatform', $keys)) {
            $attributes['transactionPlatform'] = $order->getTransactionPlatform();
        }
        if (in_array('repairRecord', $keys)) {
            $attributes['repairRecordId'] = $order->getRepairRecord();
        }
        if (in_array('transactionNumber', $keys)) {
            $attributes['transactionNumber'] = $order->getTransactionNumber();
        }

        $expression['data']['attributes'] = $attributes;
        if (in_array('enterprise', $keys)) {
            $expression['data']['relationships']['enterprise']['data'] = array(
                array(
                    'type' => 'enterprise',
                    'id' => $order->getEnterprise()->getId()
                )
            );
        }
        if (in_array('course', $keys)) {
            $expression['data']['relationships']['course']['data'] = array(
                array(
                    'type' => 'course',
                    'id' => $order->getCourse()->getId()
                )
            );
        }
        if (in_array('memberAccount', $keys)) {
            $expression['data']['relationships']['memberAccount']['data'] = array(
                array(
                    'type' => 'memberAccounts',
                    'id' => Core::$container->get('user')->getId()
                )
             );
        }

        return $expression;
    }

    protected function getOrganizationRepository()
    {
        return new OrganizationRepository();
    }

    protected function getCourseRepository()
    {
        return new CourseRepository();
    }
}
