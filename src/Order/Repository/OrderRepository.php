<?php
namespace Sdk\Order\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\DeleteAbleRepositoryTrait;

use Sdk\Order\Adapter\Order\OrderRestfulAdapter;
use Sdk\Order\Model\Order;
use Sdk\Order\Adapter\Order\IOrderAdapter;
use Marmot\Core;

use Marmot\Framework\Classes\Repository;

class OrderRepository extends Repository implements IOrderAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, OperatAbleRepositoryTrait, DeleteAbleRepositoryTrait;

    const LIST_MODEL_UN = 'ORDER_LIST';
    const FETCH_ONE_MODEL_UN = 'ORDER_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new OrderRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : IOrderAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IOrderAdapter
    {
        return new OrderMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function pay(Order $order) : bool
    {
        return $this->getAdapter()->pay($order);
    }
}
