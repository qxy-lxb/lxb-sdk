<?php
namespace Sdk\Order\Adapter\Order;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Marmot\Core;

use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\DeleteAbleRestfulAdapterTrait;

use Sdk\Order\Translator\OrderRestfulTranslator;
use Sdk\Order\Model\NullOrder;
use Sdk\Order\Model\Order;

class OrderRestfulAdapter extends GuzzleAdapter implements IOrderAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,OperatAbleRestfulAdapterTrait, FetchAbleRestfulAdapterTrait, DeleteAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'ORDER_LIST'=>[
            'fields'=>[
            ],
            'include'=>'course,organization,memberAccount,memberAccount.member,enterprise'
        ],
        'ORDER_FETCH_ONE'=>[
            'fields'=>[
            ],
            'include'=>'course,organization,memberAccount,memberAccount.member,enterprise'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new OrderRestfulTranslator();
        $this->resource = 'courseOrders';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullOrder());
    }

    public function addAction(Order $order) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $order,
            array('price', 'transactionPlatform', 'enterprise', 'course', 'memberAccount', 'repairRecord')
        );
        $this->post($this->getResource(), $data);

        if ($this->isSuccess()) {
            $this->translateToObject($order);
            return true;
        }

        return false;
    }

    public function editAction(Order $order) : bool
    {
        unset($order);

        return false;
    }

    public function pay(Order $order) : bool
    {
        return $this->payAction($order);
    }

    protected function payAction(Order $order) : bool
    {
        $this->patch(
            $this->getResource().'/'.$order->getId().'/pay'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($order);
            return true;
        }

        return false;
    }
}
