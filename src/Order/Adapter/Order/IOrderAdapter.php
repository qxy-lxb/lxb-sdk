<?php
namespace Sdk\Order\Adapter\Order;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IDeleteAbleAdapter;
use Sdk\Order\Model\Order;

interface IOrderAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter, IDeleteAbleAdapter
{
    public function pay(Order $order);
}
