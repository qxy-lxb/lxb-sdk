<?php
namespace Sdk\ExamRecord\Adapter\ExamRecord;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Marmot\Core;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\CommonMapErrorsTrait;

use Sdk\ExamRecord\Translator\ExamRecordRestfulTranslator;
use Sdk\ExamRecord\Model\NullExamRecord;
use Sdk\ExamRecord\Model\ExamRecord;

class ExamRecordRestfulAdapter extends GuzzleAdapter implements IExamRecordAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,OperatAbleRestfulAdapterTrait, FetchAbleRestfulAdapterTrait,CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'EXAMRECORD_LIST'=>[
            'fields'=>[],
            'include'=>'trainingRecord'
        ],
        'EXAMRECORD_FETCH_ONE'=>[
            'fields'=>[]
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new ExamRecordRestfulTranslator();
        $this->resource = 'examRecords';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            50001 => LEARNED_PROGRESS_FORMAT_ERROR,
            50002 => EXAM_PAPER_FORMAT_ERROR,
            50004 => TRAINING_RECORD_FORMAT_ERROR,
            50005 => TRAINING_RECORD_ALREADY_PASS,
            50006 => TRAINING_RECORD_ALREADY_FINISHED,
            50007 => TRAINING_RECORD_ALREADY_LEARNED,
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullExamRecord());
    }

    public function addAction(ExamRecord $examRecord) : bool
    {
        $data = $this->getTranslator()->objectToArray($examRecord, array('examPaper', 'trainingRecord'));
        
        $this->post($this->getResource(), $data);
        
        if ($this->isSuccess()) {
            $this->translateToObject($examRecord);
            return true;
        }

        return false;
    }

    public function editAction(ExamRecord $examRecord) : bool
    {
        unset($examRecord);
        return false;
    }
}
