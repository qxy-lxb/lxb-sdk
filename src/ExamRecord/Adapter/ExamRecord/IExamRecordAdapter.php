<?php
namespace Sdk\ExamRecord\Adapter\ExamRecord;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;

interface IExamRecordAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter
{
}
