<?php
namespace Sdk\ExamRecord\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\ExamRecord\Model\ExamRecord;
use Sdk\ExamRecord\Model\NullExamRecord;
use Sdk\TrainingRecord\Repository\TrainingRecordRepository;

class ExamRecordRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $examRecord = null)
    {
        return $this->translateToObject($expression, $examRecord);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    private function translateToObject(array $expression, $examRecord = null)
    {
        if (empty($expression)) {
            return new NullExamRecord();
        }

        if ($examRecord == null) {
            $examRecord = new ExamRecord();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $examRecord->setId($id);

        if (isset($data['attributes'])) {
            $attributes = $data['attributes'];

            if (isset($attributes['score'])) {
                $examRecord->setScore($attributes['score']);
            }
            if (isset($attributes['status'])) {
                $examRecord->setStatus($attributes['status']);
            }
            if (isset($attributes['examPaper'])) {
                $examRecord->setExamPaper($attributes['examPaper']);
            }
            if (isset($attributes['createTime'])) {
                $examRecord->setCreateTime($attributes['createTime']);
            }
            if (isset($attributes['updateTime'])) {
                $examRecord->setUpdateTime($attributes['updateTime']);
            }
            if (isset($attributes['statusTime'])) {
                $examRecord->setStatusTime($attributes['statusTime']);
            }

            $relationships = isset($data['relationships']) ? $data['relationships'] : array();
            if (isset($relationships['trainingRecord']['data'])) {
                $trainingRecord = $this->getTrainingRecordRepository()->fetchOne($relationships['trainingRecord']['data']['id']);
                $examRecord->setTrainingRecord($trainingRecord);
            }
        }
        
        return $examRecord;
    }

    public function objectToArray($examRecord, array $keys = array())
    {
        if (!$examRecord instanceof ExamRecord) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'examPaper',
                'score',
                'status',
                'trainingRecord'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'examRecords'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $examRecord->getId();
        }
        
        if (in_array('examPaper', $keys)) {
            $expression['data']['attributes']['examPaper'] = $examRecord->getExamPaper();
        }
        if (in_array('score', $keys)) {
            $expression['data']['attributes']['score'] = $examRecord->getScore();
        }
        if (in_array('trainingRecord', $keys)) {
            $expression['data']['relationships']['trainingRecord']['data'] = array(
                array(
                    'type' => 'trainingRecords',
                    'id' => $examRecord->getTrainingRecord()->getId()
                )
            );
        }

        return $expression;
    }

    protected function getTrainingRecordRepository()
    {
        return new TrainingRecordRepository();
    }
}
