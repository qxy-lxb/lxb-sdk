<?php
namespace Sdk\ExamRecord\Model;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Sdk\TrainingRecord\Model\TrainingRecord;

use Sdk\ExamRecord\Repository\ExamRecordRepository;

class ExamRecord implements IOperatAble, IObject
{
    use OperatAbleTrait, Object;

    private $id;

    private $examPaper;

    private $score;

    private $trainingRecord;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->examPaper = array();
        $this->score = 0;
        $this->status = 0;
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->trainingRecord = new TrainingRecord();
        $this->repository = new ExamRecordRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->examPaper);
        unset($this->score);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->trainingRecord);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setExamPaper(array $examPaper) : void
    {
        $this->examPaper = $examPaper;
    }

    public function getExamPaper() : array
    {
        return $this->examPaper;
    }

    public function setScore(int $score) : void
    {
        $this->score = $score;
    }

    public function getScore() : int
    {
        return $this->score;
    }

    public function setTrainingRecord(TrainingRecord $trainingRecord) : void
    {
        $this->trainingRecord = $trainingRecord;
    }

    public function getTrainingRecord() : TrainingRecord
    {
        return $this->trainingRecord;
    }

    protected function getRepository() : ExamRecordRepository
    {
        return $this->repository;
    }
    
    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
}
