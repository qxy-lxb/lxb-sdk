<?php
namespace Sdk\ExamRecord\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;

use Sdk\ExamRecord\Adapter\ExamRecord\ExamRecordRestfulAdapter;
use Sdk\ExamRecord\Adapter\ExamRecord\IExamRecordAdapter;
use Marmot\Core;

use Marmot\Framework\Classes\Repository;

class ExamRecordRepository extends Repository implements IExamRecordAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, OperatAbleRepositoryTrait;

    const LIST_MODEL_UN = 'EXAMRECORD_LIST';
    const FETCH_ONE_MODEL_UN = 'EXAMRECORD_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new ExamRecordRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : IExamRecordAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IExamRecordAdapter
    {
        return new ExamRecordMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
