<?php
namespace Sdk\DishonestyInfo\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\DishonestyInfo\Translator\DishonestyInfoRestfulTranslator;
use Sdk\DishonestyInfo\Model\CategoryModelFactory;

use Sdk\DishonestyInfo\Model\DishonestyInfo;
use Sdk\DishonestyInfo\Model\NullDishonestyInfo;
use Sdk\DishonestyInfo\Repository\DishonestyInfoRepository;

class DishonestyInfoRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getDishonestyInfoRestfulTranslator() : DishonestyInfoRestfulTranslator
    {
        return new DishonestyInfoRestfulTranslator();
    }

    public function arrayToObject(array $expression, $dishonestyInfo = null)
    {
        return $this->translateToObject($expression, $dishonestyInfo);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    private function translateToObject(array $expression, $dishonestyInfo = null)
    {
        if (empty($expression)) {
            return new NullDishonestyInfo();
        }

        if ($dishonestyInfo == null) {
            $dishonestyInfo = new DishonestyInfo();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $dishonestyInfo->setId($id);

        if (isset($data['attributes'])) {
            $attributes = $data['attributes'];

            $dishonestyInfo->setItems($attributes);
        }
        
        return $dishonestyInfo;
    }

    public function objectToArray($dishonestyInfo, array $keys = array())
    {
        unset($dishonestyInfo);
        return $expression;
    }

    protected function getDishonestyInfoRepository()
    {
        return new DishonestyInfoRepository();
    }
}
