<?php
namespace Sdk\DishonestyInfo\Adapter\DishonestyInfo;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Marmot\Core;

use Sdk\Common\Adapter\ResourcesFetchAbleAdapterTrait;

use Sdk\DishonestyInfo\Translator\DishonestyInfoRestfulTranslator;
use Sdk\DishonestyInfo\Model\NullDishonestyInfo;
use Sdk\DishonestyInfo\Model\DishonestyInfo;

class DishonestyInfoRestfulAdapter extends GuzzleAdapter implements IDishonestyInfoAdapter
{
    use ResourcesFetchAbleAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'DISHONESTYINFOS_LIST'=>[
            'fields'=>[
            ],
            'include'=>''
        ],
        'DISHONESTYINFOS_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new DishonestyInfoRestfulTranslator();
        $this->resource = 'dishonestyInfos';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id, string $resources)
    {
        return $this->fetchOneAction($id, $resources, new NullDishonestyInfo());
    }
}
