<?php
namespace Sdk\DishonestyInfo\Model;

class DishonestyInfo
{
    /**
     * @var int $id
     */
    private $id;
    /**
     * @var array $items 数据项
     */
    private $items;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->items = array();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->items);
    }

    public function setId(int $id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setItems(array $items) : void
    {
        $this->items = $items;
    }

    public function getItems() : array
    {
        return $this->items;
    }
}
