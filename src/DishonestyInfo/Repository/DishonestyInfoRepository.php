<?php
namespace Sdk\DishonestyInfo\Repository;

use Sdk\Common\Repository\ResourcesFetchRepositoryTrait;

use Marmot\Core;

use Sdk\DishonestyInfo\Model\DishonestyInfo;
use Sdk\DishonestyInfo\Adapter\DishonestyInfo\DishonestyInfoRestfulAdapter;
use Sdk\DishonestyInfo\Adapter\DishonestyInfo\IDishonestyInfoAdapter;
use Marmot\Framework\Classes\Repository;

class DishonestyInfoRepository extends Repository implements IDishonestyInfoAdapter
{
    use ResourcesFetchRepositoryTrait;

    const LIST_MODEL_UN = 'DISHONESTYINFOS_LIST';
    const FETCH_ONE_MODEL_UN = 'DISHONESTYINFOS_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new DishonestyInfoRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : IDishonestyInfoAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IDishonestyInfoAdapter
    {
        return new DishonestyInfoMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
