<?php
namespace Sdk\RepairRecord\Model;

class Dishonesty
{
    /**
     * [$photocopy 行政相对人主要登记证照复印件]
     * @var [array]
     */
    private $photocopy;
    /**
     * [$penaltyMaterials 行政处罚证明材料]
     * @var [array]
     */
    private $penaltyMaterials;

    public function __construct(
        array $photocopy = array(),
        array $penaltyMaterials = array()
    ) {
        $this->photocopy = $photocopy;
        $this->penaltyMaterials = $penaltyMaterials;
    }

    public function __destruct()
    {
        unset($this->photocopy);
        unset($this->penaltyMaterials);
    }

    public function getPhotocopy() : array
    {
        return $this->photocopy;
    }

    public function getPenaltyMaterials() : array
    {
        return $this->penaltyMaterials;
    }
}
