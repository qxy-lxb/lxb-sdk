<?php
namespace Sdk\RepairRecord\Model;

use Sdk\RepairRecord\Repository\RepairRecordRepository;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;
use Sdk\Enterprise\Model\Enterprise;
use Sdk\DishonestyInfo\Model\DishonestyInfo;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\ApplyAbleSmsTrait;
use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Adapter\IApplyAbleAdapter;
use Sdk\Member\Model\Member;
use Sdk\TrainingRecord\Model\TrainingRecord;
use Sdk\ServiceBusiness\Model\ServiceBusiness;
use Sdk\Notify\Model\SmsMessage;

class RepairRecord implements IObject, IOperatAble, IApplyAble
{
    use Object, OperatAbleTrait, ApplyAbleSmsTrait, RepairRecordTimeTrait;

    const DISHONEST_TYPE = array(
        'ORDINARY_DISHONESTY' => 1,
        'SERIOUS_DISHONESTY' => 2,
        'SPECIAL_DISHONESTY' => 3,
        'JUDICIAL_DISHONESTY' => 4
    );

    /**
     * @var STEP['RECTIFY_FAITH']  纠失信
     * @var STEP['COMMITMENT']  做承诺
     * @var STEP['BE_TRAINED']  受培训
     * @var STEP['REPORT']  提报告
     */

    const STEP = array(
        'RECTIFY_FAITH' => '纠失信',
        'COMMITMENT' => '做承诺',
        'BE_TRAINED' => '受培训',
        'REPORT' => '提报告'
    );

    /**
     * [$id 主键Id]
     * @var [int]
     */
    protected $id;
    /**
     * [$dishonestType 失信类型]
     * @var [int]
     */
    private $dishonestType;
    /**
     * [$dishonestyInfo 失信信息]
     * @var [array]
     */
    private $dishonestyInfo;
    /**
     * [$enterprise 企业信息]
     * @var [object]
     */
    private $enterprise;
    /**
     * [$dishonesty 纠失信信息]
     * @var [object]
     */
    private $dishonesty;
    /**
     * [$promise 做承诺信息]
     * @var [Object]
     */
    private $promise;
    /**
     * [$rejectReason 驳回原因]
     * @var [array]
     */
    private $rejectReason;
    /**
     * [$determineStatus 判定状态]
     * @var [int]
     */
    private $determineStatus;
    /**
     * [$deliverStatus 递交状态]
     * @var [int]
     */
    private $deliverStatus;
    /**
     * [$downloadNumber 下载数量]
     * @var [int]
     */
    private $downloadNumber;
    /**
     * [$enterpriseName 企业名称]
     * @var [string]
     */
    private $enterpriseName;
    /**
     * [$enterpriseCode 统一社会信用代码]
     * @var [string]
     */
    private $enterpriseCode;
    /**
     * [$cellphone 手机号]
     * @var [string]
     */
    private $cellphone;
    /**
     * [$member 用户信息]
     * @var [object]
     */
    private $member;
    /**
     * [$repository]
     * @var [object]
     */
    private $repository;
    /**
     * [$training 做承诺信息]
     * @var [Object]
     */
    private $training;
    /**
     * [$trainingRecord 培训记录]
     * @var [Object]
     */
    private $trainingRecord;
     /**
     * [$report 提报告]
     * @var [Object]
     */
    private $report;
    /**
     * [$serviceBusiness 报告记录]
     * @var [Object]
     */
    private $serviceBusiness;
   
    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->dishonestType = 0;
        $this->dishonestyInfo = array();
        $this->enterprise = new Enterprise();
        $this->dishonesty  = new Dishonesty();
        $this->promise = new Promise();
        $this->rejectReason = array();
        $this->determineStatus = 0;
        $this->deliverStatus = 0;
        $this->downloadNumber = 0;
        $this->enterpriseName = '';
        $this->enterpriseCode = '';
        $this->cellphone = '';
        $this->member = new Member();
        $this->repository = new RepairRecordRepository();
        $this->training = new Training();
        $this->report = new Report();
        $this->trainingRecord = new TrainingRecord();
        $this->serviceBusiness = new ServiceBusiness();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->dishonestType);
        unset($this->dishonestyInfo);
        unset($this->enterprise);
        unset($this->dishonesty);
        unset($this->promise);
        unset($this->rejectReason);
        unset($this->determineStatus);
        unset($this->deliverStatus);
        unset($this->downloadNumber);
        unset($this->enterpriseName);
        unset($this->enterpriseCode);
        unset($this->cellphone);
        unset($this->member);
        unset($this->repository);
        unset($this->training);
        unset($this->report);
        unset($this->trainingRecord);
        unset($this->serviceBusiness);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    protected function getRepository() : RepairRecordRepository
    {
        return $this->repository;
    }

    public function setDishonestType(int $dishonestType) : void
    {
        $this->dishonestType = $dishonestType;
    }

    public function getDishonestType() : int
    {
        return $this->dishonestType;
    }

    public function setDishonestyInfo(array $dishonestyInfo) : void
    {
        $this->dishonestyInfo = $dishonestyInfo;
    }

    public function getDishonestyInfo() : array
    {
        return $this->dishonestyInfo;
    }

    public function setEnterprise(Enterprise $enterprise) : void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise() : Enterprise
    {
        return $this->enterprise;
    }

    public function setDishonesty(Dishonesty $dishonesty) : void
    {
        $this->dishonesty = $dishonesty;
    }

    public function getDishonesty() : Dishonesty
    {
        return $this->dishonesty;
    }

    public function setPromise(Promise $promise) : void
    {
        $this->promise = $promise;
    }

    public function getPromise() : Promise
    {
        return $this->promise;
    }
    /**
     * 纠失信
     * @return bool 纠失信
     */
    public function rectify() : bool
    {
        return $this->getRepository()->rectify($this);
    }
    /**
     * 做承诺
     * @return bool 做承诺
     */
    public function promise() : bool
    {
        return $this->getRepository()->promise($this);
    }
    /**
     * 提交平台审核
     * @return bool 提交平台审核
     */
    public function submit() : bool
    {
        return $this->getRepository()->submit($this);
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function setApplyStatus(int $applyStatus) : void
    {
        $this->applyStatus = $applyStatus;
    }

    public function getApplyStatus() : int
    {
        return $this->applyStatus;
    }
    /**
     * 判定
     * @return bool 判定
     */
    public function determine() : bool
    {
        return $this->getRepository()->determine($this);
    }

    protected function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return $this->getRepository();
    }

    public function setRejectReason(array $rejectReason) : void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason() : array
    {
        return $this->rejectReason;
    }

    public function setDetermineStatus(int $determineStatus) : void
    {
        $this->determineStatus = $determineStatus;
    }

    public function getDetermineStatus() : int
    {
        return $this->determineStatus;
    }

    public function setDeliverStatus(int $deliverStatus) : void
    {
        $this->deliverStatus = $deliverStatus;
    }

    public function getDeliverStatus() : int
    {
        return $this->deliverStatus;
    }

    public function setDownloadNumber(int $downloadNumber) : void
    {
        $this->downloadNumber = $downloadNumber;
    }

    public function getDownloadNumber() : int
    {
        return $this->downloadNumber;
    }

    public function setEnterpriseName(string $enterpriseName) : void
    {
        $this->enterpriseName = $enterpriseName;
    }

    public function getEnterpriseName() : string
    {
        return $this->enterpriseName;
    }

    public function setUnifiedSocialCreditCode(string $enterpriseCode) : void
    {
        $this->enterpriseCode = $enterpriseCode;
    }

    public function getUnifiedSocialCreditCode() : string
    {
        return $this->enterpriseCode;
    }

    public function setCellphone(string $cellphone) : void
    {
        $this->cellphone = $cellphone;
    }

    public function getCellphone() : string
    {
        return $this->cellphone;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    /**
     * 打包下载
     * @return bool 打包下载
     */
    public function download() : bool
    {
        return $this->getRepository()->download($this);
    }
    /**
     * 申请递交
     * @return bool 申请递交
     */
    public function deliver() : bool
    {
        return $this->getRepository()->deliver($this);
    }

    public function setTraining(Training $training) : void
    {
        $this->training = $training;
    }

    public function getTraining() : Training
    {
        return $this->training;
    }

    public function setReport(Report $report) : void
    {
        $this->report = $report;
    }

    public function getReport() : Report
    {
        return $this->report;
    }
    /**
     * 作培训
     * @return bool 作培训
     */
    public function training() : bool
    {
        return $this->getRepository()->training($this);
    }

    public function setTrainingRecord(TrainingRecord $trainingRecord) : void
    {
        $this->trainingRecord = $trainingRecord;
    }

    public function getTrainingRecord() : TrainingRecord
    {
        return $this->trainingRecord;
    }

    /**
     * 自动提交
     * @return bool 自动提交
     */
    public function uploadMaterial() : bool
    {
        return $this->getRepository()->uploadMaterial($this);
    }
    /**
     * 提报告
     * @return bool 提报告
     */
    public function report() : bool
    {
        return $this->getRepository()->report($this);
    }

    public function setServiceBusiness(ServiceBusiness $serviceBusiness) : void
    {
        $this->serviceBusiness = $serviceBusiness;
    }

    public function getServiceBusiness() : ServiceBusiness
    {
        return $this->serviceBusiness;
    }

    /**
     * 自动提交
     * @return bool 自动提交报告
     */
    public function uploadReport() : bool
    {
        return $this->getRepository()->uploadReport($this);
    }

    public function approveSms()
    {
        $template = REPAIRRECORD_APPROVE_TEMPLATE_ID_AUTH;

        $message = new SmsMessage($template);

        $targets = $this->getCellphone();
        $dishonestyInfo = $this->getDishonestyInfo();
        $enterpriseName = $this->getEnterpriseName();

        $message->setTargets($targets);

        $content = array(
            'enterpriseName' => $enterpriseName,
            'punishmentDocumentNumber' => $dishonestyInfo['punishmentDocumentNumber'],
        );
        
        $message->setContent($content);
        $message->send();

        return true;
    }

    public function rejectActionSms()
    {
        $template = REPAIRRECORD_REJECT_TEMPLATE_ID_AUTH;

        $message = new SmsMessage($template);

        $targets = $this->getCellphone();
        $dishonestyInfo = $this->getDishonestyInfo();
        $enterpriseName = $this->getEnterpriseName();

        $message->setTargets($targets);

        $content = array(
            'enterpriseName' => $enterpriseName,
            'punishmentDocumentNumber' => $dishonestyInfo['punishmentDocumentNumber'],
        );
        
        $message->setContent($content);
        $message->send();
        
        return true;
    }
}
