<?php
namespace Sdk\RepairRecord\Model;

class Report
{
    /**
     * [$organizationName 培训机构名称]
     * @var [string]
    */
    private $organizationName;
    /**
     * [$reportName 报告名称]
     * @var [string]
    */
    private $reportName;
    /**
     * [$issueTime 出具时间]
     * @var [string]
    */
    private $issueTime;
    /**
     * [$endTime 截止时间]
     * @var [string]
    */
    private $endTime;
    /**
     * [$reportNumber 报告编号]
     * @var [string]
    */
    private $reportNumber;
    /**
     * [$grade 信用等级]
     * @var [string]
    */
    private $grade;
    /**
     * [$electronicReport 信用报告电子版]
     * @var [array]
    */
    private $electronicReport;
    /**
     * [$reportUploadMode 报告上传方式]
     * @var [int]
    */
    private $reportUploadMode;

    public function __construct(
        string $organizationName = '',
        string $reportName = '',
        string $issueTime = '',
        string $endTime = '',
        string $reportNumber = '',
        string $grade = '',
        array $electronicReport = array(),
        int $reportUploadMode = 0
    ) {
        $this->organizationName = $organizationName;
        $this->reportName = $reportName;
        $this->issueTime = $issueTime;
        $this->endTime = $endTime;
        $this->reportNumber = $reportNumber;
        $this->grade = $grade;
        $this->electronicReport = $electronicReport;
        $this->reportUploadMode = $reportUploadMode;
    }

    public function __destruct()
    {
        unset($this->organizationName);
        unset($this->reportName);
        unset($this->issueTime);
        unset($this->endTime);
        unset($this->reportNumber);
        unset($this->grade);
        unset($this->electronicReport);
        unset($this->reportUploadMode);
    }

    public function getOrganizationName() : string
    {
        return $this->organizationName;
    }

    public function getReportName() : string
    {
        return $this->reportName;
    }

    public function getIssueTime() : string
    {
        return $this->issueTime;
    }

    public function getEndTime() : string
    {
        return $this->endTime;
    }

    public function getReportNumber() : string
    {
        return $this->reportNumber;
    }

    public function getGrade() : string
    {
        return $this->grade;
    }

    public function getElectronicReport() : array
    {
        return $this->electronicReport;
    }

    public function getReportUploadMode() : int
    {
        return $this->reportUploadMode;
    }
}
