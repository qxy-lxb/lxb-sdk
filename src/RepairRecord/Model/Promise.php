<?php
namespace Sdk\RepairRecord\Model;

class Promise
{
    /**
     * [$commitmentBook 承诺书]
     * @var [array]
     */
    private $commitmentBook;

    public function __construct(
        array $commitmentBook = array()
    ) {
        $this->commitmentBook = $commitmentBook;
    }

    public function __destruct()
    {
        unset($this->commitmentBook);
    }

    public function getCommitmentBook() : array
    {
        return $this->commitmentBook;
    }
}
