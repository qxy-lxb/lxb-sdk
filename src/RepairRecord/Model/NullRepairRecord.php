<?php
namespace Sdk\RepairRecord\Model;

use Marmot\Interfaces\INull;
use Sdk\Common\Model\NullEnableAbleTrait;
use Sdk\Common\Model\NullOperatAbleTrait;

use Marmot\Core;

class NullRepairRecord extends RepairRecord implements INull
{
    use NullOperatAbleTrait;

    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
    /**
     * 纠失信
     * @return bool 纠失信
     */
    public function rectify() : bool
    {
        return $this->resourceNotExist();
    }
    /**
     * 做承诺
     * @return bool 做承诺
     */
    public function promise() : bool
    {
        return $this->resourceNotExist();
    }
    /**
     * 提交平台审核
     * @return bool 提交平台审核
     */
    public function submit() : bool
    {
        return $this->resourceNotExist();
    }
    /**
     * 判定
     * @return bool 判定
     */
    public function determine() : bool
    {
        return $this->resourceNotExist();
    }
}
