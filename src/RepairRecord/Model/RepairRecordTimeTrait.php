<?php
namespace Sdk\RepairRecord\Model;

/**
 * 时间性状,包括 纠失信更新时间, 做承诺更新时间, 受培训更新时间, 提报告更新时间.
 * @author chloroplast
 * @version 2.0.0:2020.06.24
 */

trait RepairRecordTimeTrait
{
    /**
     * @var int $rectifyUpdateTime 纠失信更新时间
     */
    protected $rectifyUpdateTime;
    /**
     * @var int $promiseUpdateTime 做承诺更新时间
     */
    protected $promiseUpdateTime;
    /**
     * @var int $trainingUpdateTime 受培训更新时间
     */
    protected $trainingUpdateTime;
    /**
     * @var int $reportUpdateTime 提报告更新时间
     */
    protected $reportUpdateTime;

    /**
     * 纠失信更新时间
     * @param int $rectifyUpdateTime 纠失信更新时间
     */
    public function setRectifyUpdateTime(int $rectifyUpdateTime)
    {
        $this->rectifyUpdateTime = $rectifyUpdateTime;
    }

    /**
     * 获取纠失信更新时间
     * @return int $rectifyUpdateTime 纠失信更新时间
     */
    public function getRectifyUpdateTime() : int
    {
        return $this->rectifyUpdateTime;
    }

    /**
     * 做承诺更新时间
     * @param int $promiseUpdateTime 做承诺更新时间
     */
    public function setPromiseUpdateTime(int $promiseUpdateTime)
    {
        $this->promiseUpdateTime = $promiseUpdateTime;
    }

    /**
     * 获取做承诺更新时间
     * @return int $promiseUpdateTime 做承诺更新时间
     */
    public function getPromiseUpdateTime() : int
    {
        return $this->promiseUpdateTime;
    }

    /**
     * 受培训更新时间
     * @param int $trainingUpdateTime 受培训更新时间
     */
    public function setTrainingUpdateTime(int $trainingUpdateTime)
    {
        $this->trainingUpdateTime = $trainingUpdateTime;
    }

    /**
     * 获取受培训更新时间
     * @return int $trainingUpdateTime 受培训更新时间
     */
    public function getTrainingUpdateTime() : int
    {
        return $this->trainingUpdateTime;
    }

    /**
     * 提报告更新时间
     * @param int $reportUpdateTime 提报告更新时间
     */
    public function setReportUpdateTime(int $reportUpdateTime)
    {
        $this->reportUpdateTime = $reportUpdateTime;
    }

    /**
     * 获取提报告更新时间
     * @return int $reportUpdateTime 提报告更新时间
     */
    public function getReportUpdateTime() : int
    {
        return $this->reportUpdateTime;
    }
}
