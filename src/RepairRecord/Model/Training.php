<?php
namespace Sdk\RepairRecord\Model;

class Training
{

    const INITIATOR = array(
        'INITIATOR_LEADING_DEPARTMENT' => 1,
        'INITIATOR_PILOT_UNIT' => 2,
        'INITIATOR_ORGANIZATION_NAME' => 3,
        'INITIATOR_NO_OPTIONS' =>0
    );
    /**
     * [$initiator initiator]
     * @var [int]
    */
    private $initiator;
    /**
     * [$organizationName 培训机构名称]
     * @var [string]
    */
    private $organizationName;
    /**
     * [$location 培训地点]
     * @var [string]
    */
    private $location;
    /**
     * [$endTime 结束时间]
     * @var [string]
    */
    private $endTime;
    /**
     * [$trainingClass 培训班级]
     * @var [string]
    */
    private $trainingClass;
    /**
     * [$result 培训成绩]
     * @var [string]
    */
    private $result;
    /**
     * [$voucherMaterial 培训证明材料]
     * @var [array]
    */
    private $voucherMaterial;
    /**
     * [$trainingUploadMode 培训上传方式]
     * @var [int]
    */
    private $trainingUploadMode;
    /**
     * [$examReport 成绩单]
     * @var [string]
    */
    private $examReport;

    public function __construct(
        int $initiator = self::INITIATOR['INITIATOR_NO_OPTIONS'],
        string $organizationName = '',
        string $location = '',
        string $endTime = '',
        string $trainingClass = '',
        string $result = '',
        array $voucherMaterial = array(),
        int $trainingUploadMode = 0,
        string $examReport = ''
    ) {
        $this->initiator = $initiator;
        $this->organizationName = $organizationName;
        $this->location = $location;
        $this->endTime = $endTime;
        $this->trainingClass = $trainingClass;
        $this->result = $result;
        $this->voucherMaterial = $voucherMaterial;
        $this->trainingUploadMode = $trainingUploadMode;
        $this->examReport = $examReport;
    }

    public function __destruct()
    {
        unset($this->initiator);
        unset($this->organizationName);
        unset($this->location);
        unset($this->endTime);
        unset($this->trainingClass);
        unset($this->result);
        unset($this->voucherMaterial);
        unset($this->trainingUploadMode);
        unset($this->examReport);
    }

    public function getInitiator() : int
    {
        return $this->initiator;
    }

    public function getOrganizationName() : string
    {
        return $this->organizationName;
    }

    public function getLocation() : string
    {
        return $this->location;
    }

    public function getEndTime() : string
    {
        return $this->endTime;
    }

    public function getTrainingClass() : string
    {
        return $this->trainingClass;
    }

    public function getResult() : string
    {
        return $this->result;
    }

    public function getVoucherMaterial() : array
    {
        return $this->voucherMaterial;
    }

    public function getTrainingUploadMode() : int
    {
        return $this->trainingUploadMode;
    }

    public function getExamReport() : string
    {
        return $this->examReport;
    }
}
