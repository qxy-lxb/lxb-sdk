<?php
namespace Sdk\RepairRecord\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;
use Sdk\Common\Repository\ApplyAbleRepositoryTrait;

use Sdk\RepairRecord\Adapter\RepairRecord\IRepairRecordAdapter;
use Sdk\RepairRecord\Adapter\RepairRecord\RepairRecordMockAdapter;
use Sdk\RepairRecord\Adapter\RepairRecord\RepairRecordRestfulAdapter;
use Sdk\RepairRecord\Model\RepairRecord;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class RepairRecordRepository extends Repository implements IRepairRecordAdapter
{
    use FetchRepositoryTrait,
        AsyncRepositoryTrait,
        ErrorRepositoryTrait,
        OperatAbleRepositoryTrait,
        ApplyAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'REPAIRRECORD_LIST';
    const FETCH_ONE_MODEL_UN = 'REPAIRRECORD_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new RepairRecordRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IRepairRecordAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IRepairRecordAdapter
    {
        return new RepairRecordMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function rectify(RepairRecord $repairRecord) : bool
    {
        return $this->getAdapter()->rectify($repairRecord);
    }

    public function promise(RepairRecord $repairRecord) : bool
    {
        return $this->getAdapter()->promise($repairRecord);
    }

    public function submit(RepairRecord $repairRecord) : bool
    {
        return $this->getAdapter()->submit($repairRecord);
    }

    public function determine(RepairRecord $repairRecord) : bool
    {
        return $this->getAdapter()->determine($repairRecord);
    }

    public function download(RepairRecord $repairRecord) : bool
    {
        return $this->getAdapter()->download($repairRecord);
    }

    public function deliver(RepairRecord $repairRecord) : bool
    {
        return $this->getAdapter()->deliver($repairRecord);
    }

    public function training(RepairRecord $repairRecord) : bool
    {
        return $this->getAdapter()->training($repairRecord);
    }

    public function uploadMaterial(RepairRecord $repairRecord) : bool
    {
        return $this->getAdapter()->uploadMaterial($repairRecord);
    }
    
    public function report(RepairRecord $repairRecord) : bool
    {
        return $this->getAdapter()->report($repairRecord);
    }

    public function uploadReport(RepairRecord $repairRecord) : bool
    {
        return $this->getAdapter()->uploadReport($repairRecord);
    }
}
