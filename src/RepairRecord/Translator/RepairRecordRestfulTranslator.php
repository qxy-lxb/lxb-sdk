<?php
namespace Sdk\RepairRecord\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\RepairRecord\Model\RepairRecord;
use Sdk\RepairRecord\Model\Promise;
use Sdk\RepairRecord\Model\Training;
use Sdk\RepairRecord\Model\Dishonesty;
use Sdk\Enterprise\Model\Enterprise;
use Sdk\RepairRecord\Model\LegalPersonInfo;
use Sdk\RepairRecord\Model\ContactsInfo;
use Sdk\RepairRecord\Model\NullRepairRecord;
use Sdk\Member\Model\Member;
use Sdk\RepairRecord\Model\Report;

use Sdk\Member\Translator\MemberRestfulTranslator;
use Sdk\DishonestyInfo\Model\DishonestyInfo;

/**
 * 屏蔽类中所有PMD警告
 *
 * @SuppressWarnings(PHPMD)
 */
class RepairRecordRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getMemberRestfulTranslator()
    {
        return new MemberRestfulTranslator();
    }

    public function arrayToObject(array $expression, $repairRecord = null)
    {
        return $this->translateToObject($expression, $repairRecord);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $repairRecord = null)
    {
        if (empty($expression)) {
            return NullRepairRecord::getInstance();
        }

        if ($repairRecord == null) {
            $repairRecord = new RepairRecord();
        }

        $data =  $expression['data'];

        $id = $data['id'];
        $repairRecord->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['enterpriseName'])) {
            $repairRecord->setEnterpriseName($attributes['enterpriseName']);
        }
        if (isset($attributes['enterpriseCode'])) {
            $repairRecord->setUnifiedSocialCreditCode($attributes['enterpriseCode']);
        }
        if (isset($attributes['cellphone'])) {
            $repairRecord->setCellphone($attributes['cellphone']);
        }
        if (isset($attributes['severity'])) {
            $repairRecord->setDishonestType($attributes['severity']);
        }
        if (isset($attributes['determineStatus'])) {
            $repairRecord->setDetermineStatus($attributes['determineStatus']);
        }
        if (isset($attributes['dishonestyInfo'])) {
            $repairRecord->setDishonestyInfo($attributes['dishonestyInfo']);
        }

        $photocopy = isset($attributes['photocopy'])
        ? $attributes['photocopy']
        : array();
        $penaltyMaterials = isset($attributes['penaltyMaterials'])
        ? $attributes['penaltyMaterials']
        : array();
        $repairRecord->setDishonesty(
            new Dishonesty(
                $photocopy,
                $penaltyMaterials
            )
        );
        $commitmentBook = isset($attributes['commitmentBook'])
        ? $attributes['commitmentBook']
        : array();
        $repairRecord->setPromise(
            new Promise(
                $commitmentBook
            )
        );

        $result = isset($attributes['trainingMaterials']['result'])
        ? $attributes['trainingMaterials']['result']
        : '';
        $endTime = isset($attributes['trainingMaterials']['endTime'])
        ? $attributes['trainingMaterials']['endTime']
        : '';
        $location = isset($attributes['trainingMaterials']['location'])
        ? $attributes['trainingMaterials']['location']
        : '';
        $initiator = isset($attributes['trainingMaterials']['initiator'])
        ? $attributes['trainingMaterials']['initiator']
        : Training::INITIATOR['INITIATOR_NO_OPTIONS'];
        $trainingClass = isset($attributes['trainingMaterials']['trainingClass'])
        ? $attributes['trainingMaterials']['trainingClass']
        : '';
        $voucherMaterial = isset($attributes['trainingMaterials']['trainingProof'])
        ? $attributes['trainingMaterials']['trainingProof']
        : array();
        $organizationName = isset($attributes['trainingMaterials']['organizationName'])
        ? $attributes['trainingMaterials']['organizationName']
        : '';
        $trainingUploadMode = isset($attributes['trainingUploadMode'])
        ? $attributes['trainingUploadMode']
        : 0;
        $examReport = isset($attributes['trainingMaterials']['examReport'])
        ? $attributes['trainingMaterials']['examReport']
        : '';
        $repairRecord->setTraining(
            new Training(
                $initiator,
                $organizationName,
                $location,
                $endTime,
                $trainingClass,
                $result,
                $voucherMaterial,
                $trainingUploadMode,
                $examReport
            )
        );

        $reportUploadMode = isset($attributes['reportUploadMode'])
        ? $attributes['reportUploadMode']
        : 0;
        $reportOrganizationName = isset($attributes['reportMaterials']['reportOrganizationName'])
        ? $attributes['reportMaterials']['reportOrganizationName']
        : '';
        $reportName = isset($attributes['reportMaterials']['reportName'])
        ? $attributes['reportMaterials']['reportName']
        : '';
        $reportIssueTime = isset($attributes['reportMaterials']['reportIssueTime'])
        ? $attributes['reportMaterials']['reportIssueTime']
        : '';
        $reportEndTime = isset($attributes['reportMaterials']['reportEndTime'])
        ? $attributes['reportMaterials']['reportEndTime']
        : '';
        $reportNumber = isset($attributes['reportMaterials']['reportNumber'])
        ? $attributes['reportMaterials']['reportNumber']
        : '';
        $grade = isset($attributes['reportMaterials']['grade'])
        ? $attributes['reportMaterials']['grade']
        : '';
        $electronicReport = isset($attributes['reportMaterials']['electronicReport'])
        ? $attributes['reportMaterials']['electronicReport']
        : array();
        $repairRecord->setReport(
            new Report(
                $reportOrganizationName,
                $reportName,
                $reportIssueTime,
                $reportEndTime,
                $reportNumber,
                $grade,
                $electronicReport,
                $reportUploadMode
            )
        );
        if (isset($attributes['applyStatus'])) {
            $repairRecord->setApplyStatus($attributes['applyStatus']);
        }
        if (isset($attributes['rejectReason'])) {
            $repairRecord->setRejectReason($attributes['rejectReason']);
        }
        if (isset($attributes['deliverStatus'])) {
            $repairRecord->setDeliverStatus($attributes['deliverStatus']);
        }
        if (isset($attributes['downloadNumber'])) {
            $repairRecord->setDownloadNumber($attributes['downloadNumber']);
        }
        if (isset($attributes['status'])) {
            $repairRecord->setstatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $repairRecord->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $repairRecord->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $repairRecord->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['rectifyUpdateTime'])) {
            $repairRecord->setRectifyUpdateTime($attributes['rectifyUpdateTime']);
        }
        if (isset($attributes['promiseUpdateTime'])) {
            $repairRecord->setPromiseUpdateTime($attributes['promiseUpdateTime']);
        }
        if (isset($attributes['trainingUpdateTime'])) {
            $repairRecord->setTrainingUpdateTime($attributes['trainingUpdateTime']);
        }
        if (isset($attributes['reportUpdateTime'])) {
            $repairRecord->setReportUpdateTime($attributes['reportUpdateTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);

            $repairRecord->setMember($this->getMemberRestfulTranslator()->arrayToObject($member));
        }
        if (isset($relationships['enterprise']['data'])) {
            $enterprise = $repairRecord->getEnterprise();
            $enterprise->setId($relationships['enterprise']['data']['id']);
        }

        return $repairRecord;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($repairRecord, array $keys = array())
    {
        if (!$repairRecord instanceof RepairRecord) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'photocopy',
                'penaltyMaterials',
                'commitmentBook',
                'dishonestyInfo',
                'severity',
                'rejectReason',
                'enterprise',
                'initiator',
                'organizationName',
                'location',
                'endTime',
                'trainingClass',
                'result',
                'voucherMaterial',
                'trainingRecord',
                'trainingProof',
                'examReport',
                'reportOrganizationName',
                'reportName',
                'issueTime',
                'reportEndTime',
                'reportNumber',
                'grade',
                'electronicReport'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'repairRecords'
            )
        );
        
        if (in_array('id', $keys)) {
            $expression['data']['id'] = $repairRecord->getId();
        }

        $attributes = array();

        if (in_array('photocopy', $keys)) {
            $attributes['photocopy'] = $repairRecord->getDishonesty()->getPhotocopy();
        }
        if (in_array('penaltyMaterials', $keys)) {
            $attributes['penaltyMaterials'] = $repairRecord->getDishonesty()->getPenaltyMaterials();
        }
        if (in_array('commitmentBook', $keys)) {
            $attributes['commitmentBook'] = $repairRecord->getPromise()->getCommitmentBook();
        }
        if (in_array('dishonestyInfo', $keys)) {
            $attributes['dishonestyInfo'] = $repairRecord->getDishonestyInfo();
        }
        if (in_array('rejectReason', $keys)) {
            $attributes['rejectReason'] = $repairRecord->getRejectReason();
        }

        if (in_array('severity', $keys)) {
            $attributes['severity'] = $repairRecord->getDishonestType();
        }
        if (in_array('initiator', $keys)) {
            $attributes['initiator'] = $repairRecord->getTraining()->getInitiator();
        }
        if (in_array('organizationName', $keys)) {
            $attributes['organizationName'] = $repairRecord->getTraining()->getOrganizationName();
        }
        if (in_array('location', $keys)) {
            $attributes['location'] = $repairRecord->getTraining()->getLocation();
        }
        if (in_array('endTime', $keys)) {
            $attributes['endTime'] = $repairRecord->getTraining()->getEndTime();
        }
        if (in_array('trainingClass', $keys)) {
            $attributes['trainingClass'] = $repairRecord->getTraining()->getTrainingClass();
        }
        if (in_array('result', $keys)) {
            $attributes['result'] = $repairRecord->getTraining()->getResult();
        }
        if (in_array('voucherMaterial', $keys)) {
            $attributes['trainingProof'] = $repairRecord->getTraining()->getVoucherMaterial();
        }
        if (in_array('trainingProof', $keys)) {
            $attributes['trainingProof'] = array(
                            "https://credit-lvxinbao.oss-cn-shanghai.aliyuncs.com/material/training_certificate.jpg"
                        );
        }
        if (in_array('examReport', $keys)) {
            $attributes['examReport'] = "o_1cli98qc9dfud59mf5ivkgm9t.png";
        }

        if (in_array('reportOrganizationName', $keys)) {
            $attributes['reportOrganizationName'] = $repairRecord->getReport()->getOrganizationName();
        }
        if (in_array('reportName', $keys)) {
            $attributes['reportName'] = $repairRecord->getReport()->getReportName();
        }
        if (in_array('issueTime', $keys)) {
            $attributes['reportIssueTime'] = $repairRecord->getReport()->getIssueTime();
        }
        if (in_array('reportEndTime', $keys)) {
            $attributes['reportEndTime'] = $repairRecord->getReport()->getEndTime();
        }
        if (in_array('reportNumber', $keys)) {
            $attributes['reportNumber'] = $repairRecord->getReport()->getReportNumber();
        }
        if (in_array('grade', $keys)) {
            $attributes['grade'] = $repairRecord->getReport()->getGrade();
        }
        if (in_array('electronicReport', $keys)) {
            $attributes['electronicReport'] = $repairRecord->getReport()->getElectronicReport();
        }
        
        $expression['data']['attributes'] = $attributes;

        if (in_array('enterprise', $keys)) {
            $expression['data']['relationships']['enterprise']['data'] = array(
                array(
                    'type' => 'enterprises',
                    'id' => $repairRecord->getEnterprise()->getId()
                )
             );
        }
        if (in_array('trainingRecord', $keys)) {
            $expression['data']['relationships']['trainingRecord']['data'] = array(
                array(
                    'type' => 'trainingRecords',
                    'id' => $repairRecord->getTrainingRecord()->getId()
                )
             );
        }
        if (in_array('reportRecord', $keys)) {
            $expression['data']['relationships']['reportRecord']['data'] = array(
                array(
                    'type' => 'reportRecords',
                    'id' => $repairRecord->getServiceBusiness()->getId()
                )
             );
        }

        return $expression;
    }
}
