<?php
namespace Sdk\RepairRecord\Adapter\RepairRecord;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\RepairRecord\Model\RepairRecord;
use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IApplyAbleAdapter;

interface IRepairRecordAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter, IApplyAbleAdapter
{
    public function rectify(RepairRecord $repairRecord);

    public function promise(RepairRecord $repairRecord);

    public function submit(RepairRecord $repairRecord);
    
    public function determine(RepairRecord $repairRecord);

    public function download(RepairRecord $repairRecord);
    
    public function deliver(RepairRecord $repairRecord);

    public function training(RepairRecord $repairRecord);

    public function uploadMaterial(RepairRecord $repairRecord);
    
    public function report(RepairRecord $repairRecord);

    public function uploadReport(RepairRecord $repairRecord);
}
