<?php
namespace Sdk\RepairRecord\Adapter\RepairRecord;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Marmot\Core;

use Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\CommonMapErrorsTrait;

use Sdk\RepairRecord\Translator\RepairRecordRestfulTranslator;
use Sdk\RepairRecord\Model\NullRepairRecord;
use Sdk\RepairRecord\Model\RepairRecord;

class RepairRecordRestfulAdapter extends GuzzleAdapter implements IRepairRecordAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        ApplyAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'REPAIRRECORD_LIST'=>[
            'fields'=>[],
            'include'=>''
        ],
        'REPAIRRECORD_FETCH_ONE'=>[
            'fields'=>[
                'members'=>'nickName',
                'enterprises'=>'id'
            ],
            'include'=>'members,enterprise'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new RepairRecordRestfulTranslator();
        $this->resource = 'repairRecords';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            9001 => ENTERPRISE_NAME_FORMAT_ERROR,
            9002 => DISHONESTY_INFO_FORMAT_ERROR,
            9003 => REPAIRRECORD_TYPE_NOT_EXIST,
            9004 => REPAIR_RECORD_REJECTREASON_FORMAT_ERROR,
            9005 => REPAIR_RECORD_DELIVER_STATUS_ALREADY_SUBMITTED,
            9006 => REPAIR_RECORD_DETERMINE_STATUS_ALREADY_DETERMINED,
            9007 => REPAIR_RECORD_APPLY_STATUS_NOT_UNSUBMITTED,
            9016 => REPORT_ORGANIZATION_FORMAT_ERROR,
            9017 => REPORT_NAME_FORMAT_ERROR,
            9018 => ISSUE_TIME_FORMAT_ERROR,
            9019 => END_TIME_FORMAT_ERROR,
            9020 => REPORT_NUMBER_FORMAT_ERROR,
            9021 => REPORT_GRADE_FORMAT_ERROR,
            9022 => ELECTRONIC_REPORT_FORMAT_ERROR
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullRepairRecord());
    }

    public function addAction(RepairRecord $repairRecord) : bool
    {
        $data = $this->getTranslator()->objectToArray($repairRecord, array(
            'dishonestyInfo',
            'enterprise'
        ));
        
        $this->post($this->getResource(), $data);

        if ($this->isSuccess()) {
            $this->translateToObject($repairRecord);
            return true;
        }

        return false;
    }

    public function editAction(RepairRecord $repairRecord) : bool
    {
        $data = $this->getTranslator()->objectToArray($repairRecord, array());

        $this->patch($this->getResource().'/'.$repairRecord->getId(), $data);

        if ($this->isSuccess()) {
            $this->translateToObject($repairRecord);
            return true;
        }

        return false;
    }

    public function rectify(RepairRecord $repairRecord) : bool
    {
        return $this->rectifyAction($repairRecord);
    }

    protected function rectifyAction(RepairRecord $repairRecord) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $repairRecord,
            array(
                'photocopy',
                'penaltyMaterials'
            )
        );

        $this->patch(
            $this->getResource().'/'.$repairRecord->getId().'/rectify',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($repairRecord);
            return true;
        }

        return false;
    }

    public function promise(RepairRecord $repairRecord) : bool
    {
        return $this->promiseAction($repairRecord);
    }

    protected function promiseAction(RepairRecord $repairRecord) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $repairRecord,
            array(
                'commitmentBook'
            )
        );

        $this->patch(
            $this->getResource().'/'.$repairRecord->getId().'/promise',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($repairRecord);
            return true;
        }

        return false;
    }

    public function submit(RepairRecord $repairRecord) : bool
    {
        return $this->submitAction($repairRecord);
    }

    protected function submitAction(RepairRecord $repairRecord) : bool
    {
        $this->patch(
            $this->getResource().'/'.$repairRecord->getId().'/submit'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($repairRecord);
            return true;
        }

        return false;
    }

    public function determine(RepairRecord $repairRecord) : bool
    {
        return $this->determineAction($repairRecord);
    }

    protected function determineAction(RepairRecord $repairRecord) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $repairRecord,
            array(
                'severity'
            )
        );

        $this->patch(
            $this->getResource().'/'.$repairRecord->getId().'/determine',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($repairRecord);
            return true;
        }

        return false;
    }

    public function download(RepairRecord $repairRecord) : bool
    {
        return $this->downloadAction($repairRecord);
    }

    protected function downloadAction(RepairRecord $repairRecord) : bool
    {
        $this->patch(
            $this->getResource().'/'.$repairRecord->getId().'/download'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($repairRecord);
            return true;
        }

        return false;
    }

    public function deliver(RepairRecord $repairRecord) : bool
    {
        return $this->deliverAction($repairRecord);
    }

    protected function deliverAction(RepairRecord $repairRecord) : bool
    {
        $this->patch(
            $this->getResource().'/'.$repairRecord->getId().'/deliver'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($repairRecord);
            return true;
        }

        return false;
    }

    public function training(RepairRecord $repairRecord) : bool
    {
        return $this->trainingAction($repairRecord);
    }

    protected function trainingAction(RepairRecord $repairRecord) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $repairRecord,
            array(
                'initiator',
                'organizationName',
                'location',
                'endTime',
                'trainingClass',
                'result',
                'voucherMaterial'
            )
        );

        $this->patch(
            $this->getResource().'/'.$repairRecord->getId().'/training/uploadSelf',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($repairRecord);
            return true;
        }

        return false;
    }

    public function uploadMaterial(RepairRecord $repairRecord) : bool
    {
        return $this->uploadMaterialAction($repairRecord);
    }

    protected function uploadMaterialAction(RepairRecord $repairRecord) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $repairRecord,
            array(
                'trainingRecord',
                'trainingProof',
                'examReport'
            )
        );

        $this->patch(
            $this->getResource().'/'.$repairRecord->getId().'/training/uploadAuto',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($repairRecord);
            return true;
        }

        return false;
    }

    public function report(RepairRecord $repairRecord) : bool
    {
        return $this->reportAction($repairRecord);
    }

    protected function reportAction(RepairRecord $repairRecord) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $repairRecord,
            array(
                'reportOrganizationName',
                'reportName',
                'issueTime',
                'reportEndTime',
                'reportNumber',
                'grade',
                'electronicReport'
            )
        );

        $this->patch(
            $this->getResource().'/'.$repairRecord->getId().'/report/uploadSelf',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($repairRecord);
            return true;
        }

        return false;
    }

    public function uploadReport(RepairRecord $repairRecord) : bool
    {
        return $this->uploadReportAction($repairRecord);
    }

    protected function uploadReportAction(RepairRecord $repairRecord) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $repairRecord,
            array(
                'reportRecord',
            )
        );

        $this->patch(
            $this->getResource().'/'.$repairRecord->getId().'/report/uploadAuto',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($repairRecord);
            return true;
        }

        return false;
    }
}
