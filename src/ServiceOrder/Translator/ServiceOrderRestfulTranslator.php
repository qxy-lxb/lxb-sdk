<?php
namespace Sdk\ServiceOrder\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;

use Sdk\ServiceOrder\Model\ServiceOrder;
use Sdk\ServiceOrder\Model\NullServiceOrder;
use Sdk\Organization\Repository\OrganizationRepository;
use Sdk\Course\Repository\CourseRepository;
use Sdk\MemberAccount\Translator\MemberAccountRestfulTranslator;
use Sdk\Service\Translator\ServiceRestfulTranslator;
use Sdk\Organization\Translator\OrganizationRestfulTranslator;
use Sdk\Service\Model\ServiceContent;
use Sdk\TradeRecord\Model\ITradeAble;

class ServiceOrderRestfulTranslator implements IRestfulTranslator, ITradeAble
{
    use RestfulTranslatorTrait;

    protected function getEnterpriseRestfulTranslator() : EnterpriseRestfulTranslator
    {
        return new EnterpriseRestfulTranslator();
    }

    public function getMemberAccountRestfulTranslator()
    {
        return new MemberAccountRestfulTranslator();
    }

    public function arrayToObject(array $expression, $serviceOrder = null)
    {
        return $this->translateToObject($expression, $serviceOrder);
    }

    protected function getServiceRestfulTranslator() : ServiceRestfulTranslator
    {
        return new ServiceRestfulTranslator();
    }

    protected function getOrganizationRestfulTranslator() : OrganizationRestfulTranslator
    {
        return new OrganizationRestfulTranslator();
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    private function translateToObject(array $expression, $serviceOrder = null)
    {
        
        if (empty($expression)) {
            return NullServiceOrder::getInstance();
        }

        if ($serviceOrder == null) {
            $serviceOrder = new ServiceOrder();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $serviceOrder->setId($id);

        if (isset($data['attributes'])) {
            $attributes = $data['attributes'];
            
            if (isset($attributes['number'])) {
                $serviceOrder->setNumber($attributes['number']);
            }
            if (isset($attributes['serviceName'])) {
                $serviceOrder->setServiceName($attributes['serviceName']);
            }
            if (isset($attributes['price'])) {
                $serviceOrder->setPrice($attributes['price']);
            }
            if (isset($attributes['institutionalPrice'])) {
                $serviceOrder->setInstitutionalPrice($attributes['institutionalPrice']);
            }
            if (isset($attributes['platformPrice'])) {
                $serviceOrder->setPlatformPrice($attributes['platformPrice']);
            }
            if (isset($attributes['status'])) {
                $serviceOrder->setStatus($attributes['status']);
            }
            if (isset($attributes['snapshot'])) {
                $serviceOrder->setSnapshot($attributes['snapshot']);
            }
            if (isset($attributes['createTime'])) {
                $serviceOrder->setCreateTime($attributes['createTime']);
            }
            if (isset($attributes['updateTime'])) {
                $serviceOrder->setUpdateTime($attributes['updateTime']);
            }
            if (isset($attributes['statusTime'])) {
                $serviceOrder->setStatusTime($attributes['statusTime']);
            }
            if (isset($attributes['organizationName'])) {
                $serviceOrder->getOrganization()->setName($attributes['organizationName']);
            }
            if (isset($attributes['thumbnail'])) {
                $serviceOrder->getService()->setThumbnail($attributes['thumbnail']);
            }
            if (isset($attributes['userName'])) {
                $serviceOrder->setUserName($attributes['userName']);
            }
            if (isset($attributes['transactionPlatform'])) {
                $serviceOrder->setTransactionPlatform($attributes['transactionPlatform']);
            }
            if (isset($attributes['transactionNumber'])) {
                $serviceOrder->setTransactionNumber($attributes['transactionNumber']);
            }
            if (isset($attributes['orderStatus'])) {
                $serviceOrder->setOrderStatus($attributes['orderStatus']);
            }
            $serviceContent = isset($attributes['serviceContent']['creditRating'])
            ? $attributes['serviceContent']['creditRating']
            : '';
            $thumbnail = isset($attributes['serviceContent']['thumbnail'])
            ? $attributes['serviceContent']['thumbnail']
            : '';
            $electronicReport = isset($attributes['serviceContent']['electronicReport'])
            ? $attributes['serviceContent']['electronicReport']
            : '';
            $serviceOrder->setServiceContent(
                new ServiceContent(
                    $serviceContent,
                    $thumbnail,
                    $electronicReport
                )
            );
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        if (isset($relationships['memberAccount']['data'])) {
            if (isset($expression['included'])) {
                $memberAccount = $this->changeArrayFormat(
                    $relationships['memberAccount']['data'],
                    $expression['included']
                );
            }

            if (!isset($expression['included'])) {
                $memberAccount = $this->changeArrayFormat($relationships['memberAccount']['data']);
            }
            $serviceOrder->setMemberAccount(
                $this->getMemberAccountRestfulTranslator()->arrayToObject($memberAccount)
            );
        }
        if (isset($relationships['enterprise']['data'])) {
            $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);
            $serviceOrder->setEnterprise($this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise));
        }
        if (isset($relationships['service']['data'])) {
            $service = $this->changeArrayFormat($relationships['service']['data']);
            $serviceOrder->setService($this->getServiceRestfulTranslator()->arrayToObject($service));
        }
        if (isset($relationships['organization']['data'])) {
            $organization = $this->changeArrayFormat($relationships['organization']['data']);
            $serviceOrder->setOrganization($this->getOrganizationRestfulTranslator()->arrayToObject($organization));
        }
        return $serviceOrder;
    }

    public function objectToArray($serviceOrder, array $keys = array())
    {
        if (!$serviceOrder instanceof ServiceOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'price',
                'enterprise',
                'service',
                'member',
                'repairRecord',
                'transactionPlatform',
                'transactionNumber',
                'memberAccount' => []
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'serviceOrders'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $serviceOrder->getId();
        }
        if (in_array('price', $keys)) {
            $attributes['price'] = $serviceOrder->getPrice();
        }
        if (in_array('repairRecord', $keys)) {
            $attributes['repairRecordId'] = $serviceOrder->getRepairRecord();
        }
        if (in_array('transactionPlatform', $keys)) {
            $attributes['transactionPlatform'] = $serviceOrder->getTransactionPlatform();
        }
        if (in_array('transactionNumber', $keys)) {
            $attributes['transactionNumber'] = $serviceOrder->getTransactionNumber();
        }

        $expression['data']['attributes'] = $attributes;
        if (in_array('enterprise', $keys)) {
            $expression['data']['relationships']['enterprise']['data'] = array(
                array(
                    'type' => 'enterprise',
                    'id' => $serviceOrder->getEnterprise()->getId()
                )
            );
        }
        if (in_array('service', $keys)) {
            $expression['data']['relationships']['service']['data'] = array(
                array(
                    'type' => 'service',
                    'id' => $serviceOrder->getService()->getId()
                )
            );
        }

        if (in_array('memberAccount', $keys)) {
            $expression['data']['relationships']['memberAccount']['data'] = array(
                array(
                    'type' => 'memberAccounts',
                    'id' => Core::$container->get('user')->getId()
                )
             );
        }

        return $expression;
    }
}
