<?php
namespace Sdk\ServiceOrder\Adapter\ServiceOrder;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\ServiceOrder\Model\ServiceOrder;
use Sdk\Common\Adapter\IDeleteAbleAdapter;

interface IServiceOrderAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter, IDeleteAbleAdapter
{
    public function pay(ServiceOrder $serviceOrder);
}
