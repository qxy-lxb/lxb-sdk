<?php
namespace Sdk\ServiceOrder\Adapter\ServiceOrder;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Marmot\Core;

use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;

use Sdk\ServiceOrder\Translator\ServiceOrderRestfulTranslator;
use Sdk\ServiceOrder\Model\NullServiceOrder;
use Sdk\ServiceOrder\Model\ServiceOrder;
use Sdk\Common\Adapter\DeleteAbleRestfulAdapterTrait;

class ServiceOrderRestfulAdapter extends GuzzleAdapter implements IServiceOrderAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,OperatAbleRestfulAdapterTrait, FetchAbleRestfulAdapterTrait, DeleteAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'SERVICEORDER_LIST'=>[
            'fields'=>[],
            'include'=>'memberAccount,memberAccount.member,service,enterprise,organization'
        ],
        'SERVICEORDER_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'memberAccount,memberAccount.member,service,enterprise,organization'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new ServiceOrderRestfulTranslator();
        $this->resource = 'serviceOrders';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullServiceOrder());
    }

    public function addAction(ServiceOrder $serviceOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $serviceOrder,
            array('transactionPlatform', 'price', 'enterprise', 'service', 'memberAccount', 'repairRecord')
        );

        $this->post($this->getResource(), $data);

        if ($this->isSuccess()) {
            $this->translateToObject($serviceOrder);
            return true;
        }

        return false;
    }

    public function editAction(ServiceOrder $serviceOrder) : bool
    {
        unset($serviceOrder);

        return false;
    }

    public function pay(ServiceOrder $serviceOrder) : bool
    {
        return $this->payAction($serviceOrder);
    }

    protected function payAction(ServiceOrder $serviceOrder) : bool
    {
        $this->patch(
            $this->getResource().'/'.$serviceOrder->getId().'/pay'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($serviceOrder);
            return true;
        }

        return false;
    }
}
