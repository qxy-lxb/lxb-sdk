<?php
namespace Sdk\ServiceOrder\Model;

use Sdk\Order\Model\Order;
use Sdk\Service\Model\Service;
use Sdk\ServiceOrder\Repository\ServiceOrderRepository;
use Sdk\Service\Model\ServiceContent;
use Sdk\Common\Adapter\IDeleteAbleAdapter;
 
use Sdk\MemberAccount\Model\MemberAccount;


class ServiceOrder extends Order
{
    private $service;

    private $serviceOrderRepository;

    private $serviceContent;
     
    private $memberAccount;

    private $serviceName;

    private $transactionNumber;

    private $userName;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->service = new Service();
        $this->serviceContent = new ServiceContent();
        $this->serviceOrderRepository = new ServiceOrderRepository;
        $this->memberAccount = new MemberAccount();
        $this->serviceName = "";
        $this->transactionNumber = '';
        $this->userName = '';

    }

    public function __destruct()
    {
        parent::__destruct();

        unset($this->service);
        unset($this->serviceContent);
        unset($this->serviceOrderRepository);
        unset($this->memberAccount);
        unset($this->serviceName);
        unset($this->transactionNumber);
        unset($this->userName);
    }

    public function setServiceContent(ServiceContent $serviceContent) : void
    {
        $this->serviceContent = $serviceContent;
    }

    public function getServiceContent() : ServiceContent
    {
        return $this->serviceContent;
    }

    protected function getServiceOrderRepository() : ServiceOrderRepository
    {
        return $this->serviceOrderRepository;
    }

    public function getService() : Service
    {
        return $this->service;
    }

    public function setService(Service $service) : void
    {
        $this->service = $service;
    }

    public function addServiceOrder()
    {
        $repository = $this->getServiceOrderRepository();

        return $repository->add($this);
    }

    public function serviceOrderPay() : bool
    {
        return $this->getServiceOrderRepository()->pay($this);
    }

    protected function getIDeleteAbleAdapter() : IDeleteAbleAdapter
    {
        return $this->getServiceOrderRepository();
    }

    public function getServiceName() : string
    {
        return $this->serviceName;
    }

    public function setServiceName(string $serviceName) : void
    {
        $this->serviceName = $serviceName;
    }

    public function getMemberAccount() : MemberAccount
    {
        return $this->memberAccount;
    }

    public function setMemberAccount(MemberAccount $memberAccount) : void
    {
        $this->memberAccount = $memberAccount;
    }

    public function setTransactionNumber(string $transactionNumber) : void
    {
        $this->TransactionNumber = $transactionNumber;
    }

    public function getTransactionNumber() : string
    {
        return $this->TransactionNumber;
    }

     public function setUserName(string $userName) : void
    {
        $this->userName = $userName;
    }

    public function getUserName() : string
    {
        return $this->userName;
    }
}
