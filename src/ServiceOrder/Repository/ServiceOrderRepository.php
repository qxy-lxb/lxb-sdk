<?php
namespace Sdk\ServiceOrder\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\DeleteAbleRepositoryTrait;

use Sdk\ServiceOrder\Adapter\ServiceOrder\ServiceOrderRestfulAdapter;
use Sdk\ServiceOrder\Model\ServiceOrder;
use Sdk\ServiceOrder\Adapter\ServiceOrder\IServiceOrderAdapter;
use Marmot\Core;

use Marmot\Framework\Classes\Repository;

class ServiceOrderRepository extends Repository implements IServiceOrderAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, OperatAbleRepositoryTrait,
    DeleteAbleRepositoryTrait;

    const LIST_MODEL_UN = 'SERVICEORDER_LIST';
    const FETCH_ONE_MODEL_UN = 'SERVICEORDER_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new ServiceOrderRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : IServiceOrderAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IServiceOrderAdapter
    {
        return new ServiceOrderMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function pay(ServiceOrder $serviceOrder) : bool
    {
        return $this->getAdapter()->pay($serviceOrder);
    }
}
