<?php
namespace Sdk\Message\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Message\Model\PersonalMessage;
use Sdk\Message\Model\NullPersonalMessage;

class PersonalMessageRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $personalMessage = null)
    {
        return $this->translateToObject($expression, $personalMessage);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    private function translateToObject(array $expression, $personalMessage = null)
    {
        if (empty($expression)) {
            return new NullPersonalMessage();
        }

        if ($personalMessage == null) {
            $personalMessage = new PersonalMessage();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $personalMessage->setId($id);

        if (isset($data['attributes'])) {
            $attributes = $data['attributes'];

            if (isset($attributes['title'])) {
                $personalMessage->setTitle($attributes['title']);
            }
            if (isset($attributes['category'])) {
                $personalMessage->setCategory($attributes['category']);
            }
            if (isset($attributes['source'])) {
                $personalMessage->setSource($attributes['source']);
            }
            if (isset($attributes['status'])) {
                $personalMessage->setStatus($attributes['status']);
            }
            if (isset($attributes['createTime'])) {
                $personalMessage->setCreateTime($attributes['createTime']);
            }
            if (isset($attributes['updateTime'])) {
                $personalMessage->setUpdateTime($attributes['updateTime']);
            }
            if (isset($attributes['statusTime'])) {
                $personalMessage->setStatusTime($attributes['statusTime']);
            }
        }
        
        return $personalMessage;
    }

    public function objectToArray($personalMessage, array $keys = array())
    {
        if (!$personalMessage instanceof PersonalMessage) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'status',
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'personalMessages'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $personalMessage->getId();
        }

        $attributes = array();
        if (in_array('status', $keys)) {
            $attributes['status'] = $personalMessage->getStatus();
        }

        return $expression;
    }
}
