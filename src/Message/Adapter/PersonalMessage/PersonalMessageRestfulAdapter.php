<?php
namespace Sdk\Message\Adapter\PersonalMessage;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Marmot\Core;

use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;

use Sdk\Message\Translator\PersonalMessageRestfulTranslator;
use Sdk\Message\Model\NullPersonalMessage;
use Sdk\Message\Model\PersonalMessage;
use Sdk\Common\Adapter\CommonMapErrorsTrait;

class PersonalMessageRestfulAdapter extends GuzzleAdapter implements IPersonalMessageAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'PERSONAL_MESSAGE_LIST'=>[
            'fields'=>[],
            'include'=>''
        ],
        'PERSONAL_MESSAGE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new PersonalMessageRestfulTranslator();
        $this->resource = 'personalMessages';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            112 => RESOURCE_STATUS_READ,
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullPersonalMessage());
    }

    public function read(PersonalMessage $personalMessage) : bool
    {
        return $this->readAction($personalMessage);
    }

    protected function readAction(PersonalMessage $personalMessage) : bool
    {
        $this->patch(
            $this->getResource().'/'.$personalMessage->getId().'/read'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($personalMessage);
            return true;
        }

        return false;
    }
}
