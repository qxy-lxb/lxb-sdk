<?php
namespace Sdk\Message\Adapter\PersonalMessage;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Message\Model\PersonalMessage;
use Sdk\Common\Adapter\IFetchAbleAdapter;

interface IPersonalMessageAdapter extends IFetchAbleAdapter
{
    public function read(PersonalMessage $personalMessage);
}
