<?php
namespace Sdk\Message\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Member\Model\Member;

abstract class Message implements IObject
{
    use Object;
    /**
     * @var int $id
     */
    protected $id;
    /**
     * @var string $title 标题
     */
    protected $title;
    /**
     * @var IReceiveAble $target 目标
     */
    protected $target;
    /**
     * @var array $source 来源
     */
    protected $source;
    /**
     * @var int $category 来源分类
     */
    protected $category;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->title = '';
        $this->target = new Member();
        $this->source = array();
        $this->category = 0;
        $this->updateTime = Core::$container->get('time');
        $this->createTime = Core::$container->get('time');
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->target);
        unset($this->source);
        unset($this->category);
        unset($this->updateTime);
        unset($this->createTime);
        unset($this->statusTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setTarget(IReceiveAble $target) : void
    {
        $this->target = $target;
    }

    public function getTarget() : IReceiveAble
    {
        return $this->target;
    }

    public function setSource(array $source) : void
    {
        $this->source = $source;
    }

    public function getSource() : array
    {
        return $this->source;
    }

    public function setCategory(int $category) : void
    {
        $this->category = $category;
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function setStatus(int $status) : void
    {
        $this->status= $status;
    }

    abstract public function read() : bool;
}
