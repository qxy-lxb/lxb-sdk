<?php
namespace Sdk\Message\Model;

use Marmot\Core;

use Sdk\Message\Repository\PersonalMessageRepository;

class PersonalMessage extends Message
{
    private $repository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->status = 0;
        $this->repository = new PersonalMessageRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->status);
        unset($this->repository);
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository() : PersonalMessageRepository
    {
        return $this->repository;
    }

    public function read() : bool
    {
        return $this->getRepository()->read($this);
    }
}
