<?php
namespace Sdk\Message\Model;

use Marmot\Interfaces\INull;
use Marmot\Core;

class NullPersonalMessage extends PersonalMessage implements INull
{
    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function read() : bool
    {
        return $this->resourceNotExist();
    }
}
