<?php
namespace Sdk\Message\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;

use Sdk\Message\Adapter\PersonalMessage\PersonalMessageRestfulAdapter;
use Sdk\Message\Model\PersonalMessage;
use Sdk\Message\Adapter\PersonalMessage\IPersonalMessageAdapter;
use Marmot\Core;

use Marmot\Framework\Classes\Repository;

class PersonalMessageRepository extends Repository implements IPersonalMessageAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait;

    const LIST_MODEL_UN = 'PERSONAL_MESSAGE_LIST';
    const FETCH_ONE_MODEL_UN = 'PERSONAL_MESSAGE_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new PersonalMessageRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : IPersonalMessageAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IPersonalMessageAdapter
    {
        return new PersonalMessageMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function read(PersonalMessage $personalMessage) : bool
    {
        return $this->getAdapter()->read($personalMessage);
    }
}
