<?php
namespace Sdk\MemberAccount\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Sdk\Member\Model\Member;

use Sdk\MemberAccount\Repository\MemberAccountRepository;

class MemberAccount implements IObject
{
    use Object;

    const STATUS = array(
        'ENABLED' => 0 ,
        'DISABLED' => -2
    );

    /**
     * [$id 主键Id]
     * @var [int]
     */
    private $id;
    /**
     * [$appleAccountBalance APP账户余额]
     * @var [int]
     */
    private $appleAccountBalance;

    /**
     * [$androidAccountBalance android账户余额]
     * @var [int]
     */
    private $androidAccountBalance;

    private $member;
    /**
     * [$repository]
     * @var [Object]
     */
    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->appleAccountBalance = 0;
        $this->androidAccountBalance = 0;
        $this->member = new Member();
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = self::STATUS['ENABLED'];
        $this->statusTime = 0;
        $this->repository = new MemberAccountRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->appleAccountBalance);
        unset($this->androidAccountBalance);
        unset($this->member);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setAppleAccountBalance(int $appleAccountBalance) : void
    {
        $this->appleAccountBalance = is_numeric($appleAccountBalance) ? $appleAccountBalance : 0;
    }

    public function getAppleAccountBalance() : int
    {
        return $this->appleAccountBalance;
    }

    public function setAndroidAccountBalance(int $androidAccountBalance) : void
    {
        $this->androidAccountBalance = is_numeric($androidAccountBalance) ? $androidAccountBalance : 0;
    }

    public function getAndroidAccountBalance() : int
    {
        return $this->androidAccountBalance;
    }

    public function setStatus(int $status)
    {
        $this->status = in_array($status, array_values(self::STATUS)) ? $status : self::STATUS['DISABLED'];
    }

    public function setMember(Member $member): void
    {
        $this->member = $member;
    }

    public function getMember(): Member
    {
        return $this->member;
    }

    protected function getRepository() : MemberAccountRepository
    {
        return $this->repository;
    }
}