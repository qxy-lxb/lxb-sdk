<?php
namespace Sdk\Organization\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\EnableAbleRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\TopAbleRepositoryTrait;

use Sdk\Organization\Adapter\Organization\IOrganizationAdapter;
use Sdk\Organization\Adapter\Organization\OrganizationRestfulAdapter;
use Sdk\Organization\Adapter\Organization\NewMockAdapter;
use Sdk\Organization\Model\Organization;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class OrganizationRepository extends Repository implements IOrganizationAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, EnableAbleRepositoryTrait, OperatAbleRepositoryTrait, TopAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'ORGANIZATION_LIST';
    const FETCH_ONE_MODEL_UN = 'ORGANIZATION_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new OrganizationRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : IOrganizationAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IOrganizationAdapter
    {
        return new NewMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
