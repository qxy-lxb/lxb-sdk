<?php
namespace Sdk\Organization\Translator;

use Sdk\Common\Translator\RestfulTranslatorTrait;
use Marmot\Core;
use Sdk\Crew\Translator\CrewRestfulTranslator;

use Sdk\Organization\Model\Organization;
use Sdk\Organization\Model\NullOrganization;

use Marmot\Interfaces\IRestfulTranslator;

class OrganizationRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $organization = null)
    {
        return $this->translateToObject($expression, $organization);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $organization = null)
    {
        if (empty($expression)) {
            return new NullOrganization();
        }

        if ($organization == null) {
            $organization = new Organization();
        }

        $data =  $expression['data'];

        $id = $data['id'];
        $organization->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $organization->setName($attributes['name']);
        }
        if (isset($attributes['logo'])) {
            $organization->setLogo($attributes['logo']);
        }
        if (isset($attributes['contacts'])) {
            $organization->setContacts($attributes['contacts']);
        }
        if (isset($attributes['phone'])) {
            $organization->setPhone($attributes['phone']);
        }
        if (isset($attributes['url'])) {
            $organization->setUrl($attributes['url']);
        }
        if (isset($attributes['settledDate'])) {
            $organization->setSettledDate($attributes['settledDate']);
        }
        if (isset($attributes['endDate'])) {
            $organization->setEndDate($attributes['endDate']);
        }
        if (isset($attributes['contract'])) {
            $organization->setContract($attributes['contract']);
        }
        if (isset($attributes['category'])) {
            $organization->setCategory($attributes['category']);
        }
        if (isset($attributes['trainType'])) {
            $organization->setType($attributes['trainType']);
        }
        if (isset($attributes['statusTime'])) {
            $organization->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $organization->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $organization->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $organization->setStatus($attributes['status']);
        }

        return $organization;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($organization, array $keys = array())
    {
        if (!$organization instanceof Organization) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'logo',
                'contacts',
                'phone',
                'url',
                'settledDate',
                'endDate',
                'contract',
                'category',
                'type'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'organizations'
            )
        );
        
        if (in_array('id', $keys)) {
            $expression['data']['id'] = $organization->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $organization->getName();
        }
        if (in_array('logo', $keys)) {
            $attributes['logo'] = $organization->getLogo();
        }
        if (in_array('contacts', $keys)) {
            $attributes['contacts'] = $organization->getContacts();
        }
        if (in_array('phone', $keys)) {
            $attributes['phone'] = $organization->getPhone();
        }
        if (in_array('url', $keys)) {
            $attributes['url'] = $organization->getUrl();
        }
        if (in_array('settledDate', $keys)) {
            $attributes['settledDate'] = $organization->getSettledDate();
        }
        if (in_array('endDate', $keys)) {
            $attributes['endDate'] = $organization->getEndDate();
        }
        if (in_array('contract', $keys)) {
            $attributes['contract'] = $organization->getContract();
        }
        if (in_array('category', $keys)) {
            $attributes['category'] = $organization->getCategory();
        }
        if (in_array('type', $keys)) {
            $attributes['type'] = $organization->getType();
        }

        $expression['data']['attributes'] = $attributes;

        return $expression;
    }
}
