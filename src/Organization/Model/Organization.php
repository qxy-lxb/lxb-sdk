<?php
namespace Sdk\Organization\Model;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Adapter\IEnableAbleAdapter;
use Sdk\Common\Adapter\ITopAbleAdapter;
use Sdk\Common\Model\IEnableAble;
use Sdk\Common\Model\EnableAbleTrait;

use Sdk\Crew\Model\Crew;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Marmot\Core;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Sdk\Organization\Repository\OrganizationRepository;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Organization implements IEnableAble, IOperatAble, IObject
{
    use EnableAbleTrait, OperatAbleTrait, Object;

    /**
     * @var CATEGORY['REPORTING_SERVICE_INSTITUTIONS']  信用报告服务机构 1
     * @var CATEGORY['RESTORATION_TRAINING_INSTITUTIONS']  信用修复培训机构 2
     */
    const CATEGORY = array(
        'DEFAULT' => 0,
        'ADMINISTRATION' => 1,
        'REPAIR' => 2,
        'PLATFORM' => 3,
        'PRESENTATION' => 4,
        'OTHER' => 5
    );
    /**
     * @var CATEGORY['ONLINE_TRAINING_PLATFORM']  信用建设牵头部门与行政处罚机关名称，信用中国网站公益性在线培训平台 1
     * @var CATEGORY['INTEGRATED_CREDIT_SERVICE_AGENCY']  综合信用服务机构试点单位 2
     * @var CATEGORY['CREDIT_REFERENCE_AGENCY']  征信机构 3
     */
    const TRAIN_TYPE = array(
        'DEFAULT' => 0,
        'ONLINE_TRAINING_PLATFORM' => 1,
        'INTEGRATED_CREDIT_SERVICE_AGENCY' => 2,
        'CREDIT_REFERENCE_AGENCY' => 3,
    );

    private $id;
    
    private $name;
    
    private $contacts;
    
    private $phone;
    
    private $url;
    
    private $settledDate;
    
    private $endDate;
    
    private $logo;
    
    private $contract;
    
    private $category;

    private $type;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->contacts = '';
        $this->phone = '';
        $this->url = '';
        $this->settledDate = '0000-00-00';
        $this->endDate = '0000-00-00';
        $this->logo = array();
        $this->contract = array();
        $this->category = 0;
        $this->type = 0;
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->repository = new OrganizationRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->contacts);
        unset($this->phone);
        unset($this->url);
        unset($this->settledDate);
        unset($this->endDate);
        unset($this->logo);
        unset($this->contract);
        unset($this->category);
        unset($this->type);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setContacts(string $contacts) : void
    {
        $this->contacts = $contacts;
    }

    public function getContacts() : string
    {
        return $this->contacts;
    }

    public function setPhone(string $phone) : void
    {
        $this->phone = $phone;
    }

    public function getPhone() : string
    {
        return $this->phone;
    }

    public function setUrl(string $url) : void
    {
        $this->url = $url;
    }

    public function getUrl() : string
    {
        return $this->url;
    }

    public function setSettledDate(string $settledDate) : void
    {
        $this->settledDate = $settledDate;
    }

    public function getSettledDate() : string
    {
        return $this->settledDate;
    }

    public function setEndDate(string $endDate) : void
    {
        $this->endDate = $endDate;
    }

    public function getEndDate() : string
    {
        return $this->endDate;
    }

    public function setLogo(array $logo) : void
    {
        $this->logo = $logo;
    }

    public function getLogo() : array
    {
        return $this->logo;
    }

    public function setContract(array $contract) : void
    {
        $this->contract = $contract;
    }

    public function getContract() : array
    {
        return $this->contract;
    }

    public function setCategory(int $category) : void
    {
        $this->category = in_array(
            $category,
            self::CATEGORY
        ) ? $category : 0;
    }

    public function getType() : int
    {
        return $this->type;
    }

    public function setType(int $type) : void
    {
        $this->type = in_array(
            $type,
            self::TRAIN_TYPE
        ) ? $type : 0;
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    protected function getRepository() : OrganizationRepository
    {
        return $this->repository;
    }

    protected function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }
}
