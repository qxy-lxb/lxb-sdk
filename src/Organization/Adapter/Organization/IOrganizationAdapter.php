<?php
namespace Sdk\Organization\Adapter\Organization;

use Sdk\Common\Adapter\IEnableAbleAdapter;
use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\ITopAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface IOrganizationAdapter extends IEnableAbleAdapter, IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter, ITopAbleAdapter
{
}
