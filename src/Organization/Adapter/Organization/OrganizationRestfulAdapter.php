<?php
namespace Sdk\Organization\Adapter\Organization;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Adapter\TopAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\CommonMapErrorsTrait;

use Sdk\Organization\Translator\OrganizationRestfulTranslator;
use Sdk\Organization\Model\NullOrganization;
use Sdk\Organization\Model\Organization;

use Marmot\Core;

class OrganizationRestfulAdapter extends GuzzleAdapter implements IOrganizationAdapter
{
    use EnableAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        TopAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    private $resource;

    const SCENARIOS = [
        'ORGANIZATION_LIST'=>[
            'fields'=>[]
        ],
        'ORGANIZATION_FETCH_ONE'=>[
            'fields'=>[]
        ]
    ];
    
    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new OrganizationRestfulTranslator();
        $this->resource = 'organizations';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            8001 => ORGANIZATION_NAME_FORMAT_ERROR,
            8002 => ORGANIZATION_CONTACTS_FORMAT_ERROR,
            8003 => ORGANIZATION_PHONE_FORMAT_ERROR,
            8004 => ORGANIZATION_URL_FORMAT_ERROR,
            8005 => SETTLED_DATE_FORMAT_ERROR,
            8006 => END_DATE_FORMAT_ERROR,
            8007 => CONTRACT_FORMAT_ERROR,
            8008 => ORGANIZATION_CATEGORY_FORMAT_ERROR,
            8009 => ORGANIZATION_TRAIN_TYPE_FORMAT_ERROR,
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullOrganization());
    }

    public function addAction(Organization $organization) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $organization,
            array('name', 'logo', 'contacts', 'phone', 'url', 'settledDate', 'endDate', 'contract', 'category', 'type')
        );

        $this->post(
            $this->getResource(),
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($organization);
            return true;
        }

        return false;
    }

    public function editAction(Organization $organization) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $organization,
            array('logo', 'contacts', 'phone', 'url', 'settledDate', 'endDate', 'contract', 'type')
        );

        $this->patch(
            $this->getResource().'/'.$organization->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($organization);
            return true;
        }

        return false;
    }

    public function move(Organization $organization) : bool
    {
        $this->patch(
            $this->getResource().'/'.$organization->getId().'/move/'.$organization->getOrganizationType()->getId()
        );

        if ($this->isSuccess()) {
            $this->translateToObject($organization);
            return true;
        }

        return false;
    }
}
