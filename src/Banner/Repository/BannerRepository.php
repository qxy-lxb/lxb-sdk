<?php
namespace Sdk\Banner\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\EnableAbleRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\TopAbleRepositoryTrait;

use Sdk\Banner\Adapter\Banner\BannerRestfulAdapter;
use Sdk\Banner\Model\Banner;
use Sdk\Banner\Adapter\Banner\IBannerAdapter;
use Marmot\Core;

use Marmot\Framework\Classes\Repository;

class BannerRepository extends Repository implements IBannerAdapter
{
    use FetchRepositoryTrait, EnableAbleRepositoryTrait, AsyncRepositoryTrait, OperatAbleRepositoryTrait, TopAbleRepositoryTrait;

    const LIST_MODEL_UN = 'BANNER_LIST';
    const FETCH_ONE_MODEL_UN = 'BANNER_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new BannerRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : IBannerAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IBannerAdapter
    {
        return new BannerMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
