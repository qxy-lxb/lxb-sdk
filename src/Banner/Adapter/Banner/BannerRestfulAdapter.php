<?php
namespace Sdk\Banner\Adapter\Banner;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Marmot\Core;

use Sdk\Common\Adapter\TopAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;

use Sdk\Banner\Translator\BannerRestfulTranslator;
use Sdk\Banner\Model\NullBanner;
use Sdk\Banner\Model\Banner;

class BannerRestfulAdapter extends GuzzleAdapter implements IBannerAdapter
{
    use EnableAbleRestfulAdapterTrait, AsyncFetchAbleRestfulAdapterTrait,OperatAbleRestfulAdapterTrait, FetchAbleRestfulAdapterTrait, TopAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'BANNER_LIST'=>[
            'fields'=>[
                'news'=>'title'
            ],
            'include'=>'news'
        ],
        'BANNER_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'news'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new BannerRestfulTranslator();
        $this->resource = 'banners';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullBanner());
    }

    public function addAction(Banner $banner) : bool
    {
        $data = $this->getTranslator()->objectToArray($banner, array('image', 'news'));
        
        $this->post($this->getResource(), $data);

        if ($this->isSuccess()) {
            $this->translateToObject($banner);
            return true;
        }

        return false;
    }

    public function editAction(Banner $banner) : bool
    {
        $data = $this->getTranslator()->objectToArray($banner, array('image'));

        $this->patch($this->getResource().'/'.$banner->getId(), $data);

        if ($this->isSuccess()) {
            $this->translateToObject($banner);
            return true;
        }

        return false;
    }
}
