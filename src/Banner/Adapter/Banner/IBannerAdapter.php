<?php
namespace Sdk\Banner\Adapter\Banner;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IEnableAbleAdapter;
use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\ITopAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;

interface IBannerAdapter extends IEnableAbleAdapter, IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter, ITopAbleAdapter
{
}
