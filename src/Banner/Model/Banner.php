<?php
namespace Sdk\Banner\Model;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Common\Model\IEnableAble;
use Sdk\Common\Model\EnableAbleTrait;
use Sdk\Common\Adapter\IEnableAbleAdapter;
use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Model\ITopAble;
use Sdk\Common\Model\TopAbleTrait;
use Sdk\Common\Adapter\ITopAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Sdk\News\Model\News;

use Sdk\Banner\Repository\BannerRepository;

class Banner implements IEnableAble, ITopAble, IOperatAble, IObject
{
    use EnableAbleTrait, TopAbleTrait, OperatAbleTrait, Object;

    private $id;

    private $image;

    private $news;

    private $stick;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->image = array();
        $this->news = new News();
        $this->status = IEnableAble::STATUS['DISABLED'];
        $this->stick = ITopAble::STICK['DISABLED'];
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->repository = new BannerRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->image);
        unset($this->news);
        unset($this->status);
        unset($this->stick);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setImage(array $image) : void
    {
        $this->image = $image;
    }

    public function getImage() : array
    {
        return $this->image;
    }

    public function setNews(News $news) : void
    {
        $this->news = $news;
    }

    public function getNews() : News
    {
        return $this->news;
    }

    protected function getRepository() : BannerRepository
    {
        return $this->repository;
    }
    
    protected function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getITopAbleAdapter() : ITopAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    public function getColumn(): int
    {
        return IPurviewAble::COLUMN['BANNER'];
    }

    public function isPermit(): bool
    {
        return true;
    }
}
