<?php
namespace Sdk\Banner\Model;

use Marmot\Interfaces\INull;
use Sdk\Common\Model\NullEnableAbleTrait;
use Sdk\Common\Model\NullOperatAbleTrait;

use Marmot\Core;

class NullBanner extends Banner implements INull
{
    use NullEnableAbleTrait, NullOperatAbleTrait;

    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
