<?php
namespace Sdk\Banner\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\News\Translator\NewsRestfulTranslator;
use Sdk\News\Model\CategoryModelFactory;

use Sdk\Banner\Model\Banner;
use Sdk\Banner\Model\NullBanner;
use Sdk\News\Repository\NewsRepository;

class BannerRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getNewsRestfulTranslator() : NewsRestfulTranslator
    {
        return new NewsRestfulTranslator();
    }

    public function arrayToObject(array $expression, $banner = null)
    {
        return $this->translateToObject($expression, $banner);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    private function translateToObject(array $expression, $banner = null)
    {
        if (empty($expression)) {
            return new NullBanner();
        }

        if ($banner == null) {
            $banner = new Banner();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $banner->setId($id);

        if (isset($data['attributes'])) {
            $attributes = $data['attributes'];

            if (isset($attributes['image'])) {
                $banner->setImage($attributes['image']);
            }
            if (isset($attributes['isHomePage'])) {
                $banner->setType($attributes['isHomePage']);
            }
            if (isset($attributes['source'])) {
                $banner->setSource(
                    CategoryModelFactory::create($attributes['source'], TYPE['NEWS_TYPE_CN'])
                );
            }
            if (isset($attributes['status'])) {
                $banner->setStatus($attributes['status']);
            }
            if (isset($attributes['stick'])) {
                $banner->setStick($attributes['stick']);
            }
            if (isset($attributes['createTime'])) {
                $banner->setCreateTime($attributes['createTime']);
            }
            if (isset($attributes['updateTime'])) {
                $banner->setUpdateTime($attributes['updateTime']);
            }
            if (isset($attributes['statusTime'])) {
                $banner->setStatusTime($attributes['statusTime']);
            }
        }

        $relationships = $data['relationships'];

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['news']['data'])) {
            if (isset($expression['included'])) {
                $news = $this->changeArrayFormat($relationships['news']['data'], $expression['included']);
                $banner->setNews($this->getNewsRestfulTranslator()->arrayToObject($news));
            }
            if (!isset($expression['included'])) {
                $news = $this->getNewsRepository()->fetchOne($relationships['news']['data']['id']);
                $banner->setNews($news);
            }
        }
        
        return $banner;
    }

    public function objectToArray($banner, array $keys = array())
    {
        if (!$banner instanceof Banner) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'image',
                'status',
                'news',
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'banners'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $banner->getId();
        }

        $attributes = array();
        if (in_array('image', $keys)) {
            $attributes['image'] = $banner->getImage();
        }
        if (in_array('status', $keys)) {
            $attributes['status'] = $banner->getStatus();
        }
        if (in_array('news', $keys)) {
            $expression['data']['relationships']['news']['data'] = array(
                array(
                    'type' => 'news',
                    'id' => $banner->getNews()->getId()
                )
            );
        }
        $expression['data']['attributes'] = $attributes;

        return $expression;
    }

    protected function getNewsRepository()
    {
        return new NewsRepository();
    }
}
