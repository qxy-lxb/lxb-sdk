<?php
namespace Sdk\NaturalPerson\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\NaturalPerson\Model\NaturalPerson;
use Sdk\NaturalPerson\Model\IdentityInfo;
use Sdk\NaturalPerson\Model\NullNaturalPerson;

use Sdk\Member\Translator\MemberRestfulTranslator;

class NaturalPersonRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getMemberRestfulTranslator()
    {
        return new MemberRestfulTranslator();
    }

    public function arrayToObject(array $expression, $naturalPerson = null)
    {
        return $this->translateToObject($expression, $naturalPerson);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $naturalPerson = null)
    {
        if (empty($expression)) {
            return NullNaturalPerson::getInstance();
        }

        if ($naturalPerson == null) {
            $naturalPerson = new NaturalPerson();
        }

        $data =  $expression['data'];

        $id = $data['id'];
        $naturalPerson->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $realName = isset($attributes['realName'])
        ? $attributes['realName']
        : '';
        $cardId = isset($attributes['cardId'])
        ? $attributes['cardId']
        : '';
        $gender = isset($attributes['gender'])
        ? $attributes['gender']
        : '';
        $birthday = isset($attributes['birthday'])
        ? $attributes['birthday']
        : '';
        $address = isset($attributes['address'])
        ? $attributes['address']
        : '';

        $positivePhoto = isset($attributes['cardFront'])
        ? $attributes['cardFront']
        : array();
        $reversePhoto = isset($attributes['cardReverse'])
        ? $attributes['cardReverse']
        : array();

        $naturalPerson->setIdentityInfo(
            new IdentityInfo(
                $realName,
                $cardId,
                $gender,
                $birthday,
                $address,
                $positivePhoto,
                $reversePhoto
            )
        );

        if (isset($attributes['createTime'])) {
            $naturalPerson->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $naturalPerson->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $naturalPerson->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $naturalPerson->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);
            $naturalPerson->setMember($this->getMemberRestfulTranslator()->arrayToObject($member));
        }

        return $naturalPerson;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($naturalPerson, array $keys = array())
    {
        if (!$naturalPerson instanceof NaturalPerson) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'realName',
                'cardId',
                'gender',
                'birthday',
                'address',
                'positivePhoto',
                'reversePhoto',
                'member'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'naturalPersons'
            )
        );
        
        if (in_array('id', $keys)) {
            $expression['data']['id'] = $naturalPerson->getId();
        }

        $attributes = array();

        if (in_array('realName', $keys)) {
            $attributes['realName'] = $naturalPerson->getIdentityInfo()->getRealName();
        }
        if (in_array('gender', $keys)) {
            $attributes['gender'] = $naturalPerson->getIdentityInfo()->getGender();
        }
        if (in_array('birthday', $keys)) {
            $attributes['birthday'] = $naturalPerson->getIdentityInfo()->getBirthday();
        }
        if (in_array('address', $keys)) {
            $attributes['address'] = $naturalPerson->getIdentityInfo()->getAddress();
        }
        if (in_array('cardId', $keys)) {
            $attributes['cardId'] = $naturalPerson->getIdentityInfo()->getCardId();
        }
        if (in_array('positivePhoto', $keys)) {
            $attributes['cardFront'] = $naturalPerson->getIdentityInfo()->getPositivePhoto();
        }
        if (in_array('reversePhoto', $keys)) {
            $attributes['cardReverse'] = $naturalPerson->getIdentityInfo()->getReversePhoto();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('member', $keys)) {
            $expression['data']['relationships']['member']['data'] = array(
                array(
                    'type' => 'members',
                    'id' => $naturalPerson->getMember()->getId()
                )
             );
        }

        return $expression;
    }
}
