<?php
namespace Sdk\NaturalPerson\Model;

class IdentityInfo
{
    const DEFAULT_BIRTHDAY = '0000-00-00';
    /**
     * [$realName 真实姓名]
     * @var [string]
     */
    private $realName;
    /**
     * [$cardId 身份证号]
     * @var [string]
     */
    private $cardId;
    /**
     * [$gender 性别]
     * @var [int]
     */
    protected $gender;
    /**
     * [$birthday 出生日期]
     * @var [string]
     */
    private $birthday;
    /**
     * [$address 详细地址]
     * @var [string]
     */
    private $address;
    /**
     * [$positivePhoto 正面身份证]
     * @var [array]
    */
    private $positivePhoto;
    /**
     * [$positivePhoto 反面身份证]
     * @var [array]
    */
    private $reversePhoto;

    public function __construct(
        string $realName = '',
        string $cardId = '',
        string $gender = '',
        string $birthday = self::DEFAULT_BIRTHDAY,
        string $address = '',
        array $positivePhoto = array(),
        array $reversePhoto = array()
    ) {
        $this->realName = $realName;
        $this->cardId = $cardId;
        $this->gender = $gender;
        $this->birthday = $birthday;
        $this->address = $address;
        $this->positivePhoto = $positivePhoto;
        $this->reversePhoto = $reversePhoto;
    }

    public function __destruct()
    {
        unset($this->realName);
        unset($this->cardId);
        unset($this->gender);
        unset($this->birthday);
        unset($this->address);
        unset($this->positivePhoto);
        unset($this->reversePhoto);
    }

    public function getRealName() : string
    {
        return $this->realName;
    }
    
    public function getCardId() : string
    {
        return $this->cardId;
    }

    public function getGender() : string
    {
        return $this->gender;
    }

    public function getBirthday() : string
    {
        return $this->birthday;
    }

    public function getAddress() : string
    {
        return $this->address;
    }

    public function getPositivePhoto() : array
    {
        return $this->positivePhoto;
    }

    public function getReversePhoto() : array
    {
        return $this->reversePhoto;
    }
}
