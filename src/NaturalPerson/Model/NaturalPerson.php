<?php
namespace Sdk\NaturalPerson\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Member\Model\Member;
use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\NaturalPerson\Repository\NaturalPersonRepository;

class NaturalPerson implements IOperatAble, IObject
{
    use OperatAbleTrait,Object;
    /**
     * [$id 主键Id]
     * @var [int]
     */
    private $id;
    /**
     * [$member APP用户对象]
     * @var [object]
     */
    private $member;
    /**
     * [$identityInfo 身份证信息]
     * @var [object]
     */
    private $identityInfo;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->member = Core::$container->has('user') ? Core::$container->get('user') : new Member();
        $this->identityInfo = new IdentityInfo();
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = 0;
        $this->repository = new NaturalPersonRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->member);
        unset($this->identityInfo);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setIdentityInfo(IdentityInfo $identityInfo) : void
    {
        $this->identityInfo = $identityInfo;
    }

    public function getIdentityInfo() : IdentityInfo
    {
        return $this->identityInfo;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository() : NaturalPersonRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }
}
