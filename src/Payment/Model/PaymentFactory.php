<?php
namespace Sdk\Payment\Model;

use Sdk\Common\Repository\NullRepository;

class PaymentFactory
{
    const TYPE = array(
        'COURSE' => 1,
        'SERVICE' => 2
    );
    const MAPS = array(
        self::TYPE['COURSE']=>'\Sdk\Order\Repository\OrderRepository',
        self::TYPE['SERVICE']=>'\Sdk\ServiceOrder\Repository\ServiceOrderRepository',
    );
    
    public function getModel(string $paymentId)
    {
        $temp = explode('_', $paymentId);
        $type = $temp[0];
        $id = $temp[1];
        
        $repository = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';
        $repository = class_exists($repository) ? new $repository : NullRepository::getInstance();

        return $repository->scenario($repository::FETCH_ONE_MODEL_UN)->fetchOne($id);
    }
}
