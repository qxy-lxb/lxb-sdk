<?php
namespace Sdk\Deposit\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Deposit\Model\Deposit;
use Sdk\Deposit\Model\NullDeposit;

use Sdk\Common\Translator\RestfulTranslatorTrait;
use Sdk\MemberAccount\Translator\MemberAccountRestfulTranslator;
use Sdk\TradeRecord\Model\ITradeAble;

class DepositRestfulTranslator implements IRestfulTranslator, ITradeAble
{
    use RestfulTranslatorTrait;

    protected function getMemberAccountRestfulTranslator()
    {
        return new MemberAccountRestfulTranslator();
    }

    public function arrayToObject(array $expression, $deposit = null)
    {
        return $this->translateToObject($expression, $deposit);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $deposit = null)
    {
        
        if (empty($expression)) {
            return new NullDeposit();
        }

        if ($deposit == null) {
            $deposit = new Deposit();
        }

        $data =  $expression['data'];

        $id = $data['id'];
        $deposit->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['goldCoinId'])) {
            $deposit->setGoldCoinId($attributes['goldCoinId']);
        }
        if (isset($attributes['depositName'])) {
            $deposit->setDepositName($attributes['depositName']);
        }
        if (isset($attributes['amount'])) {
            $deposit->setAmount($attributes['amount']);
        }
        if (isset($attributes['transactionPlatform'])) {
            $deposit->setTransactionPlatform($attributes['transactionPlatform']);
        }
        if (isset($attributes['price'])) {
            $deposit->setPrice($attributes['price']);
        }
        if (isset($attributes['number'])) {
            $deposit->setNumber($attributes['number']);
        }
        if (isset($attributes['transactionId'])) {
            $deposit->setTransactionId($attributes['transactionId']);
        }
        if (isset($attributes['paymentId'])) {
            $deposit->setPaymentId($attributes['paymentId']);
        }
        if (isset($attributes['paymentType'])) {
            $deposit->setPaymentType($attributes['paymentType']);
        }
        if (isset($attributes['paymentTime'])) {
            $deposit->setPaymentTime($attributes['paymentTime']);
        }
        if (isset($attributes['transactionNumber'])) {
            $deposit->setTransactionNumber($attributes['transactionNumber']);
        }
        if (isset($attributes['transactionInfo'])) {
            $deposit->setTransactionInfo($attributes['transactionInfo']);
        }
        if (isset($attributes['failureReason'])) {
            $deposit->setFailureReason($attributes['failureReason']);
        }
        if (isset($attributes['status'])) {
            $deposit->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $deposit->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $deposit->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $deposit->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['transactionPlatform'])) {
            $deposit->setTransactionPlatform($attributes['transactionPlatform']);
        }
        if (isset($attributes['price'])) {
            $deposit->setPrice($attributes['price']);
        }
        if (isset($attributes['number'])) {
            $deposit->setNumber($attributes['number']);
        }
        if (isset($attributes['transactionId'])) {
            $deposit->setTransactionId($attributes['transactionId']);
        }
        if (isset($attributes['paymentId'])) {
            $deposit->setPaymentId($attributes['paymentId']);
        }
        if (isset($attributes['paymentType'])) {
            $deposit->setPaymentType($attributes['paymentType']);
        }
        if (isset($attributes['paymentTime'])) {
            $deposit->setPaymentTime($attributes['paymentTime']);
        }
        if (isset($attributes['transactionNumber'])) {
            $deposit->setTransactionNumber($attributes['transactionNumber']);
        }
        if (isset($attributes['transactionInfo'])) {
            $deposit->setTransactionInfo($attributes['transactionInfo']);
        }
        if (isset($attributes['failureReason'])) {
            $deposit->setFailureReason($attributes['failureReason']);
        }
        if (isset($attributes['status'])) {
            $deposit->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $deposit->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $deposit->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['statusTime'])) {
            $deposit->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['memberAccount']['data'])) {
            $memberAccount = $this->changeArrayFormat($relationships['memberAccount']['data']);

            $deposit->setMemberAccount($this->getMemberAccountRestfulTranslator()->arrayToObject($memberAccount));
        }

        return $deposit;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($deposit, array $keys = array())
    {
        if (!$deposit instanceof Deposit) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'goldCoinId',
                'amount',
                'transactionPlatform',
                'price',
                'memberAccountId',
                'paymentType',
                'transactionNumber',
                'transactionInfo',
                'transactionInfo',
                'failureReason',
                'failureReason',
                'transactionId'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'deposits'
            )
        );
        
        if (in_array('id', $keys)) {
            $expression['data']['id'] = $deposit->getId();
        }

        $attributes = array();

        if (in_array('goldCoinId', $keys)) {
            $attributes['goldCoinId'] = $deposit->getGoldCoinId();
        }
        if (in_array('amount', $keys)) {
            $attributes['amount'] = $deposit->getGoldCoinId();
        }
        if (in_array('goldCoinId', $keys)) {
            $attributes['goldCoinId'] = $deposit->getGoldCoinId();
        }
        if (in_array('transactionPlatform', $keys)) {
            $attributes['transactionPlatform'] = $deposit->getTransactionPlatform();
        }
        if (in_array('price', $keys)) {
            $attributes['price'] = $deposit->getPrice();
        }
        if (in_array('paymentType', $keys)) {
            $attributes['paymentType'] = $deposit->getPaymentType();
        }
        if (in_array('transactionNumber', $keys)) {
            $attributes['transactionNumber'] = $deposit->getTransactionNumber();
        }
        if (in_array('transactionInfo', $keys)) {
            $attributes['transactionInfo'] = $deposit->getTransactionInfo();
        }
        if (in_array('transactionPlatform', $keys)) {
            $attributes['transactionPlatform'] = $deposit->getTransactionPlatform();
        }
        if (in_array('price', $keys)) {
            $attributes['price'] = $deposit->getPrice();
        }
        if (in_array('paymentType', $keys)) {
            $attributes['paymentType'] = $deposit->getPaymentType();
        }
        if (in_array('transactionNumber', $keys)) {
            $attributes['transactionNumber'] = $deposit->getTransactionNumber();
        }
        if (in_array('transactionInfo', $keys)) {
            $attributes['transactionInfo'] = $deposit->getTransactionInfo();
        }
        if (in_array('failureReason', $keys)) {
            $attributes['failureReason'] = $deposit->getFailureReason();
        }
        if (in_array('transactionId', $keys)) {
            $attributes['transactionId'] = $deposit->getTransactionId();
        }
      
        $expression['data']['attributes'] = $attributes;

        if (in_array('memberAccountId', $keys)) {
            $expression['data']['relationships']['memberAccount']['data'] = array(
                array(
                    'type' => 'memberAccounts',
                    'id' => Core::$container->get('user')->getId()
                )
             );
        }

        return $expression;
    }
}
