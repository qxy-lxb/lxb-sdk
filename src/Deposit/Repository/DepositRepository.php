<?php
namespace Sdk\Deposit\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;

use Sdk\Deposit\Adapter\Deposit\IDepositAdapter;
use Sdk\Deposit\Adapter\Deposit\DepositRestfulAdapter;
use Sdk\Deposit\Adapter\Deposit\DepositMockAdapter;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Deposit\Model\Deposit;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class DepositRepository extends Repository implements IDepositAdapter
{
    use  AsyncRepositoryTrait, OperatAbleRepositoryTrait, FetchRepositoryTrait;

    const LIST_MODEL_UN = 'DEPOSIT_LIST';
    const FETCH_ONE_MODEL_UN = 'DEPOSIT_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new DepositRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : IDepositAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : DepositAdapter
    {
        return new DepositMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function pay(Deposit $deposit): bool
    {
        return $this->getAdapter()->pay($deposit);
    }

    public function paymentFailure(Deposit $deposit): bool
    {
        return $this->getAdapter()->paymentFailure($deposit);
    }
}
