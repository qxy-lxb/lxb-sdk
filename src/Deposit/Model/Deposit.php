<?php
namespace Sdk\Deposit\Model;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;

use Sdk\MemberAccount\Model\MemberAccount;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Marmot\Core;

use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Deposit\Repository\DepositRepository;

use Sdk\TradeRecord\Model\ITradeAble;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Deposit implements IOperatAble, IObject, ITradeAble
{
    use OperatAbleTrait, Object;

    const DEPOSIT_STATUS = array(
        "PENDING" => 0, //未充值
        "APPROVE" => 2, //已充值
        "REJECT" => -2 //充值失败
    );

    const PAYMENT_TYPE_CN = array(
        self::PAYMENT_TYPE['WECHAT'] => '微信',
        self::PAYMENT_TYPE['ALIPAY'] => '支付宝',
        self::PAYMENT_TYPE['IOS'] => 'ios支付',
    );

    const PAYMENT_TYPE = array(
        'NULL' => 0,
        'WECHAT' => 1,
        'ALIPAY' => 2,
        'IOS' => 3
    );

    private $id;

    private $amount;

    private $depositName;

    private $goldCoinId;

    private $transactionPlatform;

    private $price;

    private $number;

    private $transactionId;

    private $paymentId;

    private $paymentType;

    private $paymentTime;

    private $transactionNumber;

    private $transactionInfo;

    private $failureReason;

    private $memberAccount;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->amount = 0;
        $this->depositName = "";
        $this->goldCoinId = 0;
        $this->transactionPlatform = 0;
        $this->price = 0;
        $this->number = '';
        $this->transactionId = '';
        $this->paymentId = 0;
        $this->paymentType = self::PAYMENT_TYPE['NULL'];
        $this->paymentTime = 0;
        $this->transactionNumber = '';
        $this->transactionInfo = [];
        $this->failureReason = [];
        $this->memberAccount = new MemberAccount();
        $this->repository = new DepositRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->amount);
        unset($this->depositName);
        unset($this->goldCoinId);
        unset($this->transactionPlatform);
        unset($this->price);
        unset($this->number);
        unset($this->transactionId);
        unset($this->paymentId);
        unset($this->paymentType);
        unset($this->paymentTime);
        unset($this->transactionNumber);
        unset($this->transactionInfo);
        unset($this->failureReason);
        unset($this->memberAccount);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }
    
    public function setDepositName(string $depositName) : void
    {
        $this->depositName = $depositName;
    }

    public function getDepositName() : string
    {
        return $this->depositName;
    }

    public function setAmount(int $amount) : void
    {
        $this->amount = $amount;
    }

    public function getAmount() : int
    {
        return $this->amount;
    }

    public function setGoldCoinId(int $goldCoinId) : void
    {
        $this->goldCoinId = $goldCoinId;
    }

    public function getGoldCoinId() : int
    {
        return $this->goldCoinId;
    }

    public function setTransactionPlatform(int $transactionPlatform) : void
    {
        $this->transactionPlatform = $transactionPlatform;
    }

    public function getTransactionPlatform() : int
    {
        return $this->transactionPlatform;
    }

    public function setPrice(float $price) : void
    {
        $this->price = $price;
    }

    public function getPrice() : float
    {
        return $this->price;
    }

    public function setNumber(string $number) : void
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setTransactionId(string $transactionId) : void
    {
        $this->transactionId = $transactionId;
    }

    public function getTransactionId() : string
    {
        return $this->transactionId;
    }
    
    public function setPaymentId(int $paymentId) : void
    {
        $this->paymentId = $paymentId;
    }

    public function getPaymentId() : int
    {
        return $this->paymentId;
    }
    
    public function setPaymentType(int $paymentType) : void
    {
        $this->paymentType = in_array($paymentType, self::PAYMENT_TYPE) ?
            $paymentType : self::PAYMENT_TYPE['NULL'];
    }

    public function getPaymentType() : int
    {
        return $this->paymentType;
    }

    public function setPaymentTime(int $paymentTime) : void
    {
        $this->paymentTime = $paymentTime;
    }

    public function getPaymentTime() : int
    {
        return $this->paymentTime;
    }

    public function setTransactionNumber(string $transactionNumber) : void
    {
        $this->transactionNumber = $transactionNumber;
    }

    public function getTransactionNumber() : string
    {
        return $this->transactionNumber;
    }

    public function setTransactionInfo(array $transactionInfo) : void
    {
        $this->transactionInfo = $transactionInfo;
    }

    public function getTransactionInfo() : array
    {
        return $this->transactionInfo;
    }

    public function setFailureReason(array $failureReason) : void
    {
        $this->failureReason = $failureReason;
    }

    public function getFailureReason() : array
    {
        return $this->failureReason;
    }

    public function setMemberAccount(MemberAccount $memberAccount) : void
    {
        $this->memberAccount = $memberAccount;
    }

    public function getMemberAccount() : MemberAccount
    {
        return $this->memberAccount;
    }
    
    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::DEPOSIT_STATUS) ? $status : self::DEPOSIT_STATUS['PENDING'];
    }

    protected function getRepository() : DepositRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleRepository() : DepositRepository
    {
        return $this->getRepository();
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    public function pay() : bool
    {
        return $this->getRepository()->pay($this);
    }

    public function paymentFailure() : bool
    {
        return $this->getRepository()->paymentFailure($this);
    }
}
