<?php
namespace Sdk\Deposit\Adapter\Deposit;

use Sdk\Common\Adapter\IOperatAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;
use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Deposit\Model\Deposit;

interface IdepositAdapter extends IAsyncAdapter, IOperatAbleAdapter, IFetchAbleAdapter
{
	public function pay(Deposit $deposit) : bool;

    public function paymentFailure(Deposit $deposit) : bool;
}
