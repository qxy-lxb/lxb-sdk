<?php
namespace Sdk\Deposit\Adapter\Deposit;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\CommonMapErrorsTrait;

use Sdk\Deposit\Translator\DepositRestfulTranslator;
use Sdk\Deposit\Model\NullDeposit;
use Sdk\Deposit\Model\Deposit;

use Marmot\Core;

class DepositRestfulAdapter extends GuzzleAdapter implements IDepositAdapter
{
    use OperatAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'DEPOSIT_LIST'=>[
            'fields'=>[],
            'include'=>'memberAccount'
        ],
        'DEPOSIT_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'memberAccount'
        ]
    ];
    
    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new DepositRestfulTranslator();
        $this->resource = 'deposits';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            75002 => DEPOSIT_AMOUNT_UNREASONABLE,
            75003 => DEPOSIT_STATUS_NOT_PENDING,
            75004 => PAYMENT_AMOUNT_MISMATCH
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
    
    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullDeposit());
    }

    public function addAction(Deposit $deposit) : bool
    {
        
        $data = $this->getTranslator()->objectToArray(
            $deposit,
            array('goldCoinId', 'memberAccountId','transactionPlatform')
        );
        $this->post(
            $this->getResource(),
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($deposit);
            return true;
        }

        return false;
    }

    public function editAction(int $id) : bool
    {
        unset($id);
        return false;
    }

    public function pay(Deposit $deposit) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $deposit,
            array(
                'paymentType',
                'transactionNumber',
                'transactionInfo',
                'transactionId'
            )
        );
        
        $this->patch($this->getResource().'/'.$deposit->getId().'/payment',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($deposit);
            return true;
        }

        return false;
    }

    public function paymentFailure(Deposit $deposit) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $deposit,
            array(
                'failureReason'
            )
        );
        
        $this->patch(
            $this->getResource().'/'.$deposit->getId().'/paymentFailure',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($deposit);
            return true;
        }

        return false;
    }
}
