<?php
/**
1-99 系统错误规范
100-500 通用错误规范
501-1000 用户通用错误规范
1001-2000 APP用户错误提示规范
2001-3000 OA用户错误提示规范
3001-4000 实名认证错误提示
6501-7000 修复记录错误提示规范
7001-7500 视频模块错误提示规范
7501-8000 试题模块错误提示规范
8001-8500 课程模块错误提示规范
9000-9500 考试记录模块错误提示规范
9501-10000 充值模块错误提示规范
 */

// 1-99 系统错误规范----------------------------------------------------------------

/**
 * csrf 验证失效
 */
define('CSRF_VERIFY_FAILURE', 15);
/**
 * 滑动验证失败
 */
define('AFS_VERIFY_FAILURE', 16);
/**
 * 用户未登录
 */
define('NEED_SIGNIN', 17);
/**
 * 短信发送太频繁
 */
define('SMS_SEND_TOO_QUICK', 18);
/**
 * 哈希无效
 */
define('HASH_INVALID', 19);

// 100-500 通用错误规范----------------------------------------------------------------
/**
 * 数据重复
 */
define('PARAMETER_IS_UNIQUE', 100);
/**
 * 参数不能为空
 */
define('PARAMETER_IS_EMPTY', 101);
/**
 * 状态已禁用
 */
define('STATUS_ALREADY_DISABLED', 102);
/**
 * 状态已启用
 */
define('STATUS_ALREADY_ENABLED', 103);
/**
 * 图片格式不正确
 */
define('IMAGE_FORMAT_ERROR', 104);
/**
 * 附件格式不正确
 */
define('ATTACHMENT_FORMAT_ERROR', 105);
/**
 * 数据格式不正确
 */
define('PARAMETER_FORMAT_INCORRECT', 106);

/**
 * 凭证格式不正确
 */
define('VOUCHER_FORMAT_ERROR', 107);
/**
 * 日期格式不正确
 */
define('DATE_FORMAT_ERROR', 108);
/**
 * 审核状态非审核通过
 */
define('APPLY_STATUS_NOT_APPROVED', 109);
/**
 * id格式不正确
 */
define('ID_FORMAT_ERROR', 110);
/**
 * 价格格式不正确
 */
define('PRICE_FORMAT_ERROR', 111);
/**
 * 审核状态非未提交审核
 */
define('APPLY_STATUS_NOT_UNSUBMITTED', 112);
/**
 * 来源格式不正确
 */
define('SOURCE_FORMAT_ERROR', 113);
/**
 * 已置顶
 */
define('STICK_ALREADY_TOP', 114);
/**
 * 已取消置顶
 */
define('STICK_ALREADY_CANCELTOP', 115);
/**
 * 已读
 */
define('RESOURCE_STATUS_READ', 116);
/**
 * 状态为已上架
 */
define('STATUS_ALREADY_SHELVE', 117);
/**
 * 状态为已下架
 */
define('STATUS_ALREADY_OFF_SHELVE', 118);
// 501-1000 用户通用错误规范----------------------------------------------------------------

/**
 * 姓名格式不正确
 */
define('REAL_NAME_FORMAT_ERROR', 501);
/**
 * 手机号格式不正确
 */
define('CELLPHONE_FORMAT_ERROR', 502);
/**
 * 密码格式不正确
 */
define('PASSWORD_FORMAT_ERROR', 503);
/**
 * 身份证格式不正确
 */
define('CARDID_FORMAT_ERROR', 504);
/**
 * 旧密码不正确
 */
define('OLD_PASSWORD_INCORRECT', 505);
/**
 * 密码错误
 */
define('PASSWORD_INCORRECT', 506);
/**
 * 密码与确认密码不一致
 */
define('PASSWORD_INCONSISTENCY', 507);
/**
 * 名称格式不正确
 */
define('NAME_FORMAT_ERROR', 508);
/**
 * 手机号已存在
 */
define('CELLPHONE_EXIST', 509);
/**
 * 账号不存在
 */
define('CELLPHONE_NOT_EXIST', 510);
/**
 * 验证码错误
 */
define('CAPTCHA_ERROR', 511);
/**
 * 密码与确认密码不一致
 */
define('INCONSISTENT_PASSWORD', 512);
/**
 * 性别格式不正确
 */
define('GENDER_FORMAT_ERROR', 513);
/**
 * 设备地址格式不正确
 */
define('DEVICE_ADDRESS_FORMAT_ERROR', 514);

// 1001-2000 APP用户错误提示规范----------------------------------------------------------------

/**
 * 昵称格式不正确
 */
define('NICK_NAME_FORMAT_ERROR', 1001);
/**
 * 地址格式不正确
 */
define('ADDRESS_FORMAT_ERROR', 1002);
/**
 * 性别类型不存在
 */
define('GENDER_TYPE_NOT_EXIST', 1003);

// 2001-3000 OA用户错误提示规范----------------------------------------------------------------

/**
 * 所属角色格式不正确
 */
define('ROLE_FORMAT_ERROR', 2001);
/**
 * 所属机构名称类型不存在
 */
define('ORGANIZATION_TYPE_NOT_EXIST', 2002);
// 3001 占用

// 3501-4000

//企业认证
/**
 * 企业名称格式不正确
 */
define('ENTERPRISE_NAME_FORMAT_ERROR', 3501);
/**
 * 统一社会信用代码格式不正确
 */
define('CODE_FORMAT_ERROR', 3502);
/**
 * 法定代表人格式不正确
 */
define('PRINCIPAL_FORMAT_ERROR', 3503);
/**
 * 联系人格式不正确
 */
define('CONTACTS_FORMAT_ERROR', 3504);
/**
 * 联系电话格式不正确
 */
define('PHONE_FORMAT_ERROR', 3505);
/**
 * 企业授权函格式不正确
 */
define('AUTH_LETTER_FORMAT_ERROR', 3506);
/**
 * 注册时间格式不正确
 */
define('REGISTERED_DATE_FORMAT_ERROR', 3507);
/**
 * 注册资本格式不正确
 */
define('CAPITAL_FORMAT_ERROR', 3508);
/**
 * 认证用户格式不正确
 */
define('ENTERPRISE_MEMBER_FORMAT_ERROR', 3509);
/**
 * 企业驳回原因格式不正确
 */
define('REJECTREASON_FORMAT_ERROR', 3510);
// 5001-5500 新闻错误规范----------------------------------------------------------------

/**
 * 新闻类型不存在
 */
define('NEWS_TYPE_NOT_EXIST', 5001);
/**
 * 标题格式不正确
 */
define('TITLE_FORMAT_ERROR', 5002);

// 5501-6000 信用修复规范----------------------------------------------------------------

/**
 * 行政处罚类型不存在
 */
define('REPAIRRECORD_TYPE_NOT_EXIST', 5501);
/**
 * 信用修复驳回原因格式不正确
 */
define('REPAIR_RECORD_REJECTREASON_FORMAT_ERROR', 5502);
/**
 * 信用修复递交状态已递交
 */
define('REPAIR_RECORD_DELIVER_STATUS_ALREADY_SUBMITTED', 5503);
/**
 * 信用修复判定状态已判定
 */
define('REPAIR_RECORD_DETERMINE_STATUS_ALREADY_DETERMINED', 5504);
/**
 * 信用修复审核状态非未提交审核
 */
define('REPAIR_RECORD_APPLY_STATUS_NOT_UNSUBMITTED', 5505);

// 6001-6500 第三方机构错误提示规范
/**
 * 机构名称格式不正确
 */
define('ORGANIZATION_NAME_FORMAT_ERROR', 6001);
/**
 * 联系人格式不正确
 */
define('ORGANIZATION_CONTACTS_FORMAT_ERROR', 6002);
/**
 * 联系电话格式不正确
 */
define('ORGANIZATION_PHONE_FORMAT_ERROR', 6003);
/**
 * 官网地址格式不正确
 */
define('ORGANIZATION_URL_FORMAT_ERROR', 6004);
/**
 * 入驻日期格式不正确
 */
define('SETTLED_DATE_FORMAT_ERROR', 6005);
/**
 * 截止日期格式不正确
 */
define('END_DATE_FORMAT_ERROR', 6006);
/**
 * 合同文件格式不正确
 */
define('CONTRACT_FORMAT_ERROR', 6007);
/**
 * 机构分类格式不正确
 */
define('ORGANIZATION_CATEGORY_FORMAT_ERROR', 6008);
/**
 * 机构名称存在
 */
define('ORGANIZATION_NAME_EXIST', 6009);
/**
 * 培训机构类型不存在
 */
define('ORGANIZATION_TRAIN_TYPE_FORMAT_ERROR', 6010);

// 6501-7000 信用修复记录
/**
 * 失信信息格式不正确
 */
define('DISHONESTY_INFO_FORMAT_ERROR', 6501);
/**
 * 6502被占用
 */
/**
 * 培训发起方格式错误
 */
define('INITIATOR_FORMAT_ERROR', 6503);
/**
 * 受培训相关资料格式错误
 */
define('RELEVANT_INFORMATION_FORMAT_ERROR', 6504);
/**
 * 报告出具机构名称格式错误
 */
define('REPORT_ORGANIZATION_FORMAT_ERROR', 6505);
/**
 * 报告名称格式错误
 */
define('REPORT_NAME_FORMAT_ERROR', 6506);
/**
 * 报告出具时间格式错误
 */
define('ISSUE_TIME_FORMAT_ERROR', 6507);
/**
 * 报告截止时间格式错误
 */
define('END_TIME_FORMAT_ERROR', 6508);
/**
 * 报告编号格式错误
 */
define('REPORT_NUMBER_FORMAT_ERROR', 6509);
/**
 * 报告信用等级格式错误
 */
define('REPORT_GRADE_FORMAT_ERROR', 6510);
/**
 * 信用报告电子版格式错误
 */
define('ELECTRONIC_REPORT_FORMAT_ERROR', 6511);

// 7001-7500 视频模块
/**
 * 视频名称格式不正确
 */
define('VIDEO_NAME_FORMAT_ERROR', 7001);
/**
 * 视频学时格式不正确
 */
define('VIDEO_STUDY_TIME_FORMAT_ERROR', 7002);
/**
 * 视频时长格式不正确
 */
define('VIDEO_TIME_FORMAT_ERROR', 7003);
/**
 * 视频格式不正确
 */
define('VIDEO_FORMAT_ERROR', 7004);
/**
 * 视频对应类型不正确
 */
define('VIDEO_CATEGORY_FORMAT_ERROR', 7005);

// 7501-8000 试题模块
/**
 * 题目格式不正确
 */
define('EXAMINATIONS_SUBJECT_FORMAT_ERROR', 7501);
/**
 * 选项格式不正确
 */
define('EXAMINATIONS_OPTIONS_FORMAT_ERROR', 7502);
/**
 * 答案格式不正确
 */
define('EXAMINATIONS_ANSWERS_FORMAT_ERROR', 7503);
/**
 * 选项类型不正确
 */
define('EXAMINATIONS_OPTIONS_TYPE_FORMAT_ERROR', 7504);

// 8001-8500 课程模块
/**
 * 课程编号格式不正确
 */
define('COURSE_NUMBER_FORMAT_ERROR', 8001);
/**
 * 课程名称格式不正确
 */
define('COURSE_NAME_FORMAT_ERROR', 8002);
/**
 * 课程对应类型不正确
 */
define('COURSE_CATEGORY_FORMAT_ERROR', 8003);
/**
 * 课程价格格式不正确
 */
define('COURSE_PRICE_FORMAT_ERROR', 8004);
/**
 * 课程学时格式不正确
 */
define('COURSE_STUDYTIME_FORMAT_ERROR', 8005);
/**
 * 课程计分规则格式不正确
 */
define('COURSE_PASSLINE_FORMAT_ERROR', 8006);
/**
 * 课程有效期格式不正确
 */
define('COURSE_EXPIRATION_DATE_FORMAT_ERROR', 8007);
/**
 * 课程视频格式不正确
 */
define('COURSE_VIDEOS_FORMAT_ERROR', 8008);
/**
 * 课程简介格式不正确
 */
define('COURSE_SYNOPSIS_FORMAT_ERROR', 8009);
/**
 * 是否需要考试格式不正确
 */
define('IS_REQUIRED_EXAM_FORMAT_ERROR', 8010);
/**
 * 课程状态已上架
 */
define('COURSE_STATUS_ALREADY_ON', 8011);
/**
 * 课程状态已下架
 */
define('COURSE_STATUS_ALREADY_OFF', 8012);
/**
 * 课程下架原因格式不正确
 */
define('COURSE_OFF_REASON_FORMAT_ERROR', 8013);
/**
 * 课程驳回原因格式不正确
 */
define('COURSE_REJECTREASON_FORMAT_ERROR', 8014);
/**
 * 课程学时不满足3学时
 */
define('COURSE_STUDYTIME_DISSATISFACTION_FORMAT_ERROR', 8015);
/**
 * 课程试题不足40道
 */
define('COURSE_EXAMINATION_SIZE_FORMAT_ERROR', 8016);
/**
 * 课程快照格式不正确
 */
define('COURSE_SNAPSHOT_FORMAT_ERROR', 8017);
/**
 * 该用户不具备发布权限
 */
define('CREW_NOT_HAVE_PUBLISHING_PERMISSION', 8018);

// 9000-9500 考试记录
/**
 * 考试试卷格式不正确
 */
define('EXAM_PAPER_FORMAT_ERROR', 90001);
/**
 * 已学习时长进度格式不正确
 */
define('LEARNED_PROGRESS_FORMAT_ERROR', 90002);
/**
 * 关联培训记录格式不正确
 */
define('TRAINING_RECORD_FORMAT_ERROR', 90003);
/**
 * 本次培训进度已通过
 */
define('TRAINING_RECORD_ALREADY_PASS', 90004);
/**
 * 本次培训进度已完成
 */
define('TRAINING_RECORD_ALREADY_FINISHED', 90005);
/**
 * 本次培训学习进度为已完成
 */
define('TRAINING_RECORD_ALREADY_LEARNED', 90006);

// 9501-10000 服务管理
/**
 * 报告价格格式不正确
 */
define('SERVICE_PRICE_FORMAT_ERROR', 9501);
/**
 * 附件格式不正确
 */
define('SERVICE_ATTACHMENTS_FORMAT_ERROR', 9502);
/**
 * 是否需要用户协议格式不正确
 */
define('SERVICE_ISAGREEMENT_FORMAT_ERROR', 9503);
/**
 * 报告名称格式不正确
 */
define('SERVICE_REPORTNAME_FORMAT_ERROR', 9504);
/**
 * 服务快照格式不正确
 */
define('SERVICE_SNAPSHOT_FORMAT_ERROR', 9505);
/**
 * 服务状态已上架
 */
define('SERVICE_STATUS_ALREADY_ON', 9506);
/**
 * 服务状态已下架
 */
define('SERVICE_STATUS_ALREADY_OFF', 9507);
/**
 * 服务驳回原因格式不正确
 */
define('SERVICE_REJECTREASON_FORMAT_ERROR', 9508);
/**
 * 服务强制下架原因格式不正确
 */
define('SERVICE_OFF_REASON_FORMAT_ERROR', 9509);
/**
 * 是否负担服务费格式不正确
 */
define('SERVICE_OFF_IS_BEAR_ERROR', 9510);
/**
 * 充值金额不合理
 */
define('DEPOSIT_AMOUNT_UNREASONABLE', 9601);
/**
 * 金币名称格式不正确
 */
define('GOLD_COIN_NAME_FORMAT_ERROR', 70001);
/**
 * 金币数量格式不正确
 */
define('GOLD_COIN_NUMBER_FORMAT_ERROR', 70002);
/**
 * 产品ID格式不正确
 */
define('GOLD_COIN_PRODUCT_ID_FORMAT_ERROR', 70003);
/**
 * 购买金额格式不正确
 */
define('GOLD_COIN_AMOUNT_FORMAT_ERROR', 70004);
/**
 * 发布用户格式不正确
 */
define('GOLD_COIN_CREW_FORMAT_ERROR', 70005);

// 充值
/**
 * 账户余额不足
 */
define('ACCOUNT_BALANCE_NOT_ENOUGH', 9501);
/**
 * 充值状态非待充值
 */
define('DEPOSIT_STATUS_NOT_PENDING', 9503);
/**
 * 支付金额不匹配
 */
define('PAYMENT_AMOUNT_MISMATCH', 9504);

