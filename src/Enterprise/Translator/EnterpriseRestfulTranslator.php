<?php
namespace Sdk\Enterprise\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Enterprise\Model\Enterprise;
use Sdk\Enterprise\Model\LegalPersonInfo;
use Sdk\Enterprise\Model\ContactsInfo;
use Sdk\Enterprise\Model\NullEnterprise;

use Sdk\Member\Translator\MemberRestfulTranslator;

/**
 * 屏蔽类中所有PMD警告
 *
 * @SuppressWarnings(PHPMD)
 */
class EnterpriseRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getMemberRestfulTranslator()
    {
        return new MemberRestfulTranslator();
    }

    public function arrayToObject(array $expression, $enterprise = null)
    {
        return $this->translateToObject($expression, $enterprise);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $enterprise = null)
    {
        if (empty($expression)) {
            return NullEnterprise::getInstance();
        }

        if ($enterprise == null) {
            $enterprise = new Enterprise();
        }

        $data =  $expression['data'];

        $id = $data['id'];
        $enterprise->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $enterprise->setName($attributes['name']);
        }
        if (isset($attributes['unifiedSocialCreditCode'])) {
            $enterprise->setUnifiedSocialCreditCode($attributes['unifiedSocialCreditCode']);
        }
        if (isset($attributes['logo'])) {
            $enterprise->setLogo($attributes['logo']);
        }
        if (isset($attributes['businessLicense'])) {
            $enterprise->setBusinessLicense($attributes['businessLicense']);
        }
        if (isset($attributes['authLetter'])) {
            $enterprise->setPowerAttorney($attributes['authLetter']);
        }
        if (isset($attributes['principal'])) {
            $enterprise->setLegalPerson($attributes['principal']);
        }
        if (isset($attributes['registeredDate'])) {
            $enterprise->setEstablishDate($attributes['registeredDate']);
        }
        if (isset($attributes['capital'])) {
            $enterprise->setCapital($attributes['capital']);
        }
        if (isset($attributes['rejectReason'])) {
            $enterprise->setRejectReason($attributes['rejectReason']);
        }

        $contactsName = isset($attributes['contacts'])
        ? $attributes['contacts']
        : '';
        $contactsCellphone = isset($attributes['phone'])
        ? $attributes['phone']
        : '';
        $enterprise->setContactsInfo(
            new ContactsInfo(
                $contactsName,
                $contactsCellphone
            )
        );

        if (isset($attributes['createTime'])) {
            $enterprise->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $enterprise->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $enterprise->setStatus($attributes['status']);
        }
        if (isset($attributes['applyStatus'])) {
            $enterprise->setApplyStatus($attributes['applyStatus']);
        }
        if (isset($attributes['statusTime'])) {
            $enterprise->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);
            $enterprise->setMember($this->getMemberRestfulTranslator()->arrayToObject($member));
        }

        return $enterprise;
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($enterprise, array $keys = array())
    {
        if (!$enterprise instanceof Enterprise) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'name',
                'unifiedSocialCreditCode',
                'logo',
                'businessLicense',
                'powerAttorney',
                'legalPerson',
                'establishDate',
                'capital',
                'contactsName',
                'contactsCellphone',
                'member',
                'rejectReason'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'enterprises'
            )
        );
        
        if (in_array('id', $keys)) {
            $expression['data']['id'] = $enterprise->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $enterprise->getName();
        }
        if (in_array('unifiedSocialCreditCode', $keys)) {
            $attributes['unifiedSocialCreditCode'] = $enterprise->getUnifiedSocialCreditCode();
        }
        if (in_array('logo', $keys)) {
            $attributes['logo'] = $enterprise->getLogo();
        }
        if (in_array('businessLicense', $keys)) {
            $attributes['businessLicense'] = $enterprise->getBusinessLicense();
        }
        if (in_array('powerAttorney', $keys)) {
            $attributes['authLetter'] = $enterprise->getPowerAttorney();
        }
        if (in_array('legalPerson', $keys)) {
            $attributes['principal'] = $enterprise->getLegalPerson();
        }
        if (in_array('establishDate', $keys)) {
            $attributes['registeredDate'] = $enterprise->getEstablishDate();
        }
        if (in_array('capital', $keys)) {
            $attributes['capital'] = $enterprise->getCapital();
        }
        if (in_array('contactsName', $keys)) {
            $attributes['contacts'] = $enterprise->getContactsInfo()->getName();
        }
        if (in_array('contactsCellphone', $keys)) {
            $attributes['phone'] = $enterprise->getContactsInfo()->getCellphone();
        }
        if (in_array('rejectReason', $keys)) {
            $attributes['rejectReason'] = $enterprise->getRejectReason();
        }
        $expression['data']['attributes'] = $attributes;

        if (in_array('member', $keys)) {
            $expression['data']['relationships']['member']['data'] = array(
                array(
                    'type' => 'members',
                    'id' => $enterprise->getMember()->getId()
                )
             );
        }

        return $expression;
    }
}
