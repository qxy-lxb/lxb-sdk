<?php
namespace Sdk\Enterprise\Model;

class ContactsInfo
{
    private $name;

    private $cellphone;

    public function __construct(
        string $name = '',
        string $cellphone = ''
    ) {
        $this->name = $name;
        $this->cellphone = $cellphone;
    }

    public function __destruct()
    {
        unset($this->name);
        unset($this->cellphone);
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getCellphone() : string
    {
        return $this->cellphone;
    }
}
