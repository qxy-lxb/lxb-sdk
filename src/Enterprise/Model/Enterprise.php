<?php
namespace Sdk\Enterprise\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Model\ApplyAbleSmsTrait;
use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Adapter\IApplyAbleAdapter;
use Sdk\Notify\Model\SmsMessage;

use Sdk\Member\Model\Member;

use Sdk\Enterprise\Repository\EnterpriseRepository;

class Enterprise implements IOperatAble, IObject, IApplyAble
{
    use OperatAbleTrait, Object, ApplyAbleSmsTrait;
    /**
     * [$id ID]
     * @var [int]
     */
    private $id;
    /**
     * [$name 企业名称]
     * @var [string]
     */
    private $name;
    /**
     * [$unifiedSocialCreditCode 统一社会信用代码]
     * @var [string]
     */
    private $unifiedSocialCreditCode;
    /**
     * [$logo logo]
     * @var [array]
     */
    private $logo;
    /**
     * [$businessLicense 营业执照]
     * @var [array]
     */
    private $businessLicense;
    /**
     * [$powerAttorney 企业授权函]
     * @var [array]
     */
    private $powerAttorney;
    /**
     * [$legalPerson 法人姓名]
     * @var string
     */
    private $legalPerson;
    /**
     * [$establishDate 注册时间]
     * @var string
     */
    private $establishDate;
    /**
     * [$capital 注册资本]
     * @var string
     */
    private $capital;
    /**
     * [$contactsInfo 联系人信息]
     * @var [ContactsInfo]
     */
    private $contactsInfo;
    /**
     * [$member 关联账户]
     * @var [Member]
     */
    private $member;
    /**
     * [$rejectReason 驳回原因]
     * @var [string]
     */
    private $rejectReason;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->unifiedSocialCreditCode = '';
        $this->logo = array();
        $this->businessLicense = array();
        $this->powerAttorney = array();
        $this->legalPerson = '';
        $this->establishDate = '';
        $this->capital = '';
        $this->contactsInfo = new ContactsInfo();
        $this->member = Core::$container->has('user') ? Core::$container->get('user') : new Member();
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = 0;
        $this->applyStatus = 0;
        $this->rejectReason = '';
        $this->repository = new EnterpriseRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->unifiedSocialCreditCode);
        unset($this->logo);
        unset($this->businessLicense);
        unset($this->powerAttorney);
        unset($this->legalPerson);
        unset($this->establishDate);
        unset($this->capital);
        unset($this->rejectReason);
        unset($this->contactsInfo);
        unset($this->member);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->applyStatus);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setUnifiedSocialCreditCode(string $unifiedSocialCreditCode) : void
    {
        $this->unifiedSocialCreditCode = $unifiedSocialCreditCode;
    }

    public function getUnifiedSocialCreditCode() : string
    {
        return $this->unifiedSocialCreditCode;
    }

    public function setLogo(array $logo) : void
    {
        $this->logo = $logo;
    }

    public function getLogo() : array
    {
        return $this->logo;
    }

    public function setBusinessLicense(array $businessLicense) : void
    {
        $this->businessLicense = $businessLicense;
    }

    public function getBusinessLicense() : array
    {
        return $this->businessLicense;
    }

    public function setPowerAttorney(array $powerAttorney) : void
    {
        $this->powerAttorney = $powerAttorney;
    }

    public function getPowerAttorney() : array
    {
        return $this->powerAttorney;
    }

    public function setLegalPerson(string $legalPerson) : void
    {
        $this->legalPerson = $legalPerson;
    }

    public function getLegalPerson() : string
    {
        return $this->legalPerson;
    }

    public function setEstablishDate(string $establishDate) : void
    {
        $this->establishDate = $establishDate;
    }

    public function getEstablishDate() : string
    {
        return $this->establishDate;
    }

    public function setCapital(string $capital) : void
    {
        $this->capital = $capital;
    }

    public function getCapital() : string
    {
        return $this->capital;
    }

    public function setContactsInfo(ContactsInfo $contactsInfo) : void
    {
        $this->contactsInfo = $contactsInfo;
    }

    public function getContactsInfo() : ContactsInfo
    {
        return $this->contactsInfo;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function setApplyStatus(int $applyStatus) : void
    {
        $this->applyStatus = $applyStatus;
    }

    public function getApplyStatus() : int
    {
        return $this->applyStatus;
    }

    protected function getRepository()
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return $this->getRepository();
    }

    public function setRejectReason(string $rejectReason) : void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason() : string
    {
        return $this->rejectReason;
    }
    /**
     * 企业实名认证新增保存
     * @return bool 企业实名认证新增保存
     */
    public function save() : bool
    {
        return $this->getRepository()->save($this);
    }
    /**
     * 企业实名认证新增保存
     * @return bool 企业实名认证新增保存
     */
    public function editSave() : bool
    {
        return $this->getRepository()->editSave($this);
    }

    public function approveSms()
    {
        $template = ENTERPRISE_APPROVE_TEMPLATE_ID_AUTH;

        $message = new SmsMessage($template);

        $targets = $this->getMember()->getCellphone();

        $enterpriseName = $this->getName();

        $message->setTargets($targets);

        $content = array(
            'enterpriseName' => $enterpriseName
        );
        
        $message->setContent($content);
        
        if ($message->send()) {
            return true;
        }
        return false;
    }

    public function rejectActionSms()
    {
        $template = ENTERPRISE_REJECT_TEMPLATE_ID_AUTH;

        $message = new SmsMessage($template);

        $targets = $this->getMember()->getCellphone();

        $enterpriseName = $this->getName();

        $message->setTargets($targets);

        $content = array(
            'enterpriseName' => $enterpriseName
        );
        
        $message->setContent($content);

        if ($message->send()) {
            return true;
        }
        return false;
    }
}
