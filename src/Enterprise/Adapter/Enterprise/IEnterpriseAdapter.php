<?php
namespace Sdk\Enterprise\Adapter\Enterprise;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IApplyAbleAdapter;
use Sdk\Enterprise\Model\Enterprise;

interface IEnterpriseAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IAsyncAdapter, IApplyAbleAdapter
{
    public function save(Enterprise $enterprise);
    public function editSave(Enterprise $enterprise);
}
