<?php
namespace Sdk\Enterprise\Adapter\Enterprise;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\ApplyAbleRestfulAdapterTrait;

use Sdk\Enterprise\Model\Enterprise;
use Sdk\Enterprise\Model\NullEnterprise;
use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;

class EnterpriseRestfulAdapter extends GuzzleAdapter implements IEnterpriseAdapter
{
    use CommonMapErrorsTrait,
        FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        ApplyAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'ENTERPRISE_LIST'=>[
            'fields'=>[
                'enterprises'=>
                    'name,unifiedSocialCreditCode,principal,applyStatus,registeredDate,updateTime,createTime,member,capital,logo,rejectReason'
            ],
            'include'=>'member'
        ],
        'ENTERPRISE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'member'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new EnterpriseRestfulTranslator();
        $this->resource = 'enterprises';
        $this->scenario = array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            5001 => ENTERPRISE_NAME_FORMAT_ERROR,
            5002 => CODE_FORMAT_ERROR,
            5003 => PRINCIPAL_FORMAT_ERROR,
            5004 => CONTACTS_FORMAT_ERROR,
            5005 => PHONE_FORMAT_ERROR,
            5007 => REGISTERED_DATE_FORMAT_ERROR,
            5008 => CAPITAL_FORMAT_ERROR,
            5010 => ENTERPRISE_MEMBER_FORMAT_ERROR,
            9007 => APPLY_STATUS_NOT_UNSUBMITTED,
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullEnterprise::getInstance());
    }
    /**
     * [addAction 认证企业信息]
     * @param Enterprise $enterprise [企业对象]
     * @return [bool]                [返回类型]
     */
    protected function addAction(Enterprise $enterprise) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $enterprise,
            array(
                'name',
                'unifiedSocialCreditCode',
                'logo',
                'businessLicense',
                'powerAttorney',
                'legalPerson',
                'establishDate',
                'capital',
                'contactsName',
                'contactsCellphone',
                'member'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($enterprise);
            return true;
        }

        return false;
    }
    /**
     * [editAction 编辑企业信息]
     * @param  Enterprise $enterprise [企业对象]
     * @return [bool]                 [返回类型]
     */
    protected function editAction(Enterprise $enterprise) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $enterprise,
            array(
                'name',
                'unifiedSocialCreditCode',
                'logo',
                'businessLicense',
                'powerAttorney',
                'legalPerson',
                'establishDate',
                'capital',
                'contactsName',
                'contactsCellphone'
            )
        );

        $this->patch(
            $this->getResource().'/'.$enterprise->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($enterprise);
            return true;
        }

        return false;
    }
    /**
     * [save 企业实名认证新增保存]
     * @param Enterprise $enterprise [企业对象]
     * @return [bool]                [返回类型]
     */
    public function save(Enterprise $enterprise) : bool
    {
        return $this->saveAction($enterprise);
    }
    protected function saveAction(Enterprise $enterprise) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $enterprise,
            array(
                'name',
                'unifiedSocialCreditCode',
                'logo',
                'businessLicense',
                'powerAttorney',
                'legalPerson',
                'establishDate',
                'capital',
                'contactsName',
                'contactsCellphone',
                'member'
            )
        );

        $this->post(
            $this->getResource().'/save',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($enterprise);
            return true;
        }

        return false;
    }
    /**
     * [editSave 企业实名认证编辑保存]
     * @param Enterprise $enterprise [企业对象]
     * @return [bool]                [返回类型]
     */
    public function editSave(Enterprise $enterprise) : bool
    {
        return $this->editSaveAction($enterprise);
    }
    protected function editSaveAction(Enterprise $enterprise) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $enterprise,
            array(
                'name',
                'unifiedSocialCreditCode',
                'logo',
                'businessLicense',
                'powerAttorney',
                'legalPerson',
                'establishDate',
                'capital',
                'contactsName',
                'contactsCellphone'
            )
        );

        $this->patch(
            $this->getResource().'/'.$enterprise->getId().'/save',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($enterprise);
            return true;
        }

        return false;
    }
}
