<?php
namespace Sdk\Enterprise\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;
use Sdk\Common\Repository\ResubmitAbleRepositoryTrait;
use Sdk\Common\Repository\ApplyAbleRepositoryTrait;

use Sdk\Enterprise\Adapter\Enterprise\IEnterpriseAdapter;
use Sdk\Enterprise\Adapter\Enterprise\EnterpriseMockAdapter;
use Sdk\Enterprise\Adapter\Enterprise\EnterpriseRestfulAdapter;
use Sdk\Enterprise\Model\Enterprise;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class EnterpriseRepository extends Repository implements IEnterpriseAdapter
{
    use AsyncRepositoryTrait,
        FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        ErrorRepositoryTrait,
        ResubmitAbleRepositoryTrait,
        ApplyAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'ENTERPRISE_LIST';
    const FETCH_ONE_MODEL_UN = 'ENTERPRISE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new EnterpriseRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : IEnterpriseAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IEnterpriseAdapter
    {
        return new EnterpriseMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function save(Enterprise $enterprise) : bool
    {
        return $this->getAdapter()->save($enterprise);
    }
    
    public function editSave(Enterprise $enterprise) : bool
    {
        return $this->getAdapter()->editSave($enterprise);
    }
}
