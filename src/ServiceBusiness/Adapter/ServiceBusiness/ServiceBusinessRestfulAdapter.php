<?php
namespace Sdk\ServiceBusiness\Adapter\ServiceBusiness;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Marmot\Core;

use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;

use Sdk\ServiceBusiness\Translator\ServiceBusinessRestfulTranslator;
use Sdk\ServiceBusiness\Model\NullServiceBusiness;
use Sdk\ServiceBusiness\Model\ServiceBusiness;

class ServiceBusinessRestfulAdapter extends GuzzleAdapter implements IServiceBusinessAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,OperatAbleRestfulAdapterTrait, FetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'SERVICEBUSINESS_LIST'=>[
            'fields'=>[],
            'include'=>'enterprise'
        ],
        'SERVICEBUSINESS_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'organization,member,serviceOrder,enterprise'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new ServiceBusinessRestfulTranslator();
        $this->resource = 'reportRecords';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullServiceBusiness());
    }

    public function addAction(ServiceBusiness $serviceBusiness) : bool
    {
        unset($serviceBusiness);
        
        return false;
    }

    public function editAction(ServiceBusiness $serviceBusiness) : bool
    {
        $data = $this->getTranslator()->objectToArray($serviceBusiness, array('learnedProgress'));
        
        $this->patch($this->getResource().'/'.$serviceBusiness->getId().'/toLearn', $data);

        if ($this->isSuccess()) {
            $this->translateToObject($serviceBusiness);
            return true;
        }
    }

    public function getProof(ServiceBusiness $serviceBusiness) : bool
    {
        return $this->getProofAction($serviceBusiness);
    }

    protected function getProofAction(ServiceBusiness $serviceBusiness) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $serviceBusiness,
            array(
                'authLetter'
            )
        );

        $this->patch(
            $this->getResource().'/'.$serviceBusiness->getId().'/getProof',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($serviceBusiness);
            return true;
        }

        return false;
    }
}
