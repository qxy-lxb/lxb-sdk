<?php
namespace Sdk\ServiceBusiness\Adapter\ServiceBusiness;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\ServiceBusiness\Model\ServiceBusiness;

interface IServiceBusinessAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter
{
    public function getProof(ServiceBusiness $serviceBusiness);
}
