<?php
namespace Sdk\ServiceBusiness\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;

use Sdk\ServiceBusiness\Adapter\ServiceBusiness\ServiceBusinessRestfulAdapter;
use Sdk\ServiceBusiness\Model\ServiceBusiness;
use Sdk\ServiceBusiness\Adapter\ServiceBusiness\IServiceBusinessAdapter;
use Marmot\Core;

use Marmot\Framework\Classes\Repository;

class ServiceBusinessRepository extends Repository implements IServiceBusinessAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, OperatAbleRepositoryTrait;

    const LIST_MODEL_UN = 'SERVICEBUSINESS_LIST';
    const FETCH_ONE_MODEL_UN = 'SERVICEBUSINESS_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new ServiceBusinessRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : IServiceBusinessAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IServiceBusinessAdapter
    {
        return new ServiceBusinessMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function getProof(ServiceBusiness $serviceBusiness) : bool
    {
        return $this->getAdapter()->getProof($serviceBusiness);
    }
}
