<?php
namespace Sdk\ServiceBusiness\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;
use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;

use Sdk\ServiceBusiness\Model\ServiceBusiness;
use Sdk\ServiceBusiness\Model\NullServiceBusiness;
use Sdk\Member\Translator\MemberRestfulTranslator;
use Sdk\ServiceOrder\Translator\ServiceOrderRestfulTranslator;
use Sdk\Service\Translator\ServiceRestfulTranslator;

class ServiceBusinessRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getMemberRestfulTranslator() : MemberRestfulTranslator
    {
        return new MemberRestfulTranslator();
    }
    
    public function getServiceOrderRestfulTranslator() : ServiceOrderRestfulTranslator
    {
        return new ServiceOrderRestfulTranslator();
    }

    public function getEnterpriseRestfulTranslator() : EnterpriseRestfulTranslator
    {
        return new EnterpriseRestfulTranslator();
    }

    public function getServiceRestfulTranslator() : ServiceRestfulTranslator
    {
        return new ServiceRestfulTranslator();
    }

    public function arrayToObject(array $expression, $serviceBusiness = null)
    {
        if (empty($expression)) {
            return NullServiceBusiness::getInstance();
        }

        if ($serviceBusiness == null) {
            $serviceBusiness = new ServiceBusiness();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $serviceBusiness->setId($id);

        if (isset($data['attributes'])) {
            $attributes = $data['attributes'];

            if (isset($attributes['orderNumber'])) {
                $serviceBusiness->setOrderNumber($attributes['orderNumber']);
            }
            if (isset($attributes['status'])) {
                $serviceBusiness->setStatus($attributes['status']);
            }
            if (isset($attributes['createTime'])) {
                $serviceBusiness->setCreateTime($attributes['createTime']);
            }
            if (isset($attributes['updateTime'])) {
                $serviceBusiness->setUpdateTime($attributes['updateTime']);
            }
            if (isset($attributes['statusTime'])) {
                $serviceBusiness->setStatusTime($attributes['statusTime']);
            }
            if (isset($attributes['organizationName'])) {
                $serviceBusiness->getOrganization()->setName($attributes['organizationName']);
            }
            if (isset($attributes['orderPrice'])) {
                $serviceBusiness->getServiceOrder()->setPrice($attributes['orderPrice']);
            }
            if (isset($attributes['serviceContent'])) {
                $serviceBusiness->setCreditReport($attributes['serviceContent']);
            }
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);
            $serviceBusiness->setMember($this->getMemberRestfulTranslator()->arrayToObject($member));
        }
        if (isset($relationships['serviceOrder']['data'])) {
            $serviceOrder = $this->changeArrayFormat($relationships['serviceOrder']['data']);
            $serviceBusiness->setServiceOrder($this->getServiceOrderRestfulTranslator()->arrayToObject($serviceOrder));
        }
        if (isset($relationships['enterprise']['data'])) {
            $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);
            $serviceBusiness->setEnterprise($this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise));
        }
        if (isset($relationships['service']['data'])) {
            $service = $this->changeArrayFormat($relationships['service']['data']);
            $serviceBusiness->setService($this->getServiceRestfulTranslator()->arrayToObject($service));
        }
        return $serviceBusiness;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    private function translateToObject(array $expression, $serviceBusiness = null)
    {
        if (empty($expression)) {
            return NullServiceBusiness::getInstance();
        }

        if ($serviceBusiness == null) {
            $serviceBusiness = new ServiceBusiness();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $serviceBusiness->setId($id);

        if (isset($data['attributes'])) {
            $attributes = $data['attributes'];

            if (isset($attributes['orderNumber'])) {
                $serviceBusiness->setOrderNumber($attributes['orderNumber']);
            }
            if (isset($attributes['status'])) {
                $serviceBusiness->setStatus($attributes['status']);
            }
            if (isset($attributes['createTime'])) {
                $serviceBusiness->setCreateTime($attributes['createTime']);
            }
            if (isset($attributes['updateTime'])) {
                $serviceBusiness->setUpdateTime($attributes['updateTime']);
            }
            if (isset($attributes['statusTime'])) {
                $serviceBusiness->setStatusTime($attributes['statusTime']);
            }
            if (isset($attributes['organizationName'])) {
                $serviceBusiness->getOrganization()->setName($attributes['organizationName']);
            }
            if (isset($attributes['enterpriseName'])) {
                $serviceBusiness->getEnterprise()->setName($attributes['enterpriseName']);
            }
            if (isset($attributes['userName'])) {
                $serviceBusiness->getMember()->setCellphone($attributes['userName']);
            }
            if (isset($attributes['orderPrice'])) {
                $serviceBusiness->getServiceOrder()->setPrice($attributes['orderPrice']);
            }
            if (isset($attributes['serviceName'])) {
                $serviceBusiness->getService()->setReportName($attributes['serviceName']);
            }
            if (isset($attributes['thumbnail'])) {
                $serviceBusiness->getService()->setThumbnail($attributes['thumbnail']);
            }
            if (isset($attributes['serviceContent'])) {
                $serviceBusiness->setCreditReport($attributes['serviceContent']);
            }
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        return $serviceBusiness;
    }

    public function objectToArray($serviceBusiness, array $keys = array())
    {
        if (!$serviceBusiness instanceof ServiceBusiness) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'learnedProgress',
                'authLetter'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'serviceBusiness'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $serviceBusiness->getId();
        }

        if (in_array('learnedProgress', $keys)) {
            $expression['data']['attributes']['learnedProgress'] = $serviceBusiness->getLearnedProgress();
        }
        if (in_array('authLetter', $keys)) {
            $expression['data']['attributes']['authLetter'] = $serviceBusiness->getAuthLetter();
        }
        return $expression;
    }
}
