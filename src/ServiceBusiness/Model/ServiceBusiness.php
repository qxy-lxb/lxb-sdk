<?php
namespace Sdk\ServiceBusiness\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;

use Sdk\ServiceOrder\Model\ServiceOrder;
use Sdk\Organization\Model\Organization;
use Sdk\Member\Model\Member;
use Sdk\Enterprise\Model\Enterprise;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\ServiceBusiness\Repository\ServiceBusinessRepository;
use Sdk\Service\Model\Service;

class ServiceBusiness implements IOperatAble, IObject
{
    use OperatAbleTrait, Object;

    private $id;
    
    private $member;

    private $orderNumber;

    private $serviceOrder;

    private $organization;

    private $snapshot;

    private $repository;

    private $enterprise;

    private $service;

    private $creditReport;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->member = Core::$container->has('user') ? Core::$container->get('user') : new Member();
        $this->orderNumber = '';
        $this->serviceOrder = new ServiceOrder();
        $this->organization = new Organization();
        $this->status = 0;
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->snapshot = '';
        $this->enterprise = new Enterprise();
        $this->service = new Service();
        $this->creditReport = array();
        $this->repository = new ServiceBusinessRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->member);
        unset($this->orderNumber);
        unset($this->serviceOrder);
        unset($this->organization);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->snapshot);
        unset($this->enterprise);
        unset($this->service);
        unset($this->creditReport);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setOrderNumber(string $orderNumber) : void
    {
        $this->orderNumber = $orderNumber;
    }

    public function getOrderNumber() : string
    {
        return $this->orderNumber;
    }

    public function setServiceOrder(ServiceOrder $serviceOrder) : void
    {
        $this->serviceOrder = $serviceOrder;
    }

    public function getServiceOrder() : ServiceOrder
    {
        return $this->serviceOrder;
    }

    public function setOrganization(Organization $organization) : void
    {
        $this->organization = $organization;
    }

    public function getOrganization() : Organization
    {
        return $this->organization;
    }
    
    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
    
    public function setSnapshot(string $snapshot) : void
    {
        $this->snapshot = $snapshot;
    }

    public function getSnapshot() : string
    {
        return $this->snapshot;
    }

    public function setEnterprise(Enterprise $enterprise) : void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise() : Enterprise
    {
        return $this->enterprise;
    }
    
    public function getService() : Service
    {
        return $this->service;
    }

    public function setService(Service $service) : void
    {
        $this->service = $service;
    }

    public function setCreditReport(array $creditReport) : void
    {
        $this->creditReport = $creditReport;
    }

    public function getCreditReport() : array
    {
        return $this->creditReport;
    }

    protected function getRepository() : ServiceBusinessRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    /**
     * 获取证明-模版提交
     * @return
     */
    public function getProof() : bool
    {
        return $this->getRepository()->getProof($this);
    }
}
