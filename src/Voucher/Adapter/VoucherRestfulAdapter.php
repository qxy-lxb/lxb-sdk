<?php
namespace Sdk\Voucher\Adapter;

use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Framework\Adapter\Restful\GuzzleAdapter;

use Sdk\Voucher\Model\Voucher;
use Sdk\Voucher\Model\NullVoucher;
use Sdk\Voucher\Translator\VoucherRestfulTranslator;

use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;

class VoucherRestfulAdapter extends GuzzleAdapter implements IVoucherAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
            'VOUCHER_LIST'=>[
                'fields'=>[
                    'crews'=>'realName,cellphone',
                ],
                'include'=> 'crews'
            ],
            'VOUCHER_FETCH_ONE'=>[
                'fields'=>[],
                'include'=> 'crews'
            ]
        ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new VoucherRestfulTranslator();
        $this->resource = 'vouchers';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            3001 => PARAMETER_FORMAT_INCORRECT,
            3002 => VOUCHER_FORMAT_ERROR,
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, NullVoucher::getInstance());
    }

    protected function addAction(Voucher $voucher) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $voucher,
            array(
                'category',
                'voucher',
                'status',
                'crew'
            )
        );

        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($voucher);
            return true;
        }

        return false;
    }

    protected function editAction(Label $label) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $label,
            array(
                'name',
                'icon',
                'category',
                'remark',
            )
        );

        $this->patch(
            $this->getResource().'/'.$label->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($label);
            return true;
        }

        return false;
    }
}
