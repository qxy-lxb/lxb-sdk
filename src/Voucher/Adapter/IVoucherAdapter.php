<?php
namespace Sdk\Voucher\Adapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IEnableAbleAdapter;

interface IVoucherAdapter extends IFetchAbleAdapter, IOperatAbleAdapter, IEnableAbleAdapter
{
}
