<?php
namespace Sdk\Voucher\Translator;

use Sdk\Voucher\Model\Voucher;
use Sdk\Voucher\Model\NullVoucher;
use Sdk\Common\Translator\RestfulTranslatorTrait;
use Marmot\Core;

use Sdk\Crew\Translator\CrewRestfulTranslator;

use Marmot\Interfaces\IRestfulTranslator;

class VoucherRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $voucher = null)
    {
        return $this->translateToObject($expression, $voucher);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $voucher = null)
    {
        if (empty($expression)) {
            return NullVoucher::getInstance();
        }

        if ($voucher == null) {
            $voucher = new Voucher();
        }
        
        $data =  $expression['data'];

        $id = $data['id'];

        $voucher->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['category'])) {
            $voucher->setCategory($attributes['category']);
        }
        if (isset($attributes['template'])) {
            $voucher->setVoucher($attributes['template']);
        }
        if (isset($attributes['createTime'])) {
            $voucher->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $voucher->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $voucher->setStatus($attributes['status']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);

            $voucher->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        return $voucher;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($voucher, array $keys = array())
    {
        $expression = array();

        if (!$voucher instanceof Voucher) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'type',
                'voucher',
                'status',
                'crew'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'vouchers'
            )
        );

        $attributes = array();

        if (in_array('category', $keys)) {
            $attributes['category'] = $voucher->getCategory();
        }
        if (in_array('voucher', $keys)) {
            $attributes['template'] = $voucher->getVoucher();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => Core::$container->get('crew')->getId()
                )
             );
        }

        return $expression;
    }
}
