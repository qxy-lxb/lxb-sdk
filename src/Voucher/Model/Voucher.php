<?php
namespace Sdk\Voucher\Model;

use Marmot\Core;
use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Model\IEnableAble;
use Sdk\Common\Model\EnableAbleTrait;
use Sdk\Common\Adapter\IEnableAbleAdapter;

use Sdk\Crew\Model\Crew;

use Sdk\Voucher\Repository\VoucherRepository;

class Voucher implements IObject, IOperatAble, IEnableAble
{
    use Object, OperatAbleTrait, EnableAbleTrait;

    const CATEGORY_VOUCHER = array(
        'LETTER_OF_AUTHORIZATION' => 1, //企业授权函
        'LETTER_OF_COMMITMENT' => 2, //信用承诺书
        'POWER_OF_ATTORNEY_FOR_CREDIT_TRAINING' => 3, //信用培训授权委托书
    );
    /**
     * [$id 主键Id]
     * @var [int]
     */
    private $id;
    /**
     * [$name 标签名]
     * @var [string]
     */
    private $name;
    /**
     * [$voucher 凭证]
     * @var [array]
     */
    private $voucher;
    /**
     * [$category 分类]
     * @var [int]
     */
    private $category;
    /**
     * [$crew 操作人员]
     * @var [Crew]
     */
    private $crew;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->name = '';
        $this->voucher = array();
        $this->category = self::CATEGORY_VOUCHER['LETTER_OF_AUTHORIZATION'];
        $this->crew = new Crew();
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->repository = new VoucherRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->voucher);
        unset($this->category);
        unset($this->crew);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setVoucher(array $voucher) : void
    {
        $this->voucher = $voucher;
    }

    public function getVoucher() : array
    {
        return $this->voucher;
    }

    public function setCategory(int $category) : void
    {
        $this->category = in_array(
            $category,
            self::CATEGORY_VOUCHER
        ) ? $category : self::CATEGORY_VOUCHER['LETTER_OF_AUTHORIZATION'];
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function getStatus() : int
    {
        return $this->status;
    }

    protected function getRepository() : VoucherRepository
    {
        return $this->repository;
    }
    /**
     * 新增编辑
     * @return [bool]
     */
    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }
    /**
     * 启用禁用
     * @return [bool]
     */
    protected function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return $this->getRepository();
    }
}
