<?php
namespace Sdk\Voucher\Repository;

use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\ErrorRepositoryTrait;
use Sdk\Common\Repository\EnableAbleRepositoryTrait;

use Sdk\Voucher\Adapter\IVoucherAdapter;
use Sdk\Voucher\Adapter\VoucherMockAdapter;
use Sdk\Voucher\Adapter\VoucherRestfulAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class VoucherRepository extends Repository implements IVoucherAdapter
{
    use FetchRepositoryTrait,
        OperatAbleRepositoryTrait,
        EnableAbleRepositoryTrait,
        ErrorRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'VOUCHER_LIST';
    const FETCH_ONE_MODEL_UN = 'VOUCHER_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new VoucherRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey') : []
        );
    }

    public function getActualAdapter() : IVoucherAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IVoucherAdapter
    {
        return new VoucherMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
