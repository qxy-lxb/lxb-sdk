<?php
namespace Sdk\Service\Adapter\Service;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\ShelveAbleRestfulAdapterTrait;

use Sdk\Service\Translator\ServiceRestfulTranslator;
use Sdk\Service\Model\NullService;
use Sdk\Service\Model\Service;
use Marmot\Interfaces\INull;

use Marmot\Core;

class ServiceRestfulAdapter extends GuzzleAdapter implements IServiceAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait,
        ShelveAbleRestfulAdapterTrait,
        ApplyAbleRestfulAdapterTrait;

    private $translator;
    private $resource;

    const SCENARIOS = [
        'SERVICE_LIST'=>[
            'fields'=>[
                    'organization' => 'name,contacts,phone'
                ],
            'include'=> 'organization'
        ],
        'SERVICE_FETCH_ONE'=>[
            'fields'=>[
                    'organization' => 'name,contacts,phone,logo,url'
                ],
            'include'=> 'organization'
        ]
    ];
    
    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new ServiceRestfulTranslator();
        $this->resource = 'services';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            60001 => SERVICE_REPORTNAME_FORMAT_ERROR,
            60002 => SERVICE_PRICE_FORMAT_ERROR,
            60003 => SERVICE_ISAGREEMENT_FORMAT_ERROR,
            60004 => SERVICE_ATTACHMENTS_FORMAT_ERROR,
            60005 => NEED_SIGNIN,
            60006 => SERVICE_STATUS_ALREADY_ON,
            60007 => SERVICE_STATUS_ALREADY_OFF,
            60008 => SERVICE_OFF_REASON_FORMAT_ERROR,
            30016 => CREW_NOT_HAVE_PUBLISHING_PERMISSION
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullService());
    }

    public function addAction(Service $service) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $service,
            array('isBear', 'applePrice','androidPrice', 'institutionalPrice','platformPrice','isAgreement','attachments','reportName','homeImage','thumbnail','crew')
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($service);
            return true;
        }

        return false;
    }

    public function editAction(Service $service) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $service,
            array('isBear', 'applePrice','androidPrice','institutionalPrice','platformPrice','isAgreement','attachments','reportName','homeImage','thumbnail')
        );
        
        $this->patch(
            $this->getResource().'/'.$service->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($service);
            return true;
        }

        return false;
    }
    /**
     * [forceOffShelve 强制下架]
     * @param service $service [课程对象]
     * @return [bool]                [返回类型]
     */
    public function forceOffShelve(Service $service) : bool
    {
        return $this->forceOffShelveAction($service);
    }
    protected function forceOffShelveAction(Service $service) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $service,
            array(
                'offReason'
            )
        );

        $this->patch(
            $this->getResource().'/'.$service->getId().'/forceOffShelve',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($service);
            return true;
        }

        return false;
    }
    /**
     * [snapshot 课程快照]
     * @param $snapshot [课程快照]
     */
    public function snapshot($snapshot)
    {
        return $this->snapshotAction($snapshot);
    }
    protected function snapshotAction($snapshot)
    {
        $this->get(
            $this->getResource().'/'.$snapshot
        );

        return $this->isSuccess() ? $this->translateToObject() : array();
    }

    public function fetchOneIncludeServiceContent($id, int $isApplicableEnterprise, int $enterpriseId)
    {
        return $this->fetchOneIncludeServiceContentAction($id, $isApplicableEnterprise, new NullService(), $enterpriseId);
    }

    protected function fetchOneIncludeServiceContentAction($id, int $isApplicableEnterprise, INull $null, int $enterpriseId)
    {
        $this->get(
            $this->getResource().'/'.$id.'/'.$isApplicableEnterprise.'/'.$enterpriseId
        );

        return $this->isSuccess() ? $this->translateToObject() : $null;
    }
}
