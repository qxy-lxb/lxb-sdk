<?php
namespace Sdk\Service\Adapter\Service;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IApplyAbleAdapter;
use Sdk\Common\Adapter\IShelveAbleAdapter;
use Sdk\Service\Model\Service;

use Marmot\Interfaces\IAsyncAdapter;

interface IServiceAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter, IApplyAbleAdapter, IShelveAbleAdapter
{
    public function forceOffShelve(Service $Service);
    public function snapshot($snapshot);

    public function fetchOneIncludeServiceContent($id, int $isApplicableEnterprise, int $enterpriseId);
}
