<?php
namespace Sdk\Service\Translator;

use Sdk\Common\Translator\RestfulTranslatorTrait;
use Marmot\Core;

use Sdk\Service\Model\Service;
use Sdk\Service\Model\NullService;
use Sdk\video\Translator\VideoRestfulTranslator;

use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Organization\Repository\OrganizationRepository;
use Sdk\Service\Model\ServiceContent;

class ServiceRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getVideoRestfulTranslator()
    {
        return new VideoRestfulTranslator();
    }

    public function arrayToObject(array $expression, $service = null)
    {
        return $this->translateToObject($expression, $service);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $service = null)
    {
        if (empty($expression)) {
            return new NullService();
        }

        if ($service == null) {
            $service = new Service();
        }

        $data =  $expression['data'];

        $id = $data['id'];
        $service->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';
        
        if (isset($attributes['isBear'])) {
            $service->setIsBear($attributes['isBear']);
        }
        if (isset($attributes['applePrice'])) {
            $service->setApplePrice($attributes['applePrice']);
        }
        if (isset($attributes['androidPrice'])) {
            $service->setAndroidPrice($attributes['androidPrice']);
        }
        if (isset($attributes['institutionalPrice'])) {
            $service->setInstitutionalPrice($attributes['institutionalPrice']);
        }
        if (isset($attributes['platformPrice'])) {
            $service->setPlatformPrice($attributes['platformPrice']);
        }
        if (isset($attributes['isAgreement'])) {
            $service->setIsAgreement($attributes['isAgreement']);
        }
        if (isset($attributes['attachment'])) {
            $service->setAttachments($attributes['attachment']);
        }
        if (isset($attributes['thumbnail'])) {
            $service->setThumbnail($attributes['thumbnail']);
        }
        if (isset($attributes['homeImage'])) {
            $service->setHomeImage($attributes['homeImage']);
        }
        if (isset($attributes['name'])) {
            $service->setReportName($attributes['name']);
        }
        if (isset($attributes['offReason'])) {
            $service->setOffReason($attributes['offReason']);
        }
        if (isset($attributes['applyStatus'])) {
            $service->setApplyStatus($attributes['applyStatus']);
        }
        if (isset($attributes['status'])) {
            $service->setStatus($attributes['status']);
        }
        if (isset($attributes['rejectReason'])) {
            $service->setRejectReason($attributes['rejectReason']);
        }
        if (isset($attributes['createTime'])) {
            $service->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $service->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['purchaseVolume'])) {
            $service->setPurchaseVolume($attributes['purchaseVolume']);
        }
        if (isset($attributes['isApplicableEnterprise'])) {
            $service->setIsApplicableEnterprise($attributes['isApplicableEnterprise']);
        }
        $serviceContent = isset($attributes['serviceContent']['creditRating'])
        ? $attributes['serviceContent']['creditRating']
        : '';
        $thumbnail = isset($attributes['serviceContent']['thumbnail'])
        ? $attributes['serviceContent']['thumbnail']
        : '';
        $electronicReport = isset($attributes['serviceContent']['electronicReport'])
        ? $attributes['serviceContent']['electronicReport']
        : '';
        $service->setServiceContent(
            new ServiceContent(
                $serviceContent,
                $thumbnail,
                $electronicReport
            )
        );
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        if (isset($relationships['organization']['data'])) {
            $organization = $this->getOrganizationRepository()->fetchOne($relationships['organization']['data']['id']);
             $service->setOrganization($organization);
        }
        
        return $service;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($service, array $keys = array())
    {
        if (!$service instanceof Service) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'isBear',
                'applePrice',
                'androidPrice',
                'institutionalPrice',
                'platformPrice',
                'isAgreement',
                'attachments',
                'thumbnail',
                'homeImage',
                'reportName',
                'crew',
                'rejectReason',
                'offReason'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'services'
            )
        );
        
        if (in_array('id', $keys)) {
            $expression['data']['id'] = $service->getId();
        }
        
        $attributes = array();

        if (in_array('isBear', $keys)) {
            $attributes['isBear'] = $service->getIsBear();
        }
        if (in_array('applePrice', $keys)) {
            $attributes['applePrice'] = $service->getApplePrice();
        }
        if (in_array('androidPrice', $keys)) {
            $attributes['androidPrice'] = $service->getAndroidPrice();
        }
        if (in_array('institutionalPrice', $keys)) {
            $attributes['institutionalPrice'] = $service->getInstitutionalPrice();
        }
        if (in_array('platformPrice', $keys)) {
            $attributes['platformPrice'] = $service->getPlatformPrice();
        }
        if (in_array('isAgreement', $keys)) {
            $attributes['isAgreement'] = $service->getIsAgreement();
        }
        if (in_array('attachments', $keys)) {
            $attributes['attachment'] = $service->getAttachments();
        }
        if (in_array('thumbnail', $keys)) {
            $attributes['thumbnail'] = $service->getThumbnail();
        }
        if (in_array('homeImage', $keys)) {
            $attributes['homeImage'] = $service->getHomeImage();
        }
        if (in_array('reportName', $keys)) {
            $attributes['name'] = $service->getReportName();
        }
        if (in_array('price', $keys)) {
            $attributes['price'] = $service->getPrice();
        }
        if (in_array('rejectReason', $keys)) {
            $attributes['rejectReason'] = $service->getRejectReason();
        }
        if (in_array('offReason', $keys)) {
            $attributes['offReason'] = $service->getOffReason();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $service->getCrew()->getId()
                )
            );
        }

        return $expression;
    }

    protected function getOrganizationRepository()
    {
        return new OrganizationRepository();
    }
}
