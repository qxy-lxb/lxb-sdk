<?php
namespace Sdk\Service\Model;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Adapter\IEnableAbleAdapter;
use Sdk\Common\Adapter\ITopAbleAdapter;
use Sdk\Common\Model\IEnableAble;
use Sdk\Common\Model\EnableAbleTrait;
use Sdk\Common\Model\IShelveAble;

use Sdk\Crew\Model\Crew;
use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Model\ApplyAbleTrait;
use Sdk\Common\Adapter\IApplyAbleAdapter;
use Sdk\Common\Model\ShelveAbleTrait;
use Sdk\Common\Adapter\IShelveAbleAdapter;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Marmot\Core;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Organization\Model\Organization;

use Sdk\Service\Repository\ServiceRepository;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Service implements IOperatAble, IObject, IApplyAble, IShelveAble
{
    use OperatAbleTrait, Object, ApplyAbleTrait, ShelveAbleTrait;

    /**
     * @var IS_AGREEMENT['是']  需要用户协议 2
     * @var IS_AGREEMENT['否']  不需用户协议 -2
     */
    const IS_AGREEMENT = array(
        '是' => 2,
        '否' => -2
    );

    const IS_BEAR = array(
        '是' => 1,
        '否' => 2
    );

    private $id;
    private $isBear;
    private $applePrice;
    private $androidPrice;
    private $institutionalPrice;
    private $platformPrice;
    private $isAgreement;
    private $attachments;
    private $thumbnail;
    private $homeImage;
    private $reportName;
    private $crew;
    private $organization;
    private $offReason;
    private $rejectReason;
    private $snapshot;
    private $purchaseVolume;
    private $isApplicableEnterprise;
    private $serviceContent;
    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->isBear = self::IS_BEAR['否'];
        $this->applePrice = 0;
        $this->androidPrice = 0;
        $this->institutionalPrice = 0;
        $this->platformPrice = 0;
        $this->applyStatus = self::APPLY_STATUS['NOT_SUBMITTED'];
        $this->isAgreement = self::IS_AGREEMENT['否'];
        $this->attachments = array();
        $this->thumbnail = array();
        $this->homeImage = array();
        $this->reportName = '';
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->organization = new Organization();
        $this->offReason = '';
        $this->rejectReason = '';
        $this->snapshot = '';
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->purchaseVolume = 0;
        $this->isApplicableEnterprise = 0;
        $this->serviceContent = new ServiceContent();
        $this->repository = new ServiceRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->isBear);
        unset($this->applePrice);
        unset($this->androidPrice);
        unset($this->institutionalPrice);
        unset($this->platformPrice);
        unset($this->isAgreement);
        unset($this->attachments);
        unset($this->thumbnail);
        unset($this->applyStatus);
        unset($this->homeImage);
        unset($this->reportName);
        unset($this->crew);
        unset($this->organization);
        unset($this->offReason);
        unset($this->rejectReason);
        unset($this->snapshot);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->purchaseVolume);
        unset($this->isApplicableEnterprise);
        unset($this->serviceContent);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setIsBear($isBear) : void
    {
        $this->isBear = $isBear;
    }

    public function getIsBear() : int
    {
        return $this->isBear;
    }

    public function setApplePrice(int $applePrice) : void
    {
        $this->applePrice = $applePrice;
    }

    public function getApplePrice() : int
    {
        return $this->applePrice;
    }

    public function setAndroidPrice(int $androidPrice) : void
    {
        $this->androidPrice = $androidPrice;
    }

    public function getAndroidPrice() : int
    {
        return $this->androidPrice;
    }

    public function setInstitutionalPrice(string $institutionalPrice) : void
    {
        $this->institutionalPrice = $institutionalPrice;
    }

    public function getInstitutionalPrice() : string
    {
        return $this->institutionalPrice;
    }

    public function setPlatformPrice(string $platformPrice) : void
    {
        $this->platformPrice = $platformPrice;
    }

    public function getPlatformPrice() : string
    {
        return $this->platformPrice;
    }

    public function setIsAgreement(int $isAgreement) : void
    {
        $this->isAgreement = in_array(
            $isAgreement,
            self::IS_AGREEMENT
        ) ? $isAgreement : self::IS_AGREEMENT['否'];
    }

    public function getIsAgreement() : int
    {
        return $this->isAgreement;
    }

    public function setAttachments(array $attachments) : void
    {
        $this->attachments = $attachments;
    }

    public function getAttachments() : array
    {
        return $this->attachments;
    }

    public function setThumbnail(array $thumbnail) : void
    {
        $this->thumbnail = $thumbnail;
    }

    public function getThumbnail() : array
    {
        return $this->thumbnail;
    }

    public function setHomeImage(array $homeImage) : void
    {
        $this->homeImage = $homeImage;
    }

    public function getHomeImage() : array
    {
        return $this->homeImage;
    }

    public function setReportName(string $reportName) : void
    {
        $this->reportName = $reportName;
    }

    public function getReportName() : string
    {
        return $this->reportName;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setOrganization(Organization $organization) : void
    {
        $this->organization = $organization;
    }

    public function getOrganization() : Organization
    {
        return $this->organization;
    }

    public function setOffReason(string $offReason) : void
    {
        $this->offReason = $offReason;
    }

    public function getOffReason() : string
    {
        return $this->offReason;
    }
    
    public function setRejectReason(string $rejectReason) : void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason() : string
    {
        return $this->rejectReason;
    }

    public function setSnapshot(string $snapshot) : void
    {
        $this->snapshot = $snapshot;
    }

    public function getSnapshot() : string
    {
        return $this->snapshot;
    }
    
    public function setApplyStatus(int $applyStatus) : void
    {
        $this->applyStatus = $applyStatus;
    }

    public function getApplyStatus() : int
    {
        return $this->applyStatus;
    }

    protected function getRepository() : ServiceRepository
    {
        return $this->repository;
    }

    protected function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIShelveAbleAdapter() : IShelveAbleAdapter
    {
        return $this->getRepository();
    }
    /**
     * 强制下架
     * @return bool 强制下架
     */
    public function forceOffShelve() : bool
    {
        return $this->getRepository()->forceOffShelve($this);
    }

    public function setPurchaseVolume(int $purchaseVolume) : void
    {
        $this->purchaseVolume = $purchaseVolume;
    }

    public function getPurchaseVolume() : int
    {
        return $this->purchaseVolume;
    }

    public function setIsApplicableEnterprise(int $isApplicableEnterprise) : void
    {
        $this->isApplicableEnterprise = $isApplicableEnterprise;
    }

    public function getIsApplicableEnterprise() : int
    {
        return $this->isApplicableEnterprise;
    }

    public function setServiceContent(ServiceContent $serviceContent) : void
    {
        $this->serviceContent = $serviceContent;
    }

    public function getServiceContent() : ServiceContent
    {
        return $this->serviceContent;
    }
}
