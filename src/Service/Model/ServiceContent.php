<?php
namespace Sdk\Service\Model;

class ServiceContent
{

    /**
     * [$creditRating 信用等级]
     * @var [string]
    */
    private $creditRating;
    /**
     * [$thumbnail 报告缩略图]
     * @var [string]
    */
    private $thumbnail;
    /**
     * [$electronicReport 报告电子版]
     * @var [string]
    */
    private $electronicReport;
    
    public function __construct(
        string $creditRating = '',
        string $thumbnail = '',
        string $electronicReport = ''
    ) {
        $this->creditRating = $creditRating;
        $this->thumbnail = $thumbnail;
        $this->electronicReport = $electronicReport;
    }

    public function __destruct()
    {
        unset($this->creditRating);
        unset($this->thumbnail);
        unset($this->electronicReport);
    }

    public function getCreditRating() : string
    {
        return $this->creditRating;
    }

    public function getThumbnail() : string
    {
        return $this->thumbnail;
    }

    public function getElectronicReport() : string
    {
        return $this->electronicReport;
    }
}
