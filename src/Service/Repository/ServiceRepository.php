<?php
namespace Sdk\Service\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\ShelveAbleRepositoryTrait;

use Sdk\Service\Adapter\Service\IServiceAdapter;
use Sdk\Service\Adapter\Service\ServiceRestfulAdapter;
use Sdk\Service\Adapter\Service\NewMockAdapter;
use Sdk\Service\Model\Service;
use Sdk\Common\Repository\ApplyAbleRepositoryTrait;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class ServiceRepository extends Repository implements IServiceAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, OperatAbleRepositoryTrait,ApplyAbleRepositoryTrait,ShelveAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'SERVICE_LIST';
    const FETCH_ONE_MODEL_UN = 'SERVICE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new ServiceRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : IServiceAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IServiceAdapter
    {
        return new NewMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function forceOffShelve(Service $Service) : bool
    {
        return $this->getAdapter()->forceOffShelve($Service);
    }
    
    public function snapshot($snapshot)
    {
        return $this->getAdapter()->snapshot($snapshot);
    }

    public function fetchOneIncludeServiceContent($id, int $isApplicableEnterprise, int $enterpriseId)
    {
        return $this->getAdapter()->fetchOneIncludeServiceContent($id, $isApplicableEnterprise, $enterpriseId);
    }
}
