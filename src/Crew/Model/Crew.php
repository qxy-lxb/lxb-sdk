<?php
namespace Sdk\Crew\Model;

use Sdk\Crew\Repository\CrewRepository;
use Sdk\User\Model\User;
use Sdk\Organization\Model\Organization;

use Sdk\Common\Adapter\IOperatAbleRepository;

class Crew extends User
{
    /**
     * [$repository]
     * @var [Object]
     */
    private $repository;
    private $roles;
    private $organization;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->roles = array();
        $this->organization = new Organization();
        $this->repository = new CrewRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->organization);
        unset($this->roles);
        unset($this->repository);
    }
    
    protected function getRepository() : CrewRepository
    {
        return $this->repository;
    }

    public function addRole(Role $role) : void
    {
        $this->roles[] = $role;
    }

    public function clearRoles() : void
    {
        $this->roles = [];
    }

    public function getRoles() : array
    {
        return $this->roles;
    }

    public function setOrganization(Organization $organization) : void
    {
        $this->organization = $organization;
    }

    public function getOrganization() : Organization
    {
        return $this->organization;
    }
}
