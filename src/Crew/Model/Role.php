<?php
namespace Sdk\Crew\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;

class Role
{
    use Object;

    const STATUS_NORMAL = 0;
    const STATUS_DELETE = -2;

    const ADMINISTRATION = 1;
    const REPAIR = 2;
    const PLATFORM = 3;
    const PRESENTATION = 4;
    const OTHER= 5;
    const ROLES_NULL = 0;

    //角色暂时不存储在数据库, 这里使用常量来定义角色
    const ROLES = array(
        self::ROLES_NULL=>'未知管理员',
        self::PLATFORM=>'平台管理员',
        self::PRESENTATION=>'行政机构管理员',
        self::ADMINISTRATION=>'信用报告服务机构管理员',
        self::REPAIR=>'信用修复培训机构管理员',
        self::OTHER=>'社会团体或其他机构管理员'
    );

    private $id;

    private $name;

    public function __construct(int $id = 0)
    {
        $this->id = isset(self::ROLES[$id]) ? $id : 0;
        $this->name = isset(self::ROLES[$id]) ? self::ROLES[$id] : '';
        $this->createTime = Core::$container->get('time');
        $this->status = self::STATUS_NORMAL;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->createTime);
        unset($this->status);
    }

    public function setId(int $id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setStatus(int $status) : void
    {
        $this->status= in_array(
            $status,
            array(
                self::STATUS_NORMAL,
                self::STATUS_DELETE
            )
        ) ? $status : self::STATUS_NORMAL;
    }

    public function isPlatform() : bool
    {
        return $this->getId() == self::PLATFORM;
    }

    public function isAdministration() : bool
    {
        return $this->getId() == self::ADMINISTRATION;
    }

    public function isRepair() : bool
    {
        return $this->getId() == self::REPAIR;
    }

    public function isPresentation() : bool
    {
        return $this->getId() == self::PRESENTATION;
    }
}
