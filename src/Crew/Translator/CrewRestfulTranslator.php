<?php
namespace Sdk\Crew\Translator;

use Sdk\Crew\Model\Crew;
use Sdk\Crew\Model\NullCrew;
use Sdk\Common\Translator\RestfulTranslatorTrait;
use Sdk\Organization\Translator\OrganizationRestfulTranslator;
use Sdk\Crew\Model\Role;

use Sdk\User\Translator\UserRestfulTranslator;

class CrewRestfulTranslator extends UserRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getOrganizationRestfulTranslator()
    {
        return new OrganizationRestfulTranslator();
    }

    public function arrayToObject(array $expression, $crew = null)
    {
        return $this->translateToObject($expression, $crew);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function translateToObject(array $expression, $crew = null)
    {
        if (empty($expression)) {
            return new NullCrew();
        }

        if ($crew == null) {
            $crew = new Crew();
        }

        $data =  $expression['data'];
   
        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        $crew = parent::translateToObject($expression, $crew);

        $data = $expression['data'];

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($relationships['role']['data'])) {
            if (!empty($relationships['role']['data'])) {
                    $role = new Role($relationships['role']['data']['id']);
                    $crew->addRole($role);
            }
        }

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        if (isset($relationships['organization']['data'])) {
            $organization = $this->changeArrayFormat($relationships['organization']['data']);

            $crew->setOrganization($this->getOrganizationRestfulTranslator()->arrayToObject($organization));
        }

        return $crew;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($crew, array $keys = array())
    {

        $user = parent::objectToArray($crew, $keys);

        if (!$crew instanceof Crew) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'role',
                'organization'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'crews',
            )
        );
        
        $expression['data']['attributes'] = $user['data']['attributes'];

        if (in_array('role', $keys)) {
            $role = array();

            if (!empty($crew->getRoles())) {
                foreach ($crew->getRoles() as $each) {
                    $role[] = array(
                        'type' => 'role',
                        'id' => $each->getId()
                    );
                }
            }

            $expression['data']['relationships']['role']['data'] = $role;
        }

        if (in_array('organization', $keys)) {
            $expression['data']['relationships']['organization']['data'] = array(
                array(
                    'type' => 'organizations',
                    'id' => $crew->getOrganization()->getId()
                )
            );
        }

        return $expression;
    }
}
