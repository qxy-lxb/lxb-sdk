<?php
namespace Sdk\Examination\Adapter\Examination;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IEnableAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface IExaminationAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter, IEnableAbleAdapter
{
}
