<?php
namespace Sdk\Examination\Adapter\Examination;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;

use Sdk\Examination\Translator\ExaminationRestfulTranslator;
use Sdk\Examination\Model\NullExamination;
use Sdk\Examination\Model\Examination;

use Marmot\Core;

class ExaminationRestfulAdapter extends GuzzleAdapter implements IExaminationAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        EnableAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    private $resource;

    const SCENARIOS = [
        'EXAMINATION_LIST'=>[
            'fields'=>[]
        ],
        'EXAMINATION_FETCH_ONE'=>[
            'fields'=>[]
        ]
    ];
    
    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new ExaminationRestfulTranslator();
        $this->resource = 'examinations';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            20001 => EXAMINATIONS_SUBJECT_FORMAT_ERROR,
            20002 => EXAMINATIONS_OPTIONS_FORMAT_ERROR,
            20003 => EXAMINATIONS_OPTIONS_TYPE_FORMAT_ERROR,
            20004 => EXAMINATIONS_ANSWERS_FORMAT_ERROR,
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullExamination());
    }

    public function addAction(Examination $examination) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $examination,
            array('subject', 'options', 'optionsType', 'answers', 'videoId')
        );
        
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($examination);
            return true;
        }

        return false;
    }

    public function editAction(Examination $examination) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $examination,
            array('subject', 'options', 'optionsType', 'answers')
        );

        $this->patch(
            $this->getResource().'/'.$examination->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($examination);
            return true;
        }

        return false;
    }
}
