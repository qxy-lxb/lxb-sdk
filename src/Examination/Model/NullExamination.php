<?php
namespace Sdk\Examination\Model;

use Marmot\Interfaces\INull;

use Marmot\Core;

class NullExamination extends Examination implements INull
{
    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function add() : bool
    {
        return $this->resourceNotExist();
    }

    public function edit() : bool
    {
        return $this->resourceNotExist();
    }

    public function move() : bool
    {
        return $this->resourceNotExist();
    }

    private function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
