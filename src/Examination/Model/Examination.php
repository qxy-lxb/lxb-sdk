<?php
namespace Sdk\Examination\Model;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Adapter\IEnableAbleAdapter;
use Sdk\Common\Adapter\ITopAbleAdapter;
use Sdk\Common\Model\IEnableAble;
use Sdk\Common\Model\EnableAbleTrait;

use Sdk\Crew\Model\Crew;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Marmot\Core;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Sdk\Examination\Repository\ExaminationRepository;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Examination implements IEnableAble, IOperatAble, IObject
{
    use EnableAbleTrait, OperatAbleTrait, Object;
    
    const OPTIONS_TYPE = array(
        'SINGLE_ELECTION' => 1,
        'MULTIPLE_SELECTION' => 2
    );

    private $id;
    
    private $subject;

    private $options;

    private $optionsType;

    private $answers;

    private $videoId;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->subject = '';
        $this->options = array();
        $this->optionsType = self::OPTIONS_TYPE['SINGLE_ELECTION'];
        $this->answers = array();
        $this->videoId = 0;
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->repository = new ExaminationRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->subject);
        unset($this->options);
        unset($this->answers);
        unset($this->videoId);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setSubject(string $subject) : void
    {
        $this->subject = $subject;
    }

    public function getSubject() : string
    {
        return $this->subject;
    }

    public function setOptions(array $options) : void
    {
        $this->options = $options;
    }

    public function getOptions() : array
    {
        return $this->options;
    }

    public function setAnswers(array $answers) : void
    {
        $this->answers = $answers;
    }

    public function getAnswers() : array
    {
        return $this->answers;
    }

    public function setVideoId($videoId) : void
    {
        $this->videoId = $videoId;
    }

    public function getVideoId() : int
    {
        return $this->videoId;
    }

    public function setOptionsType($optionsType) : void
    {
        $this->optionsType = $optionsType;
    }

    public function getOptionsType() : int
    {
        return $this->optionsType;
    }

    protected function getRepository() : ExaminationRepository
    {
        return $this->repository;
    }

    protected function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }
}
