<?php
namespace Sdk\Examination\Translator;

use Sdk\Common\Translator\RestfulTranslatorTrait;
use Marmot\Core;

use Sdk\Examination\Model\Examination;
use Sdk\Examination\Model\NullExamination;

use Marmot\Interfaces\IRestfulTranslator;

class ExaminationRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    public function arrayToObject(array $expression, $examination = null)
    {
        return $this->translateToObject($expression, $examination);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $examination = null)
    {
        if (empty($expression)) {
            return new NullExamination();
        }

        if ($examination == null) {
            $examination = new Examination();
        }

        $data =  $expression['data'];

        $id = $data['id'];
        $examination->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['subject'])) {
            $examination->setSubject($attributes['subject']);
        }
        if (isset($attributes['options'])) {
            $examination->setOptions($attributes['options']);
        }
        if (isset($attributes['optionsType'])) {
            $examination->setOptionsType($attributes['optionsType']);
        }
        if (isset($attributes['answers'])) {
            $examination->setAnswers($attributes['answers']);
        }
        if (isset($attributes['videoId'])) {
            $examination->setVideoId($attributes['videoId']);
        }
        if (isset($attributes['statusTime'])) {
            $examination->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['status'])) {
            $examination->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $examination->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $examination->setUpdateTime($attributes['updateTime']);
        }

        return $examination;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($examination, array $keys = array())
    {
        if (!$examination instanceof Examination) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'subject',
                'options',
                'optionsType',
                'answers',
                'videoId'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'examinations'
            )
        );
        
        if (in_array('id', $keys)) {
            $expression['data']['id'] = $examination->getId();
        }

        $attributes = array();

        if (in_array('subject', $keys)) {
            $attributes['subject'] = $examination->getSubject();
        }
        if (in_array('options', $keys)) {
            $attributes['options'] = $examination->getOptions();
        }
        if (in_array('optionsType', $keys)) {
            $attributes['optionsType'] = $examination->getOptionsType();
        }
        if (in_array('answers', $keys)) {
            $attributes['answers'] = $examination->getAnswers();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('videoId', $keys)) {
            $expression['data']['relationships']['video']['data'] = array(
                array(
                    'type' => 'videos',
                    'id' => $examination->getVideoId()
                )
             );
        }

        return $expression;
    }
}
