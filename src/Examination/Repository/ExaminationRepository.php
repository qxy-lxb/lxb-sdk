<?php
namespace Sdk\Examination\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\EnableAbleRepositoryTrait;

use Sdk\Examination\Adapter\Examination\IExaminationAdapter;
use Sdk\Examination\Adapter\Examination\ExaminationRestfulAdapter;
use Sdk\Examination\Adapter\Examination\NewMockAdapter;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class ExaminationRepository extends Repository implements IExaminationAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, OperatAbleRepositoryTrait, EnableAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'EXAMINATION_LIST';
    const FETCH_ONE_MODEL_UN = 'EXAMINATION_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new ExaminationRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : IExaminationAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : IExaminationAdapter
    {
        return new NewMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
