<?php
namespace Sdk\Log\Model;

use System\Classes\Server;

use Marmot\Core;

use Sdk\Crew\Model\Crew;
use Sdk\Crew\Model\Role;

class Log
{
    //日志操作标识
    private $operation;

    //日志类型标识
    private $category;

    //日志操作对象id
    private $recordObjId;

    //日志操作用户
    private $crew;

    //日志链id
    private $requestId;

    //日志操作时间
    private $date;

    private $message;

    //日志id
    private $id;

    public function __construct(
        int $operation = ILogable::OPERATION['OPERATION_NULL'],
        int $category = ILogable::CATEGORY['NULL'],
        int $recordObjId = 0,
        string $message = ''
    ) {
        $this->operation = $operation;
        $this->category = $category;
        $this->recordObjId = $recordObjId;
        $this->message = $message;
        $this->requestId = Server::get('REQUEST_ID');
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->date = '';
        $this->ip = '';
        $this->role = new Role();
        $this->id = '';
    }

    public function __destruct()
    {
        unset($this->operation);
        unset($this->category);
        unset($this->recordObjId);
        unset($this->requestId);
        unset($this->user);
        unset($this->date);
        unset($this->ip);
        unset($this->message);
        unset($this->role);
        unset($this->id);
    }

    public function setId(string $id)
    {
        $this->id = $id;
    }

    public function getId() : string
    {
        return $this->id;
    }

    public function getMessage() : string
    {
        return $this->message;
    }

    public function getOperation() : int
    {
        return $this->operation;
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function setCrew(Crew $crew)
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setRole(Role $role)
    {
        $this->role = $role;
    }

    public function getRole() : Role
    {
        return $this->role;
    }

    public function getRecordObjId() : int
    {
        return $this->recordObjId;
    }

    public function getRequestId() : string
    {
        return $this->requestId;
    }

    public function setDate(string $date)
    {
        $this->date = $date;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setIp(string $esIp)
    {
        $this->ip = $esIp;
    }

    public function getIp() : string
    {
        return $this->ip;
    }
}
