<?php
namespace Sdk\Log\Model;

use Monolog\Logger;

use Marmot\Interfaces\INull;
use System\Extension\Monolog\FluentdHandler;

use Sdk\Log\Model\ILogable;
use Sdk\Log\Translator\LogMonologTranslator;

use Marmot\Core;

class LogDriver
{
    const LOGGER_NAME = 'command';

    private $driver;

    private $translator;

    public function __construct()
    {
        $this->driver= new Logger(self::LOGGER_NAME);
        $this->driver->pushHandler(
            new FluentdHandler(
                Core::$container->get('fluentd.address'),
                Core::$container->get('fluentd.port'),
                Logger::INFO,
                Core::$container->get('log.info.tag')
            )
        );
        $this->driver->pushHandler(
            new FluentdHandler(
                Core::$container->get('fluentd.address'),
                Core::$container->get('fluentd.port'),
                Logger::ERROR,
                Core::$container->get('log.info.tag')
            )
        );

        $this->translator = new LogMonologTranslator();
    }

    protected function getDriver()
    {
        return $this->driver;
    }

    protected function getTranslator() : LogMonologTranslator
    {
        return $this->translator;
    }

    public function info($logable)
    {
        if ($logable instanceof ILogable) {
            $log = $logable->getLog();
            $this->getDriver()->info(
                $log->getMessage(),
                $this->getTranslator()->objectToArray($log)
            );
        }
    }

    public function error($logable)
    {
        if ($logable instanceof ILogable) {
            $log = $logable->getLog();
            $this->getDriver()->error(
                $log->getMessage(),
                $this->getTranslator()->objectToArray($log)
            );
        }
    }
}
