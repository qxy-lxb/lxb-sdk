<?php
namespace Sdk\Log\Model;

use Marmot\Interfaces\ICommand;

interface ILogable
{
    const CATEGORY = array(
        'NULL' => 0,
        'BANNER' => 1,
        'COURSE' => 2,
        'CREW' => 3,
        'ENTERPRISE' => 4,
        'EXAMINATION' => 5,
        'NEWS' => 6,
        'ORGANIZATION' => 7,
        'REPAIRRECORD' => 8,
        'SERVICE' => 9,
        'VIDEO' => 10,
        'VOUCHER' => 11
    );

    const CATEGORY_CN = array(
        '0' => '空',
        '1' => '轮播图',
        '2' => '课程',
        '3' => '人员',
        '4' => '企业',
        '5' => '试题',
        '6' => '新闻',
        '7' => '机构',
        '8' => '信用修复记录',
        '9' => '服务',
        '10' => '视频',
        '11' => '凭证'
    );

    const OPERATION = array(
        'OPERATION_NULL' => '0',
        'OPERATION_ADD' => '1',
        'OPERATION_EDIT' => '2',
        'OPERATION_ENABLE' => '5',
        'OPERATION_DISABLE' => '6',
        'OPERATION_APPROVE' => '7',
        'OPERATION_REJECT' => '8',
        'OPERATION_RESUBMIT' => '9',
        'OPERATION_SIGNIN' => '10',
        'OPERATION_SIGNOUT' => '11',
        'OPERATION_UPDATE_PASSWORD' => '12',
        'OPERATION_TOP' => '13',
        'OPERATION_CANCEL_TOP' => '14',
        'OPERATION_ACCEPT' => '16',
        'OPERATION_PUBLISH' => '17',
        'OPERATION_SUPPRESS' => '18',
        'OPERATION_MOVE' => '19',
        'OPERATION_UPLOAD' => '20',
        'OPERATION_RESET_PASSWORD' => '21',
        'OPERATION_DELETE' => '22',
        'OPERATION_READ' => '23',
        'OPERATION_UPDATE_MY_INFO' => '24',
        'OPERATION_REVOKE' => '25',
        'OPERATION_HOME_PAGE' => '26',
        'OPERATION_UPDATE_SCORE' => '27',
        'FORCE_OFFSHELVE' => '28',
        'OFFSHELVE' => '29',
        'SHELVE' => '30',
        'DETERMINE' => '31',
        'OPERATION_ADD_BANNER' => '32',
    );

    const OPERATION_CN = array(
        '0' => '未知',
        '1' => '添加',
        '2' => '编辑',
        '5' => '启用',
        '6' => '禁用',
        '7' => '审核通过',
        '8' => '审核驳回',
        '9' => '重新提交',
        '10' => '登录',
        '11' => '退出',
        '12' => '修改密码',
        '13' => '置顶',
        '14' => '取消置顶',
        '16' => '受理',
        '17' => '公示',
        '18' => '不公示',
        '19' => '移动',
        '20' => '上传',
        '21' => '重置密码',
        '22' => '删除',
        '23' => '已读',
        '24' => '修改个人信息',
        '25' => '撤销',
        '26' => '设为首页播放',
        '27' => '修改评分',
        '28' => '强制下架',
        '29' => '下架',
        '30' => '上架',
        '31' => '判定',
        '32' => '设置轮播'
    );
    
    public function getLog() : Log;
}
