<?php
namespace Sdk\Log\Repository\Log;

use Sdk\Log\Adapter\Log\ILogAdapter;
use Sdk\Log\Adapter\Log\LogEsAdapter;

use Sdk\Common\Repository\FetchRepositoryTrait;

class LogRepository implements ILogAdapter
{
    use FetchRepositoryTrait;

    private $adapter;

    public function __construct()
    {
        $this->adapter = new LogEsAdapter();
    }

    protected function getAdapter()
    {
        return $this->adapter;
    }
    
    public function logWeek($filter)
    {
        return $this->getAdapter()->logWeek($filter);
    }
}
