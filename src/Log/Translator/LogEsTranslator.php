<?php
namespace Sdk\Log\Translator;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Log\Model\Log;
use Sdk\Log\Model\ILogable;

// use Marmot\Common\Model\Object;
// use Marmot\Common\Model\IObject;

use Sdk\Crew\Model\Crew;
use Sdk\Crew\Model\Role;

class LogEsTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $log = null)
    {
        $context = (array)$expression['_source']['context'];

        $log = new Log(
            $context['operation'],
            $context['category'],
            $context['obj_id'],
            $expression['_source']['message']
        );

        $log->setId((string)$expression['_id']);
        $log->setDate($expression['_source']['datetime']);
        
        if (isset($context['ip'])) {
            $log->setIp($context['ip']);
        }

        $userContext = (array)$context['user'];
        $user = new Crew($userContext['id']);
        $userName = empty($userContext['userName']) ? '' : $userContext['userName'];
        $realName = empty($userContext['realName']) ? '' : $userContext['realName'];
        $user->setUserName($userName);
        $user->setRealName($realName);
        $log->setCrew($user);

        $roleContext = (array)$context['role'];
        $role = new Role($roleContext['id']);
        $role->setName($roleContext['name']);
        $log->setRole($role);

        return $log;
    }

    public function arrayToObjects(array $expression) : array
    {
        $logs = array();

        foreach ($expression as $each) {
            $logs[] = $this->arrayToObject($each);
        }

        return $logs;
    }

    public function objectToArray($log, array $keys = array())
    {
        unset($log);
        unset($keys);
        return array();
    }
}
