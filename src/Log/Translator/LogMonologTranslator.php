<?php
namespace Sdk\Log\Translator;

use Log\Model\NullLog;

use System\Classes\Translator;
use System\Classes\Server;
use Common\Controller\Traits\CrewAttributeTrait;
use Sdk\Crew\Model\Role;

class LogMonologTranslator extends Translator
{
    use CrewAttributeTrait;

    public function arrayToObject(array $expression, $log = null)
    {
        unset($expression);
        unset($log);
        return new NullLog();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    public function objectToArray($log, array $keys = array())
    {
        list($rolesId, $organizationId) = $this->getCrewAttribute();

        unset($keys);
        return [
            'operation' => $log->getOperation(),
            'category' => $log->getCategory(),
            'request_id' => $log->getRequestId(),
            'obj_id' => $log->getRecordObjId(),
            'ip' => Server::get('REMOTE_ADDR'),
            'user' => [
                'id'=>$log->getCrew()->getId(),
                'userName'=>$log->getCrew()->getCellphone(),
                'realName'=>$log->getCrew()->getRealName()
            ],
            'role' => [
                'id'=>$rolesId,
                'name'=>Role::ROLES[$rolesId]
            ],
            'organization' => $organizationId
        ];
    }
}
