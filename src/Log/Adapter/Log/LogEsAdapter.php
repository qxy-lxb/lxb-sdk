<?php
namespace Sdk\Log\Adapter\Log;

use Sdk\Log\Translator\LogEsTranslator;
use Sdk\Log\Model\ILogable;
use Marmot\Interfaces\IRestfulTranslator;

use Common\Utils\DateUtilsTrait;

use Marmot\Core;

use Sdk\News\Model\CategoryModelFactory;
use Elasticsearch\ClientBuilder;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */

class LogEsAdapter implements ILogAdapter
{
    const NDRC = 1;

    use DateUtilsTrait;

    private $index;

    private $type;

    private $host;

    private $translator;

    private $clientBuilder;

    public function __construct()
    {
        $this->index = Core::$container->get('elasticsearch.index');
        $this->type = Core::$container->get('elasticsearch.type');
        $this->host = Core::$container->get('elasticsearch.hosts');
        $this->translator = new LogEsTranslator();
        $this->clientBuilder = new ClientBuilder();
    }

    public function __destruct()
    {
        unset($this->index);
        unset($this->type);
        unset($this->host);
        unset($this->translator);
        unset($this->clientBuilder);
    }

    protected function getIndex()
    {
        return $this->index;
    }

    protected function getType()
    {
        return $this->type;
    }
    
    protected function getHost()
    {
        return $this->host;
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getClientBuilder() : ClientBuilder
    {
        return $this->clientBuilder;
    }

    protected function getClient()
    {
        $client = $this->getClientBuilder()->create()
                       ->setHosts($this->getHost())
                       ->build();

        return $client;
    }

    public function fetchOne($id)
    {
        $params = [
            'index' => $this->getIndex(),
            'type' => $this->getType(),
            'id' => $id,
        ];
        
        $data = $this->getClient()->get($params)['hits'];
        
        $log = $this->getTranslator()->arrayToObject((array)$data);

        return $log;
    }

    public function fetchList(array $ids) : array
    {
        unset($ids);

        $logs = array();
        
        return $logs;
    }
    
    public function search(
        array $filter = array(),
        array $sort = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        $form = ($number-1)*$size;
        $params = [
            'index' => $this->getIndex(),
            'type' => $this->getType(),
            'from' => $form,
            'size' => $size,
        ];
        
        $params['body']['sort'] = array(
            'datetime' => array(
                'order' => 'desc',
            )
        );

        if (!empty($filter['role'])) {
            $params['body']['query']['bool']['must'][]['match']['context.role.id'] = (int)$filter['role'];
        }

        if (!empty($filter['organization'])) {
            $params['body']['query']['bool']['must'][]['match']['context.organization'] = (int)$filter['organization'];
        }
        
        if (!empty($filter['userName'])) {
            $params['body']['query']['bool']['must'][]['match']['context.user.userName'] = (int)$filter['userName'];
        }

        if (!empty($filter['startTime'])) {
            $params['body']['query']['bool']['must'][]['range']['datetime']['gte'] = $filter['startTime'];
        }

        if (!empty($filter['endTime'])) {
            $params['body']['query']['bool']['must'][]['range']['datetime']['lte'] = $filter['endTime'];
        }

        $cursor = $this->getClient()->search($params)['hits'];

        $list = $cursor['hits'];

        $count = $cursor['total'];
        
        $logs = array();
      
        foreach ($list as $document) {
            $logs[] = $this->getTranslator()->arrayToObject((array)$document);
        }
        
        return [$count, $logs];
    }

    // 获取当前周平台角色的数据
    public function logWeek($filter)
    {
        $week = $this->getWeekMyActionAndEnd();

        $params = [
            'index' => $this->getIndex(),
            'type' => $this->getType(),
            "size" => 0,
            'body' =>[
                "aggs" => [
                    "datetime" => [
                        "date_histogram" => [
                            "field" => "datetime",
                            "interval" => "day",
                            "min_doc_count" => 0,
                            "extended_bounds" => [
                                "min" => $week["weekStart"],
                                "max" => $week["weekEnd"]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        if (!empty($filter['role'])) {
            $params['body']['query']['bool']['must'][]['match']['context.role.id'] = (int)$filter['role'];
        }

        $response = $this->getClient()->search($params)['aggregations']['datetime']['buckets'];
        
        return $response;
    }
}
