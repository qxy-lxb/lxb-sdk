<?php
namespace Sdk\Notify\Model;

use Common\Utils\SMS;

use Marmot\Core;

class SmsMessage extends Message implements ISend
{
    const TEMPLATE_ID_NULL = '';

    /**
     * @var string $template 短信模板
     */
    private $template;

    private $sms;

    public function __construct(
        string $template
    ) {
        parent::__construct();
        //短信没有标题
        $this->template = in_array(
            $template,
            array(
                REPAIRRECORD_APPROVE_TEMPLATE_ID_AUTH,
                REPAIRRECORD_REJECT_TEMPLATE_ID_AUTH,
                ENTERPRISE_APPROVE_TEMPLATE_ID_AUTH,
                ENTERPRISE_REJECT_TEMPLATE_ID_AUTH,
            )
        ) ? $template : self::TEMPLATE_ID_NULL;
        $this->sms = new SMS();
    }

    public function __destruct()
    {
        unset($this->template);
        unset($this->sms);
    }

    protected function getTemplate() : string
    {
        return $this->template;
    }

    protected function getSms() : SMS
    {
        return $this->sms;
    }

    public function setContent(array $content)
    {
        $this->content = $content;
    }

    public function send()
    {
        $targets = $this->getTargets();
        $template = $this->getTemplate();
        $content = $this->getContent();
        
        if ($this->getSms()->send($targets, $template, $content)) {
            return true;
        }
        return false;
    }
}
