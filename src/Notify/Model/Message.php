<?php
namespace Sdk\Notify\Model;

abstract class Message
{
    /**
     * @var string $title 信息标题
     */
    protected $title;

    /**
     * @var array $content 信息内容
     */
    protected $content;

    /**
     * @var string $targets 信息目标
     */
    protected $targets;


    public function __construct()
    {
        $this->title = '';
        $this->content = '';
        $this->targets = '';
    }

    public function __destruct()
    {
        unset($this->title);
        unset($this->content);
        unset($this->targets);
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * Gets the value of title.
     *
     * @return array $title 信息标题
     */
    public function getTitle() : array
    {
        return $this->title;
    }

    abstract public function setContent(array $content);

    /**
     * Gets the value of content.
     *
     * @return array $content 信息内容
     */
    public function getContent() : array
    {
        return $this->content;
    }

    public function setTargets(string $targets)
    {
        $this->targets = $targets;
    }

    /**
     * Gets the value of targets.
     *
     * @return string $targets 信息目标
     */
    public function getTargets() : string
    {
        return $this->targets;
    }
}
