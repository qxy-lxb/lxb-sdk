<?php
namespace Sdk\Notify\Model;

/**
 * 通用信息发送接口
 */
interface ISend
{

    //发送
    public function send();
}
