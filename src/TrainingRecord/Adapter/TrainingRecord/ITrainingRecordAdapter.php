<?php
namespace Sdk\TrainingRecord\Adapter\TrainingRecord;

use Marmot\Interfaces\IAsyncAdapter;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\TrainingRecord\Model\TrainingRecord;

interface ITrainingRecordAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter
{
    public function getProof(TrainingRecord $trainingRecord);
}
