<?php
namespace Sdk\TrainingRecord\Adapter\TrainingRecord;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Marmot\Core;

use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;

use Sdk\TrainingRecord\Translator\TrainingRecordRestfulTranslator;
use Sdk\TrainingRecord\Model\NullTrainingRecord;
use Sdk\TrainingRecord\Model\TrainingRecord;

class TrainingRecordRestfulAdapter extends GuzzleAdapter implements ITrainingRecordAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,OperatAbleRestfulAdapterTrait, FetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    const SCENARIOS = [
        'TRAININGRECORD_LIST'=>[
            'fields'=>[],
            'include'=>'member'
        ],
        'TRAININGRECORD_FETCH_ONE'=>[
            'fields'=>[
                'enterprises'=>'name,unifiedSocialCreditCode',
            ],
            'include'=>'course,organization,member,courseOrder,videos,enterprise'
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new TrainingRecordRestfulTranslator();
        $this->resource = 'trainingRecords';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullTrainingRecord());
    }

    public function addAction(TrainingRecord $trainingRecord) : bool
    {
        unset($trainingRecord);
        
        return false;
    }

    public function editAction(TrainingRecord $trainingRecord) : bool
    {
        $data = $this->getTranslator()->objectToArray($trainingRecord, array('learnedProgress'));
        
        $this->patch($this->getResource().'/'.$trainingRecord->getId().'/toLearn', $data);

        if ($this->isSuccess()) {
            $this->translateToObject($trainingRecord);
            return true;
        }
        
        return false;
    }

    public function getProof(TrainingRecord $trainingRecord) : bool
    {
        return $this->getProofAction($trainingRecord);
    }

    protected function getProofAction(TrainingRecord $trainingRecord) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $trainingRecord,
            array(
                'authLetter'
            )
        );

        $this->patch(
            $this->getResource().'/'.$trainingRecord->getId().'/getProof',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($trainingRecord);
            return true;
        }

        return false;
    }
}
