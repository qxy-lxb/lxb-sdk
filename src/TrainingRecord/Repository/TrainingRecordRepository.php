<?php
namespace Sdk\TrainingRecord\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;

use Sdk\TrainingRecord\Adapter\TrainingRecord\TrainingRecordRestfulAdapter;
use Sdk\TrainingRecord\Model\TrainingRecord;
use Sdk\TrainingRecord\Adapter\TrainingRecord\ITrainingRecordAdapter;
use Marmot\Core;

use Marmot\Framework\Classes\Repository;

class TrainingRecordRepository extends Repository implements ITrainingRecordAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, OperatAbleRepositoryTrait;

    const LIST_MODEL_UN = 'TRAININGRECORD_LIST';
    const FETCH_ONE_MODEL_UN = 'TRAININGRECORD_FETCH_ONE';

    private $adapter;

    public function __construct()
    {
        $this->adapter = new TrainingRecordRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : ITrainingRecordAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : ITrainingRecordAdapter
    {
        return new TrainingRecordMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function getProof(TrainingRecord $trainingRecord) : bool
    {
        return $this->getAdapter()->getProof($trainingRecord);
    }
}
