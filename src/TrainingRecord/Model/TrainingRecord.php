<?php
namespace Sdk\TrainingRecord\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;

use Sdk\Order\Model\Order;
use Sdk\Course\Model\Course;
use Sdk\Organization\Model\Organization;
use Sdk\Member\Model\Member;
use Sdk\Enterprise\Model\Enterprise;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\TrainingRecord\Repository\TrainingRecordRepository;

class TrainingRecord implements IOperatAble, IObject
{
    use OperatAbleTrait, Object;

    private $id;

    private $progress;
    
    private $member;

    private $course;

    private $learnedRatio;

    private $authLetter;

    private $trainingProof;

    private $examReport;
    
    private $orderNumber;

    private $order;

    private $organization;

    private $snapshot;

    private $paidTime;
    private $learnedTime;
    private $passTime;
    private $gainTime;

    private $learnedProgress;
    private $expirationDate;

    private $video;
    private $score;
    private $repository;

    private $repairableDishonestyInfo;
    private $enterprise;

    private $repairRecordId;


    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->progress = '';
        $this->member = Core::$container->has('user') ? Core::$container->get('user') : new Member();
        $this->course = new Course();
        $this->learnedRatio = '';
        $this->authLetter = array();
        $this->trainingProof = array();
        $this->examReport = '';
        $this->orderNumber = '';
        $this->order = new Order();
        $this->organization = new Organization();
        $this->paidTime = 0;
        $this->learnedTime = 0;
        $this->passTime = 0;
        $this->gainTime = 0;
        $this->status = 0;
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->learnedProgress = array();
        $this->expirationDate = 0;
        $this->video = array();
        $this->score = 0;
        $this->snapshot = '';
        $this->repairableDishonestyInfo = array();
        $this->enterprise = new Enterprise();
        $this->repository = new TrainingRecordRepository();
        $this->repairRecordId = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->progress);
        unset($this->member);
        unset($this->course);
        unset($this->learnedRatio);
        unset($this->authLetter);
        unset($this->trainingProof);
        unset($this->examReport);
        unset($this->orderNumber);
        unset($this->order);
        unset($this->organization);
        unset($this->paidTime);
        unset($this->learnedTime);
        unset($this->passTime);
        unset($this->gainTime);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->learnedProgress);
        unset($this->expirationDate);
        unset($this->video);
        unset($this->score);
        unset($this->snapshot);
        unset($this->repairableDishonestyInfo);
        unset($this->enterprise);
        unset($this->repository);
        unset($this->repairRecordId);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setProgress(string $progress) : void
    {
        $this->progress = $progress;
    }

    public function getProgress() : string
    {
        return $this->progress;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setCourse(Course $course) : void
    {
        $this->course = $course;
    }

    public function getCourse() : Course
    {
        return $this->course;
    }

    public function setLearnedRatio(string $learnedRatio) : void
    {
        $this->learnedRatio = $learnedRatio;
    }

    public function getLearnedRatio() : string
    {
        return $this->learnedRatio;
    }

    public function setAuthLetter(array $authLetter) : void
    {
        $this->authLetter = $authLetter;
    }

    public function getAuthLetter() : array
    {
        return $this->authLetter;
    }

    public function setTrainingProof(array $trainingProof) : void
    {
        $this->trainingProof = $trainingProof;
    }

    public function getTrainingProof() : array
    {
        return $this->trainingProof;
    }

    public function setExamReport(string $examReport) : void
    {
        $this->examReport = $examReport;
    }

    public function getExamReport() : string
    {
        return $this->examReport;
    }

    public function setOrderNumber(string $orderNumber) : void
    {
        $this->orderNumber = $orderNumber;
    }

    public function getOrderNumber() : string
    {
        return $this->orderNumber;
    }

    public function setOrder(Order $order) : void
    {
        $this->order = $order;
    }

    public function getOrder() : Order
    {
        return $this->order;
    }

    public function setOrganization(Organization $organization) : void
    {
        $this->organization = $organization;
    }

    public function getOrganization() : Organization
    {
        return $this->organization;
    }

    public function setPaidTime(int $paidTime)
    {
        $this->paidTime = $paidTime;
    }

    public function getPaidTime() : int
    {
        return $this->paidTime;
    }

    public function setLearnedTime(int $learnedTime)
    {
        $this->learnedTime = $learnedTime;
    }

    public function getLearnedTime() : int
    {
        return $this->learnedTime;
    }

    public function setPassTime(int $passTime)
    {
        $this->passTime = $passTime;
    }

    public function getPassTime() : int
    {
        return $this->passTime;
    }

    public function setGainTime(int $gainTime)
    {
        $this->gainTime = $gainTime;
    }

    public function getGainTime() : int
    {
        return $this->gainTime;
    }
    
    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function setLearnedProgress(array $learnedProgress)
    {
        $this->learnedProgress = $learnedProgress;
    }

    public function getLearnedProgress() : array
    {
        return $this->learnedProgress;
    }

    public function setExpirationDate(string $expirationDate)
    {
        $this->expirationDate = $expirationDate;
    }

    public function getExpirationDate() : string
    {
        return $this->expirationDate;
    }

    public function setVideo(array $video) : void
    {
        $this->video = $video;
    }

    public function getVideo() : array
    {
        return $this->video;
    }

    public function setScore(int $score) : void
    {
        $this->score = $score;
    }

    public function getScore() : int
    {
        return $this->score;
    }
    
    public function setSnapshot(string $snapshot) : void
    {
        $this->snapshot = $snapshot;
    }

    public function getSnapshot() : string
    {
        return $this->snapshot;
    }

    public function setRepairableDishonestyInfo(array $repairableDishonestyInfo) : void
    {
        $this->repairableDishonestyInfo = $repairableDishonestyInfo;
    }

    public function getRepairableDishonestyInfo() : array
    {
        return $this->repairableDishonestyInfo;
    }

    public function setEnterprise(Enterprise $enterprise) : void
    {
        $this->enterprise = $enterprise;
    }

    public function getEnterprise() : Enterprise
    {
        return $this->enterprise;
    }

    protected function getRepository() : TrainingRecordRepository
    {
        return $this->repository;
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    /**
     * 获取证明-模版提交
     * @return
     */
    public function getProof() : bool
    {
        return $this->getRepository()->getProof($this);
    }

    public function setRepairRecordId(int $repairRecordId) : void
    {
        $this->repairRecordId = $repairRecordId;
    }

    public function getRepairRecordId() : int
    {
        return $this->repairRecordId;
    }
}
