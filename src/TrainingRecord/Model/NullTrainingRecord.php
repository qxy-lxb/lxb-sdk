<?php
namespace Sdk\TrainingRecord\Model;

use Marmot\Interfaces\INull;
use Sdk\Common\Model\NullOperatAbleTrait;

use Marmot\Core;

class NullTrainingRecord extends TrainingRecord implements INull
{
    use NullOperatAbleTrait;

    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
