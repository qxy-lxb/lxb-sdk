<?php
namespace Sdk\TrainingRecord\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Organization\Translator\OrganizationRestfulTranslator;
use Sdk\Course\Translator\CourseRestfulTranslator;
use Sdk\Video\Translator\VideoRestfulTranslator;
use Sdk\Enterprise\Translator\EnterpriseRestfulTranslator;

use Sdk\TrainingRecord\Model\TrainingRecord;
use Sdk\TrainingRecord\Model\NullTrainingRecord;
use Sdk\Organization\Repository\OrganizationRepository;
use Sdk\Course\Repository\CourseRepository;
use Sdk\Member\Translator\MemberRestfulTranslator;
use Sdk\Order\Translator\OrderRestfulTranslator;

class TrainingRecordRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getOrganizationRestfulTranslator() : OrganizationRestfulTranslator
    {
        return new OrganizationRestfulTranslator();
    }

    protected function getCourseRestfulTranslator() : CourseRestfulTranslator
    {
        return new CourseRestfulTranslator();
    }

    public function getMemberRestfulTranslator() : MemberRestfulTranslator
    {
        return new MemberRestfulTranslator();
    }
    
    public function getOrderRestfulTranslator() : OrderRestfulTranslator
    {
        return new OrderRestfulTranslator();
    }

    public function getVideoRestfulTranslator() : VideoRestfulTranslator
    {
        return new VideoRestfulTranslator();
    }

    public function getEnterpriseRestfulTranslator() : EnterpriseRestfulTranslator
    {
        return new EnterpriseRestfulTranslator();
    }

    public function arrayToObject(array $expression, $trainingRecord = null)
    {
        if (empty($expression)) {
            return NullTrainingRecord::getInstance();
        }

        if ($trainingRecord == null) {
            $trainingRecord = new TrainingRecord();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $trainingRecord->setId($id);

        if (isset($data['attributes'])) {
            $attributes = $data['attributes'];

            if (isset($attributes['orderNumber'])) {
                $trainingRecord->setOrderNumber($attributes['orderNumber']);
            }
            if (isset($attributes['progress'])) {
                $trainingRecord->setProgress($attributes['progress']);
            }
            if (isset($attributes['learnedRatio'])) {
                $trainingRecord->setLearnedRatio($attributes['learnedRatio']);
            }
            if (isset($attributes['authLetter'])) {
                $trainingRecord->setAuthLetter($attributes['authLetter']);
            }
            if (isset($attributes['trainingProof'])) {
                $trainingRecord->setTrainingProof($attributes['trainingProof']);
            }
            if (isset($attributes['examReport'])) {
                $trainingRecord->setExamReport($attributes['examReport']);
            }
            if (isset($attributes['status'])) {
                $trainingRecord->setStatus($attributes['status']);
            }
            if (isset($attributes['createTime'])) {
                $trainingRecord->setCreateTime($attributes['createTime']);
            }
            if (isset($attributes['updateTime'])) {
                $trainingRecord->setUpdateTime($attributes['updateTime']);
            }
            if (isset($attributes['statusTime'])) {
                $trainingRecord->setStatusTime($attributes['statusTime']);
            }
            if (isset($attributes['paidTime'])) {
                $trainingRecord->setPaidTime($attributes['paidTime']);
            }
            if (isset($attributes['learnedTime'])) {
                $trainingRecord->setLearnedTime($attributes['learnedTime']);
            }
            if (isset($attributes['passTime'])) {
                $trainingRecord->setPassTime($attributes['passTime']);
            }
            if (isset($attributes['gainTime'])) {
                $trainingRecord->setGainTime($attributes['gainTime']);
            }
            if (isset($attributes['expirationDate'])) {
                $trainingRecord->setExpirationDate($attributes['expirationDate']);
            }
            if (isset($attributes['learnedProgress'])) {
                $trainingRecord->setLearnedProgress($attributes['learnedProgress']);
            }
            if (isset($attributes['score'])) {
                $trainingRecord->setScore($attributes['score']);
            }
            if (isset($attributes['snapshot'])) {
                $trainingRecord->setSnapshot($attributes['snapshot']);
            }
            if (isset($attributes['courseName'])) {
                $trainingRecord->getCourse()->setName($attributes['courseName']);
            }
            if (isset($attributes['repairableDishonestyInfos'])) {
                $trainingRecord->setRepairableDishonestyInfo($attributes['repairableDishonestyInfos']);
            }
            if (isset($attributes['repairRecordId'])) {
                $trainingRecord->setRepairRecordId($attributes['repairRecordId']);
            }
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        if (isset($relationships['course']['data'])) {
            $course = $this->changeArrayFormat($relationships['course']['data']);
            $trainingRecord->setCourse($this->getCourseRestfulTranslator()->arrayToObject($course));
        }
        if (isset($relationships['videos']['data'])) {
            $videos = $this->changeArrayFormat($relationships['videos']['data']);
            $trainingRecord->getCourse()->setVideos($videos);
        }
        if (isset($relationships['organization']['data'])) {
            $trainingRecord->getOrganization()->setId($relationships['organization']['data']['id']);
            $trainingRecord->getOrganization()->setName($attributes['organizationName']);
        }
        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);
            $trainingRecord->setMember($this->getMemberRestfulTranslator()->arrayToObject($member));
        }
        if (isset($relationships['courseOrder']['data'])) {
            $courseOrder = $this->changeArrayFormat($relationships['courseOrder']['data']);
            $trainingRecord->setOrder($this->getOrderRestfulTranslator()->arrayToObject($courseOrder));
        }
        if (isset($relationships['enterprise']['data'])) {
            $enterprise = $this->changeArrayFormat($relationships['enterprise']['data']);
            $trainingRecord->setEnterprise($this->getEnterpriseRestfulTranslator()->arrayToObject($enterprise));
        }
        return $trainingRecord;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    private function translateToObject(array $expression, $trainingRecord = null)
    {
        if (empty($expression)) {
            return NullTrainingRecord::getInstance();
        }

        if ($trainingRecord == null) {
            $trainingRecord = new TrainingRecord();
        }

        $data = $expression['data'];

        $id = $data['id'];
        $trainingRecord->setId($id);

        if (isset($data['attributes'])) {
            $attributes = $data['attributes'];

            if (isset($attributes['orderNumber'])) {
                $trainingRecord->setOrderNumber($attributes['orderNumber']);
            }
            if (isset($attributes['progress'])) {
                $trainingRecord->setProgress($attributes['progress']);
            }
            if (isset($attributes['status'])) {
                $trainingRecord->setStatus($attributes['status']);
            }
            if (isset($attributes['createTime'])) {
                $trainingRecord->setCreateTime($attributes['createTime']);
            }
            if (isset($attributes['paidTime'])) {
                $trainingRecord->setPaidTime($attributes['paidTime']);
            }
            if (isset($attributes['updateTime'])) {
                $trainingRecord->setUpdateTime($attributes['updateTime']);
            }
            if (isset($attributes['statusTime'])) {
                $trainingRecord->setStatusTime($attributes['statusTime']);
            }
            if (isset($attributes['courseName'])) {
                $trainingRecord->getCourse()->setName($attributes['courseName']);
            }
            if (isset($attributes['thumbnail'])) {
                $trainingRecord->getCourse()->setThumbnail($attributes['thumbnail']);
            }
            if (isset($attributes['learnedRatio'])) {
                $trainingRecord->setLearnedRatio($attributes['learnedRatio']);
            }
            if (isset($attributes['score'])) {
                $trainingRecord->setScore($attributes['score']);
            }
            if (isset($attributes['isRequiredExam'])) {
                $trainingRecord->getCourse()->setIsRequiredExam($attributes['isRequiredExam']);
            }
            if (isset($attributes['expirationDate'])) {
                $trainingRecord->setExpirationDate($attributes['expirationDate']);
            }
            if (isset($attributes['organizationName'])) {
                $trainingRecord->getOrganization()->setName($attributes['organizationName']);
            }
            if (isset($attributes['enterpriseCode'])) {
                $trainingRecord->getEnterprise()->setUnifiedSocialCreditCode($attributes['enterpriseCode']);
            }
            if (isset($attributes['repairRecordId'])) {
                $trainingRecord->setRepairRecordId($attributes['repairRecordId']);
            }
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);
            $trainingRecord->setMember($this->getMemberRestfulTranslator()->arrayToObject($member));
        }
        return $trainingRecord;
    }

    public function objectToArray($trainingRecord, array $keys = array())
    {
        if (!$trainingRecord instanceof TrainingRecord) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'learnedProgress',
                'authLetter'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'trainingRecords'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $trainingRecord->getId();
        }

        if (in_array('learnedProgress', $keys)) {
            $expression['data']['attributes']['learnedProgress'] = $trainingRecord->getLearnedProgress();
        }
        if (in_array('authLetter', $keys)) {
            $expression['data']['attributes']['authLetter'] = $trainingRecord->getAuthLetter();
        }
        return $expression;
    }
}
