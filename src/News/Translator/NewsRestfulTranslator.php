<?php
namespace Sdk\News\Translator;

use Sdk\Common\Translator\RestfulTranslatorTrait;
use Marmot\Core;
use Sdk\Crew\Translator\CrewRestfulTranslator;

use Sdk\News\Model\News;
use Sdk\News\Model\NullNews;

use Marmot\Interfaces\IRestfulTranslator;

class NewsRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getCrewRestfulTranslator()
    {
        return new CrewRestfulTranslator();
    }

    public function arrayToObject(array $expression, $news = null)
    {
        return $this->translateToObject($expression, $news);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $news = null)
    {
        if (empty($expression)) {
            return new NullNews();
        }

        if ($news == null) {
            $news = new News();
        }

        $data =  $expression['data'];

        $id = $data['id'];
        $news->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['title'])) {
            $news->setTitle($attributes['title']);
        }
        if (isset($attributes['createTime'])) {
            $news->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $news->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['content'])) {
            $news->setContent($attributes['content']);
        }
        if (isset($attributes['category'])) {
            $news->setCategory($attributes['category']);
        }
        if (isset($attributes['image'])) {
            $news->setImage($attributes['image']);
        }
        if (isset($attributes['attachment'])) {
            $news->setAttachments($attributes['attachment']);
        }
        if (isset($attributes['status'])) {
            $news->setStatus($attributes['status']);
        }
        if (isset($attributes['stick'])) {
            $news->setStick($attributes['stick']);
        }
        if (isset($attributes['statusTime'])) {
            $news->setStatusTime($attributes['statusTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['crew']['data'])) {
            $crew = $this->changeArrayFormat($relationships['crew']['data']);

            $news->setCrew($this->getCrewRestfulTranslator()->arrayToObject($crew));
        }

        return $news;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($news, array $keys = array())
    {
        if (!$news instanceof News) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'source',
                'content',
                'category',
                'dimension',
                'reason',
                'image',
                'attachments',
                'crew'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'news'
            )
        );
        
        if (in_array('id', $keys)) {
            $expression['data']['id'] = $news->getId();
        }

        $attributes = array();

        if (in_array('title', $keys)) {
            $attributes['title'] = $news->getTitle();
        }
        if (in_array('source', $keys)) {
            $attributes['source'] = $news->getSource();
        }
        if (in_array('content', $keys)) {
            $attributes['content'] = $news->getContent();
        }
        if (in_array('category', $keys)) {
            $attributes['category'] = $news->getCategory();
        }
        if (in_array('reason', $keys)) {
            $attributes['rejectReason'] = $news->getReason();
        }
        if (in_array('image', $keys)) {
            $attributes['image'] = $news->getImage();
        }
        if (in_array('attachments', $keys)) {
            $attributes['attachment'] = $news->getAttachments();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => Core::$container->get('crew')->getId()
                )
             );
        }

        return $expression;
    }
}
