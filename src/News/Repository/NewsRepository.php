<?php
namespace Sdk\News\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\EnableAbleRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\TopAbleRepositoryTrait;

use Sdk\News\Adapter\News\INewsAdapter;
use Sdk\News\Adapter\News\NewsRestfulAdapter;
use Sdk\News\Adapter\News\NewMockAdapter;
use Sdk\News\Model\News;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class NewsRepository extends Repository implements INewsAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, EnableAbleRepositoryTrait, OperatAbleRepositoryTrait, TopAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'NEWS_LIST';
    const FETCH_ONE_MODEL_UN = 'NEWS_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new NewsRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : INewsAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : INewsAdapter
    {
        return new NewMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
