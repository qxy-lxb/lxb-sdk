<?php
namespace Sdk\News\Model;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Adapter\IEnableAbleAdapter;
use Sdk\Common\Adapter\ITopAbleAdapter;
use Sdk\Common\Model\IEnableAble;
use Sdk\Common\Model\EnableAbleTrait;
use Sdk\Common\Model\ITopAble;
use Sdk\Common\Model\TopAbleTrait;

use Sdk\Crew\Model\Crew;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Marmot\Core;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Sdk\News\Repository\NewsRepository;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class News implements IEnableAble, IOperatAble, IObject, ITopAble
{
    use EnableAbleTrait, OperatAbleTrait, Object, TopAbleTrait;

    const CATEGORY = array(
        'POLICY_NEWS' => 1,
        'CREDIT_INFO' => 2
    );

    private $id;

    private $title;

    private $source;

    private $content;

    private $attachments;

    private $image;

    private $crew;

    private $category;

    private $stick;

    private $reason;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->title = '';
        $this->source = '';
        $this->content = array();
        $this->category = self::CATEGORY['POLICY_NEWS'];
        $this->attachments = array();
        $this->image = array();
        $this->crew = new Crew();
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->stick = ITopAble::STICK['DISABLED'];
        $this->reason = '';
        $this->repository = new NewsRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->content);
        unset($this->category);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->status);
        unset($this->stick);
        unset($this->source);
        unset($this->attachments);
        unset($this->image);
        unset($this->crew);
        unset($this->reason);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setContent(array $content) : void
    {
        $this->content = $content;
    }

    public function getContent() : array
    {
        return $this->content;
    }

    public function setSource(string $source) : void
    {
        $this->source = $source;
    }

    public function getSource() : string
    {
        return $this->source;
    }

    public function setAttachments(array $attachments) : void
    {
        $this->attachments = $attachments;
    }

    public function getAttachments() : array
    {
        return $this->attachments;
    }

    public function setImage(array $image) : void
    {
        $this->image = $image;
    }

    public function getImage() : array
    {
        return $this->image;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setCategory(int $category) : void
    {
        $this->category = in_array(
            $category,
            self::CATEGORY
        ) ? $category : self::CATEGORY['POLICY_NEWS'];
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function getStick() : int
    {
        return $this->stick;
    }
    
    public function setReason(string $reason) : void
    {
        $this->reason = $reason;
    }

    public function getReason() : string
    {
        return $this->reason;
    }

    protected function getRepository() : NewsRepository
    {
        return $this->repository;
    }

    protected function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getITopAbleAdapter() : ITopAbleAdapter
    {
        return $this->getRepository();
    }

    public function move()
    {
        if (!$this->isOperateAble()) {
            Core::setLastError(PURVIEW_UNDEFINED);
            return false;
        }

        return $this->getRepository()->move($this);
    }

    protected function getIOperatAbleRepository() : NewsRepository
    {
        return $this->getRepository();
    }

    public function getColumn() : int
    {
        $newsType = $this->getNewsType()->getId();

        $column = self::NEWS_COLUMN[$newsType]!= null ? self::NEWS_COLUMN[$newsType] : 0;

        return $column;
    }

    public function isPermit(): bool
    {
         return $this->isUserGroupEqual();
    }

    private function isUserGroupEqual()
    {
        return Core::$container->get('user')->getUserGroup()->getId() == $this->getPublishUserGroup()->getId();
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }
}
