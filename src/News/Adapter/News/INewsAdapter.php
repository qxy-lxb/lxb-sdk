<?php
namespace Sdk\News\Adapter\News;

use Sdk\Common\Adapter\IEnableAbleAdapter;
use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\ITopAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;

use Marmot\Interfaces\IAsyncAdapter;

interface INewsAdapter extends IEnableAbleAdapter, IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter, ITopAbleAdapter
{
}
