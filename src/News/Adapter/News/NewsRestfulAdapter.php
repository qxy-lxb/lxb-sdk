<?php
namespace Sdk\News\Adapter\News;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Adapter\TopAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\EnableAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\CommonMapErrorsTrait;

use Sdk\News\Translator\NewsRestfulTranslator;
use Sdk\News\Model\NullNews;
use Sdk\News\Model\News;

use Marmot\Core;

class NewsRestfulAdapter extends GuzzleAdapter implements INewsAdapter
{
    use EnableAbleRestfulAdapterTrait,
        AsyncFetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        TopAbleRestfulAdapterTrait,
        CommonMapErrorsTrait;

    private $translator;
    private $resource;

    const SCENARIOS = [
        'NEWS_LIST'=>[
            'fields'=>[
                'crews'=>'cellphone'
            ],
            'include'=>'crews'
        ],
        'NEWS_FETCH_ONE'=>[
            'fields'=>[
                'crews'=>'cellphone'
            ],
            'include'=>'crews'
        ]
    ];
    
    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new NewsRestfulTranslator();
        $this->resource = 'news';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            6001 => TITLE_FORMAT_ERROR,
            6003 => ATTACHMENT_FORMAT_ERROR,
            6004 => NEWS_TYPE_NOT_EXIST,
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullNews());
    }

    public function addAction(News $news) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $news,
            array('title', 'content', 'image', 'attachments', 'category', 'crew',)
        );
     
        
        $this->post(
            $this->getResource(),
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($news);
            return true;
        }

        return false;
    }

    public function editAction(News $news) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $news,
            array('title', 'content', 'image', 'attachments')
        );

        $this->patch(
            $this->getResource().'/'.$news->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($news);
            return true;
        }

        return false;
    }

    public function move(News $news) : bool
    {
        $this->patch(
            $this->getResource().'/'.$news->getId().'/move/'.$news->getNewsType()->getId()
        );

        if ($this->isSuccess()) {
            $this->translateToObject($news);
            return true;
        }

        return false;
    }
}
