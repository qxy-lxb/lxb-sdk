<?php
namespace Sdk\Common\Repository;

use Sdk\Common\Model\IDeleteAble;

trait DeleteAbleRepositoryTrait
{
    public function softDelete(IDeleteAble $deleteAbleObject) : bool
    {
        return $this->getAdapter()->softDelete($deleteAbleObject);
    }

    public function cancel(IDeleteAble $deleteAbleObject) : bool
    {
        return $this->getAdapter()->cancel($deleteAbleObject);
    }
}
