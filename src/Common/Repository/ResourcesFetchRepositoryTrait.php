<?php
namespace Sdk\Common\Repository;

trait ResourcesFetchRepositoryTrait
{
    public function fetchOne($id, string $resources)
    {
        return $this->getAdapter()->fetchOne($id, $resources);
    }

    public function search(
        string $resources,
        array $filter = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->search($resources, $filter, $number, $size);
    }
}
