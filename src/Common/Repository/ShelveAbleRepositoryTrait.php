<?php
namespace Sdk\Common\Repository;

use Sdk\Common\Model\IShelveAble;

trait ShelveAbleRepositoryTrait
{
    public function shelve(IShelveAble $shelveAbleObject) : bool
    {
        return $this->getAdapter()->shelve($shelveAbleObject);
    }

    public function offShelve(IShelveAble $shelveAbleObject) : bool
    {
        return $this->getAdapter()->offShelve($shelveAbleObject);
    }
}
