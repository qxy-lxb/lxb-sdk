<?php
namespace Sdk\Common\Adapter;

use Sdk\Common\Model\IDeleteAble;

interface IDeleteAbleAdapter
{
    public function softDelete(IDeleteAble $deleteAbleObject) : bool;

    public function cancel(IDeleteAble $deleteAbleObject) : bool;
}
