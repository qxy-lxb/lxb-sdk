<?php
namespace Sdk\Common\Adapter;

use Sdk\Common\Model\IDeleteAble;

trait DeleteAbleMockAdapterTrait
{
    public function softDelete(IDeleteAble $deleteAbleObject) : bool
    {
        unset($deleteAbleObject);
        return true;
    }

    public function cancel(IDeleteAble $deleteAbleObject) : bool
    {
        unset($deleteAbleObject);
        return true;
    }
}
