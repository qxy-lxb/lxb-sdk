<?php
namespace Sdk\Common\Adapter;

use Marmot\Interfaces\INull;

trait ResourcesFetchAbleAdapterTrait
{
    abstract protected function getResource() : string;

    public function fetchOne(int $id, string $resources, INull $null)
    {
        return $this->fetchOneAction($id, $null);
    }

    protected function fetchOneAction(int $id, string $resources, INull $null)
    {
        $this->get(
            $this->getResource().'/'.$resources.'/'.$id
        );

        return $this->isSuccess() ? $this->translateToObject() : $null;
    }

    public function search(
        string $resources,
        array $filter = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        return $this->searchAction($resources, $filter, $number, $size);
    }

    protected function searchAction(
        string $resources,
        array $filter = array(),
        int $number = 0,
        int $size = 20
    ) : array {
        $this->get(
            $this->getResource().'/'.$resources,
            array(
                'filter'=>$filter,
                'page'=>array('size'=>$size, 'number'=>$number)
            )
        );

        return $this->isSuccess() ? $this->translateToObjects() : array(0, array());
    }
}
