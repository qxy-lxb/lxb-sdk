<?php
namespace Sdk\Common\Adapter;

trait CommonMapErrorsTrait
{
    public function commonMapErrors()
    {
        return [
            15 => CSRF_VERIFY_FAILURE,
            16 => AFS_VERIFY_FAILURE,
            17 => NEED_SIGNIN,
            18 => SMS_SEND_TOO_QUICK,
            19 => HASH_INVALID,
            100 => PARAMETER_IS_UNIQUE,
            101 => PARAMETER_IS_EMPTY,
            102 => STATUS_ALREADY_DISABLED,
            103 => STATUS_ALREADY_ENABLED,
            104 => IMAGE_FORMAT_ERROR,
            105 => APPLY_STATUS_NOT_UNSUBMITTED,
            106 => STICK_ALREADY_TOP,
            107 => STICK_ALREADY_CANCELTOP,
            501 => REAL_NAME_FORMAT_ERROR,
            502 => CELLPHONE_FORMAT_ERROR,
            503 => PASSWORD_FORMAT_ERROR,
            504 => CARDID_FORMAT_ERROR,
            505 => OLD_PASSWORD_INCORRECT,
            506 => PASSWORD_INCORRECT,
            507 => PASSWORD_INCONSISTENCY,
            508 => NAME_FORMAT_ERROR,
            1001 => NICK_NAME_FORMAT_ERROR,
            1002 => ADDRESS_FORMAT_ERROR,
            1003 => GENDER_TYPE_NOT_EXIST,
            2001 => ROLE_FORMAT_ERROR,
            2002 => ORGANIZATION_TYPE_NOT_EXIST,
            70001 => GOLD_COIN_NAME_FORMAT_ERROR,
            70002 => GOLD_COIN_NUMBER_FORMAT_ERROR,
            70003 => GOLD_COIN_PRODUCT_ID_FORMAT_ERROR,
            70004 => GOLD_COIN_AMOUNT_FORMAT_ERROR,
            70005 => GOLD_COIN_CREW_FORMAT_ERROR,
        ];
    }
}
