<?php
namespace Sdk\Common\Adapter;

use Sdk\Common\Model\IDeleteAble;

trait DeleteAbleRestfulAdapterTrait
{
    abstract protected function getResource() : string;

    public function softDelete(IDeleteAble $deleteAbleObject) : bool
    {
        return $this->softDeleteAction($deleteAbleObject);
    }

    protected function softDeleteAction(IDeleteAble $deleteAbleObject) : bool
    {
        $this->patch(
            $this->getResource().'/'.$deleteAbleObject->getId().'/softDelete'
        );
        if ($this->isSuccess()) {
            $this->translateToObject($deleteAbleObject);
            return true;
        }
        return false;
    }

    public function cancel(IDeleteAble $deleteAbleObject) : bool
    {
        return $this->cancelAction($deleteAbleObject);
    }

    protected function cancelAction(IDeleteAble $deleteAbleObject) : bool
    {
        $this->patch(
            $this->getResource().'/'.$deleteAbleObject->getId().'/cancel'
        );

        if ($this->isSuccess()) {
            $this->translateToObject($deleteAbleObject);
            return true;
        }
        return false;
    }
}
