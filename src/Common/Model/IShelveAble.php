<?php
namespace Sdk\Common\Model;

interface IShelveAble
{
    const STATUS = array(
        'SHELVE' => 0 ,
        'OFFSHELVE' => -2
    );

    public function shelve() : bool;
    
    public function offShelve() : bool;
}
