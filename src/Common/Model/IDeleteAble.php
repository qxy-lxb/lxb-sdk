<?php
namespace Sdk\Common\Model;

interface IDeleteAble
{
    const STATUS = array(
        'DEFAULT' => 0,
        'SOFT_DELETE' => -3,
        'CANCEL' => -4
    );

    public function softDelete() : bool;

    public function cancel() : bool;
}
