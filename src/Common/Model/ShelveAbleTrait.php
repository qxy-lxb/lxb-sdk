<?php
namespace Sdk\Common\Model;

use Marmot\Core;

use Sdk\Common\Adapter\IShelveAbleAdapter;
use Sdk\Common\Model\IShelveAble;

trait ShelveAbleTrait
{
    /**
     * 设置状态
     * @param int $status 状态
     */
    public function setStatus(int $status)
    {
        $this->status = in_array($status, array_values(IShelveAble::STATUS)) ? $status : IShelveAble::STATUS['OFFSHELVE'];
    }
    /**
     * 上架
     * @return bool 是否上架成功
     */
    public function shelve() : bool
    {
        if (!$this->isOffStock()) {
            Core::setLastError(STATUS_ALREADY_OFF_SHELVE);
            return false;
        }

        $shelveAddapter = $this->getIShelveAbleAdapter();
        return $shelveAddapter->shelve($this);
    }

    /**
     * 下架
     * @return bool 是否下架成功
     */
    public function offShelve() : bool
    {
        if (!$this->isShelve()) {
            Core::setLastError(STATUS_ALREADY_SHELVE);
            return false;
        }

        $shelveAddapter = $this->getIShelveAbleAdapter();
        return $shelveAddapter->offShelve($this);
    }

    abstract protected function getIShelveAbleAdapter() : IShelveAbleAdapter;

    public function isShelve() : bool
    {
        return $this->getStatus() == IShelveAble::STATUS['SHELVE'];
    }

    public function isOffStock() : bool
    {
        return $this->getStatus() == IShelveAble::STATUS['OFFSHELVE'];
    }
}
