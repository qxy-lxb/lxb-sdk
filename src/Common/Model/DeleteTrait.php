<?php
namespace Sdk\Common\Model;

use Sdk\Common\Adapter\IDeleteAbleAdapter;

trait DeleteTrait
{
    public function setStatus(int $status)
    {
        $this->status = in_array($status, array_values(self::STATUS)) ? $status : self::STATUS['DEFAULT'];
    }
    /**
     * 软删除
     * @return bool 是否删除成功
     */
    public function softDelete() : bool
    {
        if ($this->isSoftDelete()) {
            return false;
        }

        $deleteAdapter = $this->getIDeleteAbleAdapter();
        return $deleteAdapter->softDelete($this);
    }

    /**
     * 关闭
     * @return bool 是否关闭成功
     */
    public function cancel() : bool
    {
        if ($this->isCancel()) {
            return false;
        }
        $deleteAdapter = $this->getIDeleteAbleAdapter();
        return $deleteAdapter->cancel($this);
    }

    abstract protected function getIDeleteAbleAdapter() : IDeleteAbleAdapter;

    public function isSoftDelete() : bool
    {
        return $this->getStatus() == self::STATUS['SOFT_DELETE'];
    }

    public function isCancel() : bool
    {
        return $this->getStatus() == self::STATUS['CANCEL'];
    }
}
