<?php
namespace Sdk\Common\CommandHandler;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\Common\Model\IShelveAble;
use Sdk\Common\Command\OffShelveCommand;

abstract class OffShelveCommandHandler implements ICommandHandler
{
    abstract protected function fetchIOnShelfObject($id) : IShelveAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(OffShelveCommand $command)
    {
        $this->offShelve = $this->fetchIOnShelfObject($command->id);

        if ($this->offShelve->offShelve()) {
            // $this->logDriverInfo($this);
            return true;
        }

        // $this->logDriverError($this);
        return false;
    }
}
