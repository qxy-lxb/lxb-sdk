<?php
namespace Sdk\Common\CommandHandler;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use Sdk\Common\Model\IShelveAble;
use Sdk\Common\Command\ShelveCommand;

abstract class ShelveCommandHandler implements ICommandHandler
{
    abstract protected function fetchIShelveObject($id) : IShelveAble;

    public function execute(ICommand $command)
    {
        return $this->executeAction($command);
    }

    protected function executeAction(ShelveCommand $command)
    {
        $this->shelveAble = $this->fetchIShelveObject($command->id);

        if ($this->shelveAble->shelve()) {
            // $this->logDriverInfo($this);
            return true;
        }

        // $this->logDriverError($this);
        return false;
    }
}
