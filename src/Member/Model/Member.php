<?php
namespace Sdk\Member\Model;

use Sdk\Member\Repository\MemberRepository;

use Sdk\User\Model\User;

class Member extends User
{
    const DEFAULT_BIRTHDAY = '0000-00-00';
    /**
     * [$nickName 昵称]
     * @var [string]
     */
    private $nickName;
    /**
     * [$birthday 出生日期]
     * @var [string]
     */
    private $birthday;
    /**
     * [$address 详细地址]
     * @var [string]
     */
    private $address;
    /**
     * [$deviceAddress 设备地址]
     * @var [string]
     */
    private $deviceAddress;
    /**
     * [$repository]
     * @var [Object]
     */
    private $repository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->nickName = '';
        $this->birthday = self::DEFAULT_BIRTHDAY;
        $this->address = '';
        $this->deviceAddress = '';
        $this->repository = new MemberRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->nickName);
        unset($this->birthday);
        unset($this->address);
        unset($this->deviceAddress);
        unset($this->repository);
    }

    public function setNickName(string $nickName) : void
    {
        $this->nickName = $nickName;
    }

    public function getNickName() : string
    {
        return $this->nickName;
    }

    public function setBirthday(string $birthday) : void
    {
        $this->birthday = $birthday;
    }

    public function getBirthday() : string
    {
        return $this->birthday;
    }

    public function setAddress(string $address) : void
    {
        $this->address = $address;
    }

    public function getAddress() : string
    {
        return $this->address;
    }

    public function setDeviceAddress(string $deviceAddress) : void
    {
        $this->deviceAddress = $deviceAddress;
    }

    public function getDeviceAddress() : string
    {
        return $this->deviceAddress;
    }
    /**
     * 验证码修改密码
     * @return [bool]
     */
    public function captchaUpdatePassword() : bool
    {
        return $this->getRepository()->captchaUpdatePassword($this);
    }

    public function editAvatar() : bool
    {
        return $this->getRepository()->editAvatar($this);
    }

    protected function getRepository() : MemberRepository
    {
        return $this->repository;
    }

    public function editDeviceAddress() : bool
    {
        return $this->getRepository()->editDeviceAddress($this);
    }
}
