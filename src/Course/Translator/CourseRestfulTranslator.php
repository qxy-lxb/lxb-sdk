<?php
namespace Sdk\Course\Translator;

use Sdk\Common\Translator\RestfulTranslatorTrait;
use Marmot\Core;

use Sdk\Course\Model\Course;
use Sdk\Course\Model\NullCourse;
use Sdk\video\Translator\VideoRestfulTranslator;

use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Organization\Repository\OrganizationRepository;

class CourseRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    public function getVideoRestfulTranslator()
    {
        return new VideoRestfulTranslator();
    }

    public function arrayToObject(array $expression, $course = null)
    {
        return $this->translateToObject($expression, $course);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $course = null)
    {
        if (empty($expression)) {
            return new NullCourse();
        }

        if ($course == null) {
            $course = new Course();
        }

        $data =  $expression['data'];

        $id = $data['id'];
        $course->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['number'])) {
            $course->setNumber($attributes['number']);
        }
        if (isset($attributes['name'])) {
            $course->setName($attributes['name']);
        }
        if (isset($attributes['category'])) {
            $course->setCategory($attributes['category']);
        }
        if (isset($attributes['isBear'])) {
            $course->setIsBear($attributes['isBear']);
        }
        if (isset($attributes['applePrice'])) {
            $course->setApplePrice($attributes['applePrice']);
        }
        if (isset($attributes['androidPrice'])) {
            $course->setAndroidPrice($attributes['androidPrice']);
        }
        if (isset($attributes['studyTime'])) {
            $course->setStudyTime($attributes['studyTime']);
        }
        if (isset($attributes['isRequiredExam'])) {
            $course->setIsRequiredExam($attributes['isRequiredExam']);
        }
        if (isset($attributes['passLine'])) {
            $course->setPassLine($attributes['passLine']);
        }
        if (isset($attributes['expirationDate'])) {
            $course->setExpirationDate($attributes['expirationDate']);
        }
        if (isset($attributes['thumbnail'])) {
            $course->setThumbnail($attributes['thumbnail']);
        }
        if (isset($attributes['homeImage'])) {
            $course->setHomeImage($attributes['homeImage']);
        }
        if (isset($attributes['synopsis'])) {
            $course->setSynopsis($attributes['synopsis']);
        }
        if (isset($attributes['offReason'])) {
            $course->setOffReason($attributes['offReason']);
        }
        if (isset($attributes['applyStatus'])) {
            $course->setApplyStatus($attributes['applyStatus']);
        }
        if (isset($attributes['rejectReason'])) {
            $course->setRejectReason($attributes['rejectReason']);
        }
        if (isset($attributes['status'])) {
            $course->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $course->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $course->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['learnedVolume'])) {
            $course->setLearnedVolume($attributes['learnedVolume']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['videos']['data'])) {
            $videos = $this->changeArrayFormat($relationships['videos']['data']);
            $course->setVideos($videos);
        }
        if (isset($relationships['organization']['data'])) {
            $organization = $this->getOrganizationRepository()->fetchOne($relationships['organization']['data']['id']);
             $course->setOrganization($organization);
        }

        return $course;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($course, array $keys = array())
    {
        if (!$course instanceof Course) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'number',
                'name',
                'category',
                'isBear',
                'applePrice',
                'androidPrice',
                'studyTime',
                'isRequiredExam',
                'passLine',
                'expirationDate',
                'thumbnail',
                'homeImage',
                'videos',
                'synopsis',
                'crew',
                'rejectReason',
                'offReason',
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'courses'
            )
        );
        
        if (in_array('id', $keys)) {
            $expression['data']['id'] = $course->getId();
        }

        $attributes = array();

        if (in_array('number', $keys)) {
            $attributes['number'] = $course->getNumber();
        }
        if (in_array('name', $keys)) {
            $attributes['name'] = $course->getName();
        }
        if (in_array('category', $keys)) {
            $attributes['category'] = $course->getCategory();
        }
        if (in_array('isBear', $keys)) {
            $attributes['isBear'] = $course->getIsBear();
        }
        if (in_array('applePrice', $keys)) {
            $attributes['applePrice'] = $course->getApplePrice();
        }
        if (in_array('androidPrice', $keys)) {
            $attributes['androidPrice'] = $course->getAndroidPrice();
        }
        if (in_array('studyTime', $keys)) {
            $attributes['studyTime'] = $course->getStudyTime();
        }
        if (in_array('isRequiredExam', $keys)) {
            $attributes['isRequiredExam'] = $course->getIsRequiredExam();
        }
        if (in_array('passLine', $keys)) {
            $attributes['passLine'] = !empty($course->getPassLine()) ? $course->getPassLine() : 0;
        }
        if (in_array('expirationDate', $keys)) {
            $attributes['expirationDate'] = $course->getExpirationDate();
        }
        if (in_array('thumbnail', $keys)) {
            $attributes['thumbnail'] = $course->getThumbnail();
        }
        if (in_array('homeImage', $keys)) {
            $attributes['homeImage'] = $course->getHomeImage();
        }
        if (in_array('synopsis', $keys)) {
            $attributes['synopsis'] = $course->getSynopsis();
        }
        if (in_array('rejectReason', $keys)) {
            $attributes['rejectReason'] = $course->getRejectReason();
        }
        if (in_array('offReason', $keys)) {
            $attributes['offReason'] = $course->getOffReason();
        }

        $expression['data']['attributes'] = $attributes;

        if (in_array('crew', $keys)) {
            $expression['data']['relationships']['crew']['data'] = array(
                array(
                    'type' => 'crews',
                    'id' => $course->getCrew()->getId()
                )
            );
        }
        if (in_array('videos', $keys)) {
            $videos = array();
            $courses = explode(',', $course->getVideosId());
            foreach ($courses as $each) {
                $video = array();
                $video['type'] = 'videos';
                $video['id'] = marmot_decode($each);
                $videos[] = $video;
            }
            $expression['data']['relationships']['videos']['data'] = $videos;
        }

        return $expression;
    }

    protected function getOrganizationRepository()
    {
        return new OrganizationRepository();
    }
}
