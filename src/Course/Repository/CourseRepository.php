<?php
namespace Sdk\Course\Repository;

use Sdk\Common\Repository\AsyncRepositoryTrait;
use Sdk\Common\Repository\FetchRepositoryTrait;
use Sdk\Common\Repository\OperatAbleRepositoryTrait;
use Sdk\Common\Repository\ShelveAbleRepositoryTrait;

use Sdk\Course\Adapter\Course\ICourseAdapter;
use Sdk\Course\Adapter\Course\CourseRestfulAdapter;
use Sdk\Course\Adapter\Course\NewMockAdapter;
use Sdk\Course\Model\Course;
use Sdk\Common\Repository\ApplyAbleRepositoryTrait;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

class CourseRepository extends Repository implements ICourseAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, OperatAbleRepositoryTrait,ApplyAbleRepositoryTrait,ShelveAbleRepositoryTrait;

    private $adapter;

    const LIST_MODEL_UN = 'COURSE_LIST';
    const FETCH_ONE_MODEL_UN = 'COURSE_FETCH_ONE';

    public function __construct()
    {
        $this->adapter = new CourseRestfulAdapter(
            Core::$container->has('sdk.url') ? Core::$container->get('sdk.url') : '',
            Core::$container->has('sdk.authKey') ? Core::$container->get('sdk.authKey'):[]
        );
    }

    public function getActualAdapter() : ICourseAdapter
    {
        return $this->adapter;
    }

    public function getMockAdapter() : ICourseAdapter
    {
        return new NewMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function forceOffShelve(Course $course) : bool
    {
        return $this->getAdapter()->forceOffShelve($course);
    }
    
    public function snapshot($snapshot)
    {
        return $this->getAdapter()->snapshot($snapshot);
    }
}
