<?php
namespace Sdk\Course\Adapter\Course;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Adapter\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\OperatAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\CommonMapErrorsTrait;
use Sdk\Common\Adapter\ApplyAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\ShelveAbleRestfulAdapterTrait;

use Sdk\Course\Translator\CourseRestfulTranslator;
use Sdk\Course\Model\NullCourse;
use Sdk\Course\Model\Course;

use Marmot\Core;

class CourseRestfulAdapter extends GuzzleAdapter implements ICourseAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait,
        OperatAbleRestfulAdapterTrait,
        FetchAbleRestfulAdapterTrait,
        CommonMapErrorsTrait,
        ShelveAbleRestfulAdapterTrait,
        ApplyAbleRestfulAdapterTrait;

    private $translator;
    private $resource;

    const SCENARIOS = [
        'COURSE_LIST'=>[
            'fields'=>[
                    'organization'=>'name',
                ],
            'include'=> 'organization,videos'
        ],
        'COURSE_FETCH_ONE'=>[
            'fields'=>[
                    'videos'=>'name,category,studyTime,attachment,videoTime',
                    'organization' => 'name'
                ],
            'include'=> 'videos,organization'
        ]
    ];
    
    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );
        $this->translator = new CourseRestfulTranslator();
        $this->resource = 'courses';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getMapErrors() : array
    {
        $mapError = [
            30001 => COURSE_NUMBER_FORMAT_ERROR,
            30002 => COURSE_NAME_FORMAT_ERROR,
            30003 => COURSE_CATEGORY_FORMAT_ERROR,
            30004 => COURSE_PRICE_FORMAT_ERROR,
            30005 => COURSE_STUDYTIME_FORMAT_ERROR,
            30007 => IS_REQUIRED_EXAM_FORMAT_ERROR,
            30008 => COURSE_EXPIRATION_DATE_FORMAT_ERROR,
            30009 => COURSE_SYNOPSIS_FORMAT_ERROR,
            30010 => COURSE_VIDEOS_FORMAT_ERROR,
            30011 => NEED_SIGNIN,
            30012 => COURSE_STATUS_ALREADY_ON,
            30013 => COURSE_STATUS_ALREADY_OFF,
            30014 => COURSE_OFF_REASON_FORMAT_ERROR,
            30015 => COURSE_PASSLINE_FORMAT_ERROR,
            30016 => CREW_NOT_HAVE_PUBLISHING_PERMISSION
        ];

        $commonMapErrors = $this->commonMapErrors();

        return $mapError+$commonMapErrors;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullCourse());
    }

    public function addAction(Course $course) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $course,
            array('number','name','category','isBear','applePrice','androidPrice','studyTime','isRequiredExam','passLine','expirationDate','thumbnail','homeImage','videos','synopsis','crew')
        );
        $this->post(
            $this->getResource(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($course);
            return true;
        }

        return false;
    }

    public function editAction(Course $course) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $course,
            array('category','isBear','applePrice','androidPrice','studyTime','isRequiredExam','passLine','expirationDate','thumbnail','homeImage','videos','synopsis')
        );

        $this->patch(
            $this->getResource().'/'.$course->getId(),
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($course);
            return true;
        }

        return false;
    }
    /**
     * [forceOffShelve 强制下架]
     * @param Course $course [课程对象]
     * @return [bool]                [返回类型]
     */
    public function forceOffShelve(Course $course) : bool
    {
        return $this->forceOffShelveAction($course);
    }
    protected function forceOffShelveAction(Course $course) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $course,
            array(
                'offReason'
            )
        );

        $this->patch(
            $this->getResource().'/'.$course->getId().'/forceOffShelve',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($course);
            return true;
        }

        return false;
    }
    /**
     * [snapshot 课程快照]
     * @param $snapshot [课程快照]
     */
    public function snapshot($snapshot)
    {
        return $this->snapshotAction($snapshot);
    }
    protected function snapshotAction($snapshot)
    {
        $this->get(
            $this->getResource().'/'.$snapshot
        );

        return $this->isSuccess() ? $this->translateToObject() : array();
    }
}
