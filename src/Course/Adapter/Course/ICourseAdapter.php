<?php
namespace Sdk\Course\Adapter\Course;

use Sdk\Common\Adapter\IFetchAbleAdapter;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Common\Adapter\IApplyAbleAdapter;
use Sdk\Common\Adapter\IShelveAbleAdapter;
use Sdk\Course\Model\Course;

use Marmot\Interfaces\IAsyncAdapter;

interface ICourseAdapter extends IFetchAbleAdapter, IAsyncAdapter, IOperatAbleAdapter, IApplyAbleAdapter, IShelveAbleAdapter
{
    public function forceOffShelve(Course $course);
    public function snapshot($snapshot);
}
