<?php
namespace Sdk\Course\Model;

use Sdk\Common\Model\IOperatAble;
use Sdk\Common\Model\OperatAbleTrait;
use Sdk\Common\Adapter\IEnableAbleAdapter;
use Sdk\Common\Adapter\ITopAbleAdapter;
use Sdk\Common\Model\IEnableAble;
use Sdk\Common\Model\EnableAbleTrait;
use Sdk\Common\Model\IShelveAble;

use Sdk\Crew\Model\Crew;
use Sdk\Common\Model\IApplyAble;
use Sdk\Common\Model\ApplyAbleTrait;
use Sdk\Common\Adapter\IApplyAbleAdapter;
use Sdk\Common\Model\ShelveAbleTrait;
use Sdk\Common\Adapter\IShelveAbleAdapter;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Marmot\Core;
use Sdk\Common\Adapter\IOperatAbleAdapter;
use Sdk\Organization\Model\Organization;

use Sdk\Course\Repository\CourseRepository;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Course implements IOperatAble, IObject, IApplyAble, IShelveAble
{
    use OperatAbleTrait, Object, ApplyAbleTrait, ShelveAbleTrait;

    /**
     * @var CATEGORY['GENERAL_PURPOSE']  通用型 1
     */
    const CATEGORY = array(
        'GENERAL_PURPOSE' => 1
    );

    /**
     * @var IS_REQUIREDEXAM['YES']  需要考试 1
     * @var IS_REQUIREDEXAM['NO']  不需要考试 2
     */
    const IS_REQUIREDEXAM = array(
        'YES' => 1,
        'NO' => 2
    );

    /**
     * @var PASSLINE['DEFAULT']  默认 0
     * @var PASSLINE['NINETY_POINTS_PASS']  90分及格 1
     * @var PASSLINE['EIGHTY_POINTS_PASS']  80分及格 2
     * @var PASSLINE['SEVENTY_POINTS_PASS']  70分及格 3
     * @var PASSLINE['SIXTY_POINTS_PASS']  60分及格 4
     */
    const PASSLINE = array(
        'DEFAULT' => 0,
        'NINETY_POINTS_PASS' => 1,
        'EIGHTY_POINTS_PASS' => 2,
        'SEVENTY_POINTS_PASS' => 3,
        'SIXTY_POINTS_PASS' => 4
    );
    
    /**
     * @var EXPIRATION_DATE['THREE_MONTH']  自购买之日起三个月内有效 1
     * @var EXPIRATION_DATE['SIX_MONTH']  自购买之日起六个月内有效 2
     * @var EXPIRATION_DATE['ONE_YEAR']  自购买之日一年内有效 3
     * @var EXPIRATION_DATE['GET_TRAINING_CERTIFICATE']  获取培训证明之后失效 4
     * @var EXPIRATION_DATE['FOREVER']  永久有效 5
     */
    const EXPIRATION_DATE = array(
        'THREE_MONTH' => 1,
        'SIX_MONTH' => 2,
        'ONE_YEAR' => 3,
        'GET_TRAINING_CERTIFICATE' => 4,
        'FOREVER' => 5
    );

    private $id;

    private $number;

    private $name;

    private $category;

    private $isBear;

    private $applePrice;

    private $androidPrice;

    private $studyTime;

    private $isRequiredExam;

    private $passLine;

    private $expirationDate;

    private $thumbnail;

    private $homeImage;

    private $videos;

    private $videosId;

    private $synopsis;

    private $crew;

    private $offReason;

    private $rejectReason;

    private $snapshot;

    private $repository;

    private $organization;

    private $learnedVolume;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->number = '';
        $this->name = '';
        $this->category = self::CATEGORY['GENERAL_PURPOSE'];
        $this->isBear = self::IS_REQUIREDEXAM['YES'];
        $this->applePrice = 0; 
        $this->androidPrice = 0;
        $this->studyTime = '';
        $this->isRequiredExam = self::IS_REQUIREDEXAM['NO'];
        $this->passLine = self::PASSLINE['DEFAULT'];
        $this->expirationDate = self::EXPIRATION_DATE['THREE_MONTH'];
        $this->thumbnail = array();
        $this->homeImage = array();
        $this->videos = array();
        $this->videosId = '';
        $this->synopsis = '';
        $this->crew = Core::$container->has('crew') ? Core::$container->get('crew') : new Crew();
        $this->offReason = '';
        $this->applyStatus = 0;
        $this->rejectReason = '';
        $this->snapshot = '';
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->repository = new CourseRepository();
        $this->organization = new Organization();
        $this->learnedVolume = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->number);
        unset($this->name);
        unset($this->category);
        unset($this->isBear);
        unset($this->applePrice);
        unset($this->androidPrice);
        unset($this->studyTime);
        unset($this->isRequiredExam);
        unset($this->passLine);
        unset($this->expirationDate);
        unset($this->thumbnail);
        unset($this->homeImage);
        unset($this->videos);
        unset($this->videosId);
        unset($this->synopsis);
        unset($this->crew);
        unset($this->offReason);
        unset($this->applyStatus);
        unset($this->rejectReason);
        unset($this->snapshot);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->repository);
        unset($this->organization);
        unset($this->learnedVolume);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setNumber(string $number) : void
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setCategory(int $category) : void
    {
        $this->category = in_array(
            $category,
            self::CATEGORY
        ) ? $category : self::CATEGORY['GENERAL_PURPOSE'];
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function setIsBear(int $isBear) : void
    {
        $this->isBear = in_array(
            $isBear,
            self::IS_REQUIREDEXAM
        ) ? $isBear : self::IS_REQUIREDEXAM['YES'];
    }

    public function getIsBear() : int
    {
        return $this->isBear;
    }

    public function setApplePrice(int $applePrice) : void
    {
        $this->applePrice = $applePrice;
    }

    public function getApplePrice() : int
    {
        return $this->applePrice;
    }

    public function setAndroidPrice(int $androidPrice) : void
    {
        $this->androidPrice = $androidPrice;
    }

    public function getAndroidPrice() : int
    {
        return $this->androidPrice;
    }

    public function setStudyTime(string $studyTime) : void
    {
        $this->studyTime = $studyTime;
    }

    public function getStudyTime() : string
    {
        return $this->studyTime;
    }

    public function setIsRequiredExam(int $isRequiredExam) : void
    {
        $this->isRequiredExam = in_array(
            $isRequiredExam,
            self::IS_REQUIREDEXAM
        ) ? $isRequiredExam : self::IS_REQUIREDEXAM['NO'];
    }

    public function getIsRequiredExam() : int
    {
        return $this->isRequiredExam;
    }

    public function setpassLine(int $passLine) : void
    {
        $this->passLine = in_array(
            $passLine,
            self::PASSLINE
        ) ? $passLine : self::PASSLINE['DEFAULT'];
    }

    public function getpassLine() : int
    {
        return $this->passLine;
    }

    public function setExpirationDate(int $expirationDate) : void
    {
        $this->expirationDate = in_array(
            $expirationDate,
            self::EXPIRATION_DATE
        ) ? $expirationDate : self::EXPIRATION_DATE['THREE_MONTH'];
    }

    public function getExpirationDate() : int
    {
        return $this->expirationDate;
    }

    public function setThumbnail(array $thumbnail) : void
    {
        $this->thumbnail = $thumbnail;
    }

    public function getThumbnail() : array
    {
        return $this->thumbnail;
    }

    public function setHomeImage(array $homeImage) : void
    {
        $this->homeImage = $homeImage;
    }

    public function getHomeImage() : array
    {
        return $this->homeImage;
    }

    public function setVideos(array $videos) : void
    {
        $this->videos = $videos;
    }

    public function getVideos() : array
    {
        return $this->videos;
    }

    public function setVideosId(string $videosId) : void
    {
        $this->videosId = $videosId;
    }

    public function getVideosId() : string
    {
        return $this->videosId;
    }

    public function setSynopsis(string $synopsis) : void
    {
        $this->synopsis = $synopsis;
    }

    public function getSynopsis() : string
    {
        return $this->synopsis;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setOffReason(string $offReason) : void
    {
        $this->offReason = $offReason;
    }

    public function getOffReason() : string
    {
        return $this->offReason;
    }

    public function setApplyStatus(int $applyStatus) : void
    {
        $this->applyStatus = $applyStatus;
    }

    public function getApplyStatus() : int
    {
        return $this->applyStatus;
    }
    
    public function setRejectReason(string $rejectReason) : void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason() : string
    {
        return $this->rejectReason;
    }

    public function setSnapshot(string $snapshot) : void
    {
        $this->snapshot = $snapshot;
    }

    public function getSnapshot() : string
    {
        return $this->snapshot;
    }

    protected function getRepository() : CourseRepository
    {
        return $this->repository;
    }

    protected function getIEnableAbleAdapter() : IEnableAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIOperatAbleAdapter() : IOperatAbleAdapter
    {
        return $this->getRepository();
    }

    public function setOrganization(Organization $organization) : void
    {
        $this->organization = $organization;
    }

    public function getOrganization() : Organization
    {
        return $this->organization;
    }

    public function setLearnedVolume($learnedVolume) : void
    {
        $this->learnedVolume = $learnedVolume;
    }

    public function getLearnedVolume() : int
    {
        return $this->learnedVolume;
    }

    protected function getIApplyAbleAdapter() : IApplyAbleAdapter
    {
        return $this->getRepository();
    }

    protected function getIShelveAbleAdapter() : IShelveAbleAdapter
    {
        return $this->getRepository();
    }
    /**
     * 强制下架
     * @return bool 强制下架
     */
    public function forceOffShelve() : bool
    {
        return $this->getRepository()->forceOffShelve($this);
    }
}
